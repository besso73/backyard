
exec { 'apt-update':
  command => '/usr/bin/apt-get update'
}

# Packages that need to be installed
$webtools = [
  "apache2",
  "git-core",
  "curl",
  "python-boto",
  "memcached",
  "php5-fpm",
  "php5",
  "mysql-server",
  "mysql-client",
  "php5-mysql",
  "libapache2-mod-auth-mysql",
  "libapache2-mod-php5",
  "php5-dev",
  "php-pear",
  "php5-curl",
  "php5-imap",
  "php5-mcrypt",
  "php5-memcached",
  "php5-xdebug",
  "phpmyadmin"
]

package { $webtools:
  ensure => "installed",
  require => Exec['apt-update']
}

service { 'apache2':
  enable => true,
  ensure => "running"
}

exec { 'allow Apache to accept mod_rewrite' :
  command => '/bin/sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/sites-available/default',  
  logoutput => true,
  require => Package[$webtools]
}

exec { 'enable mod_rewrite' :
  command => '/bin/ln -s /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/rewrite.load',
  logoutput => true,
  require => Package[$webtools]
}

exec { 'set DocumentRoot in httpd.conf' :
  command => '/bin/sed -i "s/DocumentRoot \/var\/www/DocumentRoot \/var\/www\/backyard/g" /etc/apache2/sites-available/default',
  logoutput => true,
  require => Package[$webtools]
}

Package[$webtools] ->
Service['apache2']

exec { 'link DocumentRoot' :
  command => '/bin/ln -s /vagrant/www /var/www/backyard',
  logoutput => true
}

exec { 'create cache folder' : 
  command => '/bin/mkdir /var/www/backyard/system/cache && /bin/chmod 777 /var/www/backyard/system/cache',  
  logoutput => true,
  require =>Exec['link DocumentRoot']
}

exec { 'create system log folder' : 
  command => '/bin/mkdir /var/www/backyard/system/logs && /bin/chmod 777 /var/www/backyard/system/logs',  
  logoutput => true,
  require =>Exec['link DocumentRoot']
}

exec { 'create image cache folder' : 
  command => '/bin/mkdir /var/www/backyard/image/cache && /bin/chmod 777 /var/www/backyard/image/cache',  
  logoutput => true,
  require =>Exec['link DocumentRoot']
}

#file { 'bash_profile':
#  ensure => file,
#  mode => 0644,
#  owner => 'vagrant',
#  group => 'vagrant',
#  path => '/home/vagrant/.bash_profile',
#  source => '/vagrant/manifests/conf/.bash_profile'
#}