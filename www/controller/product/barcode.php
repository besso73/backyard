<?php
class ControllerProductBarcode extends Controller{
    
    private $error = array();
    
    public function index(){
	$this->mobile = true;
        $this->route = $this->request->get['route'];
    	$this->view();
    }
    
    private function view(){
    	$this->template = $this->route . '.tpl';
    	$this->children = array(
    		'common/mheader',
    		'common/footer'
    	);
    	$this->response->setOutput($this->render(TRUE));
    	
    }
}
?>
