<?php

class ModelProductBase extends Model {
    
    public function getTees() {
        $key = CACHE_KEY_TEES;
        $this->cache->delete($key);

        $sql = "SELECT p.product_id, pd.name as name, p.model, p.ws_price, p.rt_price,
					   p.quantity, p.barcode
				  FROM product p
				  JOIN product_description pd ON p.product_id = pd.product_id
				 WHERE p.status = 1
				   AND p.model != ''
				   AND pd.name != ''
				   AND substr(p.model,1,2) = 'PH'
				 ORDER BY p.model ASC";
        $query = $this->db->query($sql);
        $tees = $query->rows;
        
        $this->cache->set($key, $tees);
        return $tees;
    }
}
?>