<?php
class ControllerCommonCache extends Controller {

	private $error = array();

	public function ajaxRemoveCache($key='products') {
		$this->cache->delete($key);
	}

	public function ajaxFlushCache($key='products', $reset=true) {

		//echo '<pre>'; print_r($this->request); echo '</pre>'; exit;
		
		if(isset($this->request->get['key'])){
			$key = $this->request->get['key'];
        }
        
        $this->cache->delete($key);
        
        if(isset($this->request->get['reset'])){
			$reset = $this->request->get['reset'];
        }

		if ("true" == $reset || true == $reset) {
			switch ($key) {
				case (CACHE_KEY_PRODUCTS) :
			    $this->load->model('product/price');
			    $data = $this->model_product_price->getAllProducts();
			    break;
			}
			
			$this->cache->set($key, $data);
		}

		/* TODO
		 * spend time to set general response of ajax later
		 * james, 2014-03
		 */
        $json = json_encode($data);
		$return = array(
			'code' => 200,
			'message' => $json
		);
		echo json_encode($return);

	}
}
