<?php
class ControllerReportProduct extends Controller{
	public function index(){
    
 		isset($this->error['warning']) ? $this->data['error_warning'] = $this->error['warning'] : $this->data['error_warning'] = '';
		isset($this->session->data['success']) ? $this->data['success'] = $this->session->data['success'] : $this->data['success'] = '';
		unset($this->session->data['success']);

    $page   = $this->util->parseRequest('page','get','1');
    $sort   = $this->util->parseRequest('sort','get','');
    $order  = $this->util->parseRequest('order','get','DESC');
    
    $filter_from   = $this->util->parseRequest('filter_from','get',date("Y-m").'-01');
    $filter_to     = $this->util->parseRequest('filter_to','get',date("Y-m-t",strtotime("0 month")));

		$url = '';
		if($page) $url.='&page='.$page;
		if($sort) $url.='&sort='.$sort;
		if($order) $url.='&order='.$order;

		$this->data['token'] = $this->session->data['token'];
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['filter_from'] = $filter_from;
		$this->data['filter_to'] = $filter_to;

		$this->load->model('report/product');

		$req = array(
		  'sort'  => $sort,
		  'order' => $order,
		  'filter_from' => $filter_from,
		  'filter_to' => $filter_to,
		);

		$res = $this->model_report_product->stat_product($req);
		//$this->log->aPrint( $res );

    // today
		//$order_total_day = 0;
		//foreach($res['today'] as $row){ $order_total_day += $row['qty']; }
		//$total_count_day = count($res['today']);
		$this->data['today'] = $res['today'];
		//$this->data['order_total_day'] = $order_total_day;
		//$this->data['total_count_day'] = $total_count_day;
		
		// baseDate is before 4 months to show 4 month
		$baseDate = date("m-d-Y", strtotime('-3 month',strtotime(date('m').'/01/'.date('Y').' 00:00:00')));
    $this->data['baseDate'] = $baseDate;

    //$this->log->aPrint( $res['today'] );

		//$order_total = 0;
		//foreach($res['this_month'] as $row){ $order_total += $row['order_price']; }
		//$total_count = count($res['this_month']);
		$this->data['stat'] = $res['this_month'];
		//$this->data['order_total'] = $order_total;
		//$this->data['total_count'] = $total_count;
    //
		//$order_total_last = 0;
		//foreach($res['last_month'] as $row){ $order_total += $row['order_price']; }
		//$total_count_last = count($res['last_month']);
		//$this->data['lstat'] = $res['last_month'];
		//$this->data['order_total_last'] = $order_total_last;
		//$this->data['total_count_last'] = $total_count_last;

    // day
    for($i=0;$i<7;$i++){
      $week = date('w',strtotime("-1 day"));
      if($week != 0 || $week != 6){
        $pday_label = date('m-d(D)',strtotime("-1 day"));
        $pday_from = $pday_to = date('Y-m-d',strtotime("-1 day"));
        break;
      }
    }
    $this->data['pday_label'] = $pday_label;
		$this->data['lnk_pday'] = HTTPS_SERVER . '/report/product&filter_from=' . $pday_from . '&filter_to=' . $pday_to;

    $tday_label = date('m-d(D)',strtotime("0 month"));
    $tday_from = $tday_to = date('Y-m-d',strtotime("0 month"));
    $this->data['tday_label'] = $tday_label;
		$this->data['lnk_tday'] = HTTPS_SERVER . '/report/product&filter_from=' . $tday_from . '&filter_to=' . $tday_to;
    
    // month
    $pmonth_label = date('Y-m',strtotime("-1 month"));
    $pmonth_from = date('Y-m-01',strtotime("-1 month"));
    $pmonth_to = date('Y-m-t',strtotime("-1 month"));
    $this->data['pmonth_label'] = $pmonth_label;
		$this->data['lnk_pmonth'] = HTTPS_SERVER . '/report/product&filter_from=' . $pmonth_from . '&filter_to=' . $pmonth_to;

    // todo. Need Label work
    $tmonth_label = date('Y-m');
    $tmonth_from = date('Y-m-01');
    $tmonth_to = date('Y-m-t');
    $this->data['tmonth_label'] = $tmonth_label;
		$this->data['lnk_tmonth'] = HTTPS_SERVER . '/report/product&filter_from=' . $tmonth_from . '&filter_to=' . $tmonth_to;

    $nmonth_label = date('Y-m',strtotime("+1 month"));
    $nmonth_from = date('Y-m-01',strtotime("+1 month"));
    $nmonth_to = date('Y-m-t',strtotime("+1 month"));
    $this->data['nmonth_label'] = $nmonth_label;
		$this->data['lnk_nmonth'] = HTTPS_SERVER . '/report/product&filter_from=' . $nmonth_from . '&filter_to=' . $nmonth_to;

    /***
    // quarter
    $pquarter_label = date('Y-m',strtotime("-6 month"));
    $pquarter_from = date('Y-m-01',strtotime("-6 month"));
    $pquarter_to = date('Y-m-t',strtotime("-6 month"));
    $this->data['pquarter_label'] = $pquarter_label;
		$this->data['lnk_pquarter'] = HTTPS_SERVER . '/report/product&filter_from=' . $pquarter_from . '&filter_to=' . $pquarter_to;

    $pquarter_label = date('Y-m',strtotime("-3 month"));
    $pquarter_from = date('Y-m-01',strtotime("-3 month"));
    $pquarter_to = date('Y-m-t',strtotime("-3 month"));
    $this->data['pquarter_label'] = $pquarter_label;
		$this->data['lnk_pquarter'] = HTTPS_SERVER . '/report/product&filter_from=' . $pquarter_from . '&filter_to=' . $pquarter_to;
    ***/

		$this->template = 'report/product.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
  }

	public function ordersales(){
    $filter_from = $this->util->parseRequest('filter_from','get',date("Y-m").'-01');
    $filter_to   = $this->util->parseRequest('filter_to','get',date("Y-m-t",strtotime("0 month")));
    $group       = $this->util->parseRequest('group','get','');

		$this->load->model('report/product');
		$req = array(
		  'filter_from' => $filter_from,
		  'filter_to' => $filter_to,
		  'group' => $group
		);

		$res = $this->model_report_product->ordersales($req);
		echo json_encode($res);
	}

	public function search(){
    // model

    $from = $this->request->get['from'];
    $to = $this->request->get['to'];

    if( $this->request->get['id'] == 'today' ){
   		$this->load->model('report/product');
  		$res = $this->model_report_product->getToday($this->request->get);
   		$this->data['aProduct'] = $res;
   		$this->data['from'] = $from;
  	  $this->template = 'report/product_today.tpl';
  	}else{
  		$this->load->model('report/product');
  		$res = $this->model_report_product->getSearch($this->request->get);
   		$this->data['aProduct'] = $res;
   		$this->data['from'] = $from;
   		$this->data['to'] = $to;
  	  $this->template = 'report/product_search.tpl';
  	}
		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
	}
}
?>
