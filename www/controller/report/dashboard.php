<?php
class ControllerReportDashboard extends Controller{
	public function index(){
		$this->template = 'report/dashboard.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
	}
}
?>
