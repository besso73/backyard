<?php
/***
todo. need to make list button

DROP TABLE `statement`;
CREATE TABLE `statement` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `txids` varchar(255) not null,
    `total` decimal(5,2) NOT NULL,
    `credit` decimal(5,2) NOT NULL,
    `credit_note` text,
    `rep` int(3) NOT NULL,
    `issued_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;


***/

class ControllerInvoiceStatement extends Controller{
	private $error = array();
	private $bApprove = false;
	private $bManager = false;
	private $txids = '';
	private $aTxid = '';

    public function index() {
        $txids = $this->request->get['txids'];
        $txids = substr( $txids, 0, -1 );
        $this->txids = $txids;
        $this->aTxid = explode( ',', $txids );
        $this->callOrderForm();
    }
    
    public function updateApprove(){
      //$this->log->aPrint( $this->request->get );
      isset($this->request->get['txid']) ?	$txid = $this->request->get['txid'] : $txid = '';
      isset($this->request->get['status']) ?	$status = $this->request->get['status'] : $status = '';
    
      $this->load->model('sales/order');
      # AR total
      if($this->model_sales_order->updateApprove($txid,$status)){
        echo 'success';
      }
    }
    
    public function arHistory(){
      //$this->log->aPrint( $this->request->get );
      isset($this->request->get['store_id']) ?	$store_id = $this->request->get['store_id'] : $store_id = '';
      $this->data['store_id'] = $store_id;
    
      $this->load->model('sales/order');
      # AR total
      if($res = $this->model_sales_order->selectStoreARTotal($this->data['store_id'])){
        $this->data['store_ar_total'] = $res;
      }
      # history
      if($res = $this->model_sales_order->selectStoreHistory($this->data['store_id'])){
        $this->data['store_history'] = $res;
      }
      //$this->log->aPrint( $res );
      $this->template = 'sales/arHistory.tpl';
      $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }
    
    public function saveStatement() {
        $this->load->model('invoice/order');
        $this->model_invoice_order->saveStatement($_REQUEST);
        echo '<pre>'; print_r($_POST); echo '</pre>';
        exit;
    }
    
    public function show(){
        
        $this->load->model('invoice/order');
        $aList = $this->model_invoice_order->show($_POST);
        
    }
    
    
    
    
    // order form
    private function callOrderForm($txid = ''){
        
        $this->data['txids'] = $this->txids;
        
        # translate
     	$this->data['button_save']   = $this->language->get('button_save');
     	$this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['heading_title'] = $this->language->get('heading_title');

    	if (isset($this->error['warning'])) {
    		$this->data['error_warning'] = $this->error['warning'];
        } else {
        	$this->data['error_warning'] = '';
        }

        # data from model
        $url = '';  // query param

        $aTemp = explode( '-', $this->aTxid[0] );
        $accountno = $aTemp[0];

        $this->load->model('sales/order');

        $aTX = array();
        foreach( $this->aTxid as $i => $txid ) {
            if($rows = $this->model_sales_order->selectTransaction($txid)){
                $aTX[$txid] = $rows;
                if( $i == 0 ) {
                    $this->data['store_id']  = $rows['store_id'];
                    $this->data['shipto']    = $rows['shipto'];
                    $this->data['salesrep']  = $rows['order_user'];
                }
            }
        }
        

        $this->data['aTX'] = $aTX;


        if($res = $this->model_sales_order->selectStoreWithAccount($accountno)){
            $this->data['store_name']   = $res['name'];
            $this->data['storetype']    = $res['storetype'];
            $this->data['accountno']    = $res['accountno'];
            $this->data['address1']   = $res['address1'];
            $this->data['city']   = $res['city'];
            $this->data['state']  = $res['state'];
            $this->data['zipcode']    = $res['zipcode'];
            $this->data['phone1']    = $res['phone1'];
            $this->data['fax']    = $res['fax'];
       	} else {
            echo 'selectStore fail';
            exit;
       	}

        $this->data['ddl'] = 'update';

        //$this->load->model('product/lib');
        //if($res = $this->model_product_lib->getProduct($this->data['txid'])){
     	
        // [todo] it's ugly way to pass session
     	$this->data['token']    = $this->session->data['token'];

        # lnk
    	$this->data['lnk_cancel'] = HTTP_SERVER . '/sales/order&token=' . $this->session->data['token'] . $url;
    	$this->data['lnk_list'] = HTTP_SERVER . '/invoice/list&token=' . $this->session->data['token'] . $url;
        $this->data['order_action'] = HTTP_SERVER . '/invoice/order/saveOrder&token=' . $this->session->data['token'];

        $this->template = 'invoice/statement.tpl';
        $this->children = array(
        	'common/header',
        	'common/footer'
        );
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }

    public function saveOrder(){
      $this->log->aPrint( $this->request->post );    
      # only store ship
    
      $txid = $this->request->post['txid'];
      $store_id = $this->request->post['store_id'];
      $order_date = $this->request->post['order_date'];
      $invoice_no = $this->request->post['invoice_no'];
      $shipped_yn = $this->request->post['shipped_yn'];
    
      #ship and pay
      $weight_sum = $this->request->post['weight_sum'];
      $term = $this->request->post['term'];
    
      #ship
      $aShip_id = $this->request->post['ship_id'];
      $aMethod = $this->request->post['method'];
      $aShip_date = $this->request->post['ship_date'];
      $aLift = $this->request->post['lift'];
      $aCod = $this->request->post['cod'];
      $aShip_appointment = $this->request->post['ship_appointment'];
      $aShip_comment = $this->request->post['ship_comment'];
      $aShip_user = $this->request->post['ship_user'];
    
      $this->load->model('sales/order');
    
      // parsing data for ddl
      $data = array(
        'ship' => array(
          $aMethod,
          $aShip_date,
          $aLift,
          $aCod,
          $aShip_appointment,
          $aShip_comment,
          $aShip_user,
          'txid' => $txid
        )
      );
    
      //$this->log->aPrint( $data['ship'] ); exit;
      # always insert for ship and 
      if('ok' == $this->model_sales_order->insertShip($data['ship']) ){
        // todo. keep logging for any transaction in log
      }else{
        // todo. need exception 
        return false; 
      }
      
      $this->redirect(HTTP_SERVER . '/invoice/order&token=' . $this->session->data['token'] . '&txid=' . $txid );
    }
    
    public function verify_txid(){
          //$this->log->aPrint( $this->request->get['txid'] );
          # call model to check txid and return
        $this->load->model('sales/order');
          $response = $this->model_sales_order->getTxid($this->request->get['txid']);
          $this->data['txid'] = array();
          //$this->log->aPrint( count($response) );
          if(count($response) != 0 ){
            $this->data['txid'] = $response;
          }
          $this->template = 'sales/verify_txid_proxy.tpl';
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }


    // ajax proxy call
    public function getList(){
        $this->load->model('invoice/list');
        $total = $this->model_invoice_list->getTotalStatementList();
        $list = $this->model_invoice_list->getStatementList();
        $this->data['list'] = $list;
        
        # call view
        $this->template = 'invoice/statement.list.tpl';
        $this->children = array(
        	'common/header',
        	'common/footer'
        );
        $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
    }

}
?>
