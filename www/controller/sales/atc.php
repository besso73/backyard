<?php
class ControllerSalesATC extends Controller{

	private $error = array();

 	public function products() {
 		
        $query = isset($this->request->get['query']) ?  $this->request->get['query'] : '';
		$products = $this->cache->get(CACHE_KEY_PRODUCTS);
		$idx = 0;
		$result = array();
		foreach($products as $product) {

			if($idx == ATC_MAX_RETURN) break;

			$query = strtolower($query);
			
			
			if (strstr($query,' ')) {
				$aWord = explode(' ',$query);
				$ptn = '(.*)';
				foreach($aWord as $word) {
					$ptn .= '(' . $word . ')(.*)';
				}
			} else {
				$ptn = $query;
			}
			
			if( preg_match("/$ptn/", strtolower($product['name'])) ) {
				$result[] = $product;
				$idx++;
				continue;
			}

			if( preg_match("/$ptn/", strtolower($product['model'])) ) {
				$result[] = $product;
				$idx++;
				continue;
			}

		}
		echo json_encode($result);
	}

}
?>
