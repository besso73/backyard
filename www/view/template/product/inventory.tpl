<?php
function minName($code,$name){
  $min = $name;
  if( strstr($min,'Display') ){
    return 'Display';
  }

  $prefix = substr($name,0,2);
  $prefix = strtoupper($prefix);
  $aTmp = preg_split('/ /',$name);
  $cnt = count($aTmp);

  if( $cnt == 3 ){
    $ptn = '/([A-Z0-9\-]*\s)([A-Za-z0-9\-\+]*\s.[0-9.]*)/';
    preg_match($ptn,$name,$matches);
    $min = isset($matches[2]) ? $matches[2] : $name;
  }
  if( $cnt == 4 ){
    $ptn = '/([A-Za-z]*\s)([A-Za-z\-0-9]*\s)([A-Za-z0-9]*\s.[0-9\#.]*)/';
    preg_match($ptn,$name,$matches);
    //if( $code == 3106 ) print_r($matches);
    $min = isset($matches[3]) ? $matches[3] : $name;
  }

  $min = str_replace(' ','',$min);
  $min = str_replace('Yellow','Ylow',$min);
  $min = str_replace('Black','Blk',$min);
  $min = str_replace('Spray','Spry',$min);
  $min = str_replace('Shampoo','Smp',$min);
  $min = str_replace('Remover','Rmr',$min);
  $min = str_replace('Lotion','Ltn',$min);
  $min = str_replace('Conditioner','Con',$min);
  $min = str_replace('Conditioner','Con',$min);
  $min = str_replace('Deodorizer','Deo',$min);
  $min = str_replace('Style','styl',$min);
  $min = str_replace('Foam','Fom',$min);
  $min = str_replace('Detangler','Det',$min);
  $min = str_replace('Extreme','Extm',$min);
  $min = str_replace('Cleaner','Clnr',$min);
  $min = str_replace('Sweet','Swt',$min);
  $min = str_replace('Pink','Pnk',$min);
  $min = str_replace('Potency','Ptncy',$min);
  $min = str_replace('Blossom','Blsm',$min);
  $min = str_replace('Gardenia','Gdnia',$min);
  $min = str_replace('Cherry','Chry',$min);
  $min = str_replace('Cotton','Ctn',$min);
  $min = str_replace('Fresh','Frsh',$min);
  $min = str_replace('Extra','Extr',$min);
  $min = str_replace('Original','Org',$min);
  $min = str_replace('Cucumber','Ccumbr',$min);
  $min = str_replace('Laminator','Lminatr',$min);
  $min = str_replace('Butter','Butr',$min);
  $min = str_replace('Protein','Ptein',$min);
  $min = str_replace('Protector','Ptectr',$min);
  $min = str_replace('Clean','Cln',$min);
  $min = str_replace('Mahogany','Mhgany',$min);

  $min = str_replace('30S','',$min);

  return $min;
  //return $name;
}
function displayBlock($key,$offset,$exception,$aInventory,$aProduct){
  $aVal = $aInventory;
  //asort($aVal);
  /***
  if( $offset >= 3 ){
    //echo "<tr><td colspan=" . $offset * 2 . " class='title'>$key</td></tr>";
    echo "<tr><td colspan=12 class='title'>$key</td></tr>";
  }else{
    //echo "<tr><td colspan=" . $offset * 3 . " class='title' style='width:200px'>$key</td></tr>";
    echo "<tr><td colspan=12 class='title' style='width:200px'>$key</td></tr>";
  }
  ***/
  echo "<tr><td colspan=12 class='title' style='width:200px'>$key</td></tr>";
  echo "<tr>";
  $i=1;
  foreach($aVal as $val){
    //if( !in_array($val,$exception) ){
      if( $val == '' ){
        $t1 = $offset + 1;
        echo "<td colspan=$t1></td>";
        if($i%$offset == 0) echo "</tr><tr>";
        continue;
      }
      //$this->log->aPrint( $aProduct[$val] );
      $code = substr($val,2,4);
      $name = $aProduct[$val][0];
      $name = minName($code,$name);
      $count = $aProduct[$val][1];
      $bgInput = '';
      if($count < 100) $bgInput = 'background-color:yellow;';
      if($count < 0)   $bgInput = 'background-color:red;color:white;font-weight:bold;';
      if( $offset >= 3 ){
        //echo "<td class='code'>$code</td><td class='count'><input type='number' name='quantity' value=$count style='width:35px;$bgInput'/><input type=hidden name='code' value='$val' /></td>";
        if( $code >= '0105' && $code <= '0550' & substr($code,2,2) != '05' ){
          echo "<td class='code'>$code<input type=hidden name='model' value='$val'></td><td class='count'><input type='number' name='quantity' value=$count style='width:30px;$bgInput' /><input type=hidden name='code' value='$val' /></td>";
        }else{
          echo "<td class=name>$name</td><td class='code'>$code<input type=hidden name='model' value='$val'></td><td class='count'><input type='number' name='quantity' value=$count style='width:30px;$bgInput' /><input type=hidden name='code' value='$val' /></td>";
        }
      
      }else{
        if( substr($code,0,2) == '09' ){
          echo "<td></td><td class='code'>$code<input type=hidden name='model' value='$val'></td><td class='count'><input type='number' name='quantity' value=$count style='width:35px;$bgInput' /><input type=hidden name='code' value='$val'/></td>";
        }else{
          echo "<td class=name>$name</td><td class='code'>$code<input type=hidden name='model' value='$val'></td><td class='count'><input type='number' name='quantity' value=$count style='width:35px;$bgInput' /><input type=hidden name='code' value='$val' /></td>";
        }
      }
      if($i%$offset == 0){
        if( $offset == 3 )  echo "<td colspan='3' style='border:0'></td>";
        if( $offset == 2 )  echo "<td colspan='6' style='border:0'></td>";
        echo "</tr><tr>";
      }
      $i++;
    //}
  }
  echo "</tr>";
}
?>
<?php echo $header; ?>
<div class="box">
  <style>
    #content{ min-width:600px; }
    @page{ size:a4; }
    .title{
      color:white;  background-color:black;
      font-weight:bold; height:24px;
      font-size:14px; width:365px;
    }
    .name{
      background-color:#e3e3e3; width:20px;
    }
    .code{
      background-color:#e3e3e3; width:20px;
      font-weight:bold;
    }
    .count{
      width:20px; font-weight:bold; color:blue;
    }
    .list td{ border:none;  }
    .list table td{ 
      border:1px solid black;
      padding:0 0 0 1px;
      vertical-align:top;
    }
    .breadcrumb{  display:none; }
    .box .content{  border:none;  }
    .list{  border:none;  }
    #header,#menu{  display:none; }
  </style>
  <div class="content">
    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="table">
        <tbody>
          <?php
            # revamp products
            $aProduct = array();
            foreach( $products as $product ){
              $aProduct[$product['model']] = array($product['name'],$product['quantity']);
            }
            $aInventory = $this->config->getCatalogInventory();
          ?>
          <tr class='page1'>
            <td>
              <table>
                <tr>
                  <td class='col1'>
                    <table>
                      <?php
                      $key = 'AE St GEL';
                      $offset = 4;
                      $exception = array('AE0521');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = 'AE Nu Gro 1Dz';
                      $offset = 3;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = 'AE Spritz 1Dz';
                      $offset = 3;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = 'AE Shine 4oz 1Dz / AE Herbal Oil 4oz 1Dz';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = 'AE Lotion/Shampoo Dz / AE Spray 12oz Dz';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = 'SP HAIR FOOD 4 oz DZ';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = 'SP HAIR BONDING GLUE';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = 'SP REMOVER LOTION / SHAMPOO';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = '30 MAINTENANCE COLLECTION Dz box';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);
                      ?>
                    </table>
                  </td>
                  <td class='col2' style='vertical-align:top;'>
                    <table>
                      <?php
                      $key = '30 GLUE ESSENTIALS';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = '30 LACE BOND REMOVER SYSTEM';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = 'IRIE DRED 6pc-Lock VIA NATURAL EDGE-GEL';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = 'Weave Wonder Wrap';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = '30 Easy Track Weft Sealer';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      ?>
                      
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr style='height:80px;'><td></td></tr>

          <tr class='page2' style='page-break-before:always;'>
            <td>
              <table>
                <tr>
                  <td class='col1'>
                    <!-- color section has 2 column line -->
                    <table>
                      <tr>
                        <td>
                          <table>
                            <?php
                            $key = 'COLOR';
                            $offset = 2;
                            $exception = array('');
                            displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);
                            ?>
                          </table>
                        </td>
                        <td style='width:160px;'>
                          <table>
                            <?php
                            $key = '30 Lace Tape Regular Surface Dz';
                            $offset = 1;
                            $exception = array('');
                            displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                            $key = 'VIA NATURAL Oil-Free WIG SHINE';
                            $offset = 1;
                            $exception = array('');
                            displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                            $key = 'TUBE GLUE';
                            $offset = 1;
                            $exception = array('');
                            displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                            $key = 'VIA Tube Oil 24pc';
                            $offset = 1;
                            $exception = array('');
                            displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);
                            ?>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td colspan=2>
                          <table>
                            <?php
                            $key = 'VIA NATURAL GEL';
                            $offset = 3;
                            $exception = array('');
                            displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                            $key = 'VIA NATURAL STYLE SHINING GEL';
                            $offset = 3;
                            $exception = array('');
                            displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);
                            ?>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td class='col2'>
                    <table>
                      <?php
                      $key = 'VIA Spray 8oz*12pc . VIA SHINE 4OZ*12pc';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = 'VIA H Lotion 2oz*24';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = 'VIA NATURAL CERAMIC ENHANCING';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = 'VIA NATURAL REVITALIZATION GRO 6pc';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = 'VIA NATURAL Botanical GEL';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = 'Marcs Lano Lustre';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      $key = 'I-Remi and Romantic';
                      $offset = 2;
                      $exception = array('');
                      displayBlock($key,$offset,$exception,$aInventory[$key],$aProduct);

                      ?>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>

          <tr style='height:80px;'><td></td></tr>



          <tr class='page3' style='page-break-before:always;'>
            <td>
              <table>
                <tr>
                  <td class='col1'>
                    <!-- color section has 2 column line -->
                    <table>
                      <tr>
                        <td>
                          <table>
                            <?php
                              # revamp oem
                              $aOEM = array();
                              foreach( $oems as $oem ){
                                $aOEM[$oem['model']] = array($oem['name'],$oem['quantity']);
                              }
                              $key = 'OEM GLUE';
                              $offset = 3;
                              $exception = array('');
                              displayBlock($key,$offset,$exception,$aInventory[$key],$aOEM);
                            ?>
                          </table>
                        </td>
                        <td style='width:160px;'>
                          <table>
                            <?php
                              # revamp oem
                              $aOEM = array();
                              foreach( $oems as $oem ){
                                $aOEM[$oem['model']] = array($oem['name'],$oem['quantity']);
                              }
                              $key = 'OEM REMOVER';
                              $offset = 3;
                              $exception = array('');
                              displayBlock($key,$offset,$exception,$aInventory[$key],$aOEM);
                            ?>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td colspan=2>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td class='col2'>
                  </td>
                </tr>
              </table>
            </td>
          </tr>


        </tbody>
      </table>
    </form>
  </div>
</div>
<!--
-->
<style>
#detail{
  position:absolute;  top:100px;  left:200px;
  visibility:hidden;  border:1px dotted green;
  background-color:white; z-index:99;
}
</style>
<div id='detail'></div>
<script type="text/javascript">
$('input[name=quantity]').bind('click',function(e){ $(e.target).select(); });
$('input[name=quantity]')
.bind('keydown',function(e){
  if('13' == e.keyCode){
    $tgt = $(e.target);
    var $pnt = $tgt.parents('td'),
        $id = $pnt.find('input[name=code]').val(),
        $quantity = $tgt.val();
    //debugger;
    $.ajax({
      type:'get',
      url:'/product/price/updateQuantity2',
      dataType:'html',
      data:'&id=' + $id + '&quantity=' + $quantity,
      success:function(html){
        $p = $tgt.position();
        $imgCss = {
          'visibility':'visible','width':'150px','height':'20px',
          'top':$p.top-30,'left':$p.left-30,'background-color':'black',
          'color':'white','text-align':'center'
        }
        $('#detail').css($imgCss).html('success');
      }
    });
  }
});

$('.code').bind('click',function(e){
  var $tgt = $(e.target);
  var $code = $tgt.find("input[name=model]").val();
  $.ajax({
    type:'get',
    url:'/product/price/lookupProductHistory',
    dataType:'html',
    data:'&code=' + $code,
    success:function(html){
      $p = $tgt.position();
      $imgCss = {
        'visibility':'visible','width':'800px','height':'300px',
        'top':$p.top-30,'left':$p.left-30,'background-color':'black',
        'color':'white','text-align':'left','padding':'5px'
      }
      if( html != 0 ){
        $('#detail').css($imgCss).html(html);
        $('#detail').bind('click',function(e){
          $(this).css('visibility','hidden');
        });
      }
    }
  });
});
</script>
