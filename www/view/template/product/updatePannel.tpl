<style>
#tab_transfer,#tab_history{
  display:none;
}
#form .form{
    width:400px;
}

table.form {
	margin-bottom:0;
}

table.form tr td:first-child {
  width: 100px;
  background-color:white;
}

table.form tr td:first-child {
	background-color: #E2E2E2;
	line-height: 32px;
	width: 100px;
}

td.content {
	text-align: left;
}

.box > .heading {
	width: 600px;
}

.box > .content {
	width: 600px;
}
</style>
<div class="box" style='background-color:white;'>
    
    <div class="heading">
      <h1>Update Product</h1>
      <div class="buttons">
        <a class="btn btn-primary" id='save-product'>Save</a>
      </div>
    </div>
    <div class="content">
        <!--div id="tabs" class="htabs">
          <a tab="#tab_general">Base Info</a>
        </div-->
        <form action="<?php echo $action; ?>" id="updateForm">
		<div id="tab_general">

            <?php
              //$this->log->aPrint( $data );
              if('update' == $mode){
                $product_id = $data['product_id'];
                $model      = $data['model'];
                $sku        = $data['sku'];
                $quantity   = $data['quantity'];
                $image      = $data['image'];
                $ws_price   = $data['ws_price'];
                $rt_price   = $data['rt_price'];
                $pc         = $data['pc'];
                $ups_weight = $data['ups_weight'];
                $thres      = $data['thres'];
                $dc         = $data['dc'];
                $dc2        = $data['dc2'];
                $status     = $data['status'];
                $name = $data['name'];
                $name_for_sales = $data['name_for_sales'];
              } else {
                $product_id = '';
                $model      = '';
                $sku        = '';
                $quantity   = 0;
                $image      = '';
                $rt_price   = '';
                $ws_price   = '';
                $pc         = '';
                $ups_weight = '';
                $thres      = '';
                $dc   = 0;
                $status     = '1';
                $name = '';
                $name_for_sales = '';
              }
              
              //$sku = ( $sku == '' ) ? 'J6' : $sku;
            ?>
            <table class="form" border=0>
              <tr>
                <td class='label'>Code</td>
                <td class='content'>
                  <input type="hidden" name="mode" size="50" value="<?php echo $mode; ?>" />
                  <input type="hidden" name="product_id" size="50" value="<?php echo $product_id; ?>" readonly />
                  <input type="text" name="model" <?php if($mode=='insert'){ echo "id='product-model'"; } ?> value="<?php echo $model; ?>" placeholder="Code" />
                  <input type="hidden" name="sku" size="5" value="<?php echo $sku; ?>" placeholder="SKU" />
                  <select name="status">
                    <option value="1" <?php if('1'==$status) echo 'selected'; ?>>IN USE</option>
                    <option value="0" <?php if('0'==$status) echo 'selected'; ?>>Unuse</option>
                  </select>
                </td>
              </tr>
              <!--tr>
                <td class='label'>category</td>
                <td class='content'>
                    <select name="category">
                        <option value="0" <?php if('0'==$status) echo 'selected'; ?>>Unuse</option>
                    </select>
                </td>
              </tr-->
              <tr>
                <td class='label'>Name</td>
                <td class='content'>
                	<input type="text" name="name" value="<?php echo $name; ?>" class="input-xlarge" />
                </td>
              </tr>
              <tr>
                <td class='label'>Inventory</td>
                <td class='content'><input type="text" name="quantity" value="<?php echo $quantity; ?>" class="input-mini"/></td>
              </tr>
              <!--tr>
                <td class='label'>Alias</td>
                <td class='content'><input type="text" name="name_for_sales" size="50" value="<?php echo $name_for_sales; ?>" /></td>
              </tr-->
              <tr>
                <td class='label'>Price</td>
                <td class='content'>
                  <input type="text" name="rt_price" class="input-small" value="<?php echo $rt_price; ?>" placeholder='Retail' />
                  <input type="text" name="ws_price" class="input-small" value="<?php echo $ws_price; ?>" placeholder='Wholesale' />
                </td>
              </tr>
              <tr>
                <td class='label'>Alert Limit</td>
                <td class='content'>
                	<input type="text" name="thres" class="input-mini" value="<?php echo $thres; ?>" />
                </td>
              </tr>
              <tr>
                <td class='label'>Piece</td>
                <td class='content'><input type="text" name="pc" class="input-mini" value="<?php echo $pc; ?>" /></td>
              </tr>
              <!--tr>
                <td class='label'>Weight</td>
                <td class='content'><input type="text" name="ups_weight" class="input-mini" value="<?php echo $ups_weight; ?>" /></td>
              </tr-->
              <tr>
                <td class='label'>Discount</td>
                <td class='content'><input type="text" name="dc" class="input-mini" value="<?php echo $dc; ?>" /> %</td>
              </tr>
            </table>
        </div>
    	</form>
    </div>
</div>