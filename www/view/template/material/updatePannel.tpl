<style>
#tab_transfer,#tab_history{
  display:none;
}
#form .form{
  width:400px;
}
table.form tr td:first-child {
  width: 100px;
  background-color:white;
}
</style>
<div class="box" style='background-color:white;'>
  <div class="left" colspan=2></div>
  
  <div class="heading">
    <h1 style="background-image: url('view/image/product.png');">Package Update</h1>
    <div class="buttons">
      <a class="button save">
        <span>Save</span></a>
      <a onclick="$('#detail').html(); $('#detail').css('visibility','hidden');" class="button">
        <span>Cancel</span></a>
    </div>
  </div>
  <div id='xxxxx' class="content">
    <!--div id="tabs" class="htabs">
      <a tab="#tab_general">Base Info</a>
    </div-->
    <form action="<?php echo $action; ?>" id="updateForm">
      <div id="tab_general">
        <?php
          //$this->log->aPrint( $data );
          if('update' == $ddl){
            $id =          $data['id'];
            $code =        $data['code'];
            $name =        htmlentities($data['name']);
            $cat =         $data['cat'];
            $image =       $data['image'];
            $quantity =    $data['quantity'];
            $thres =       $data['thres'];
            $warehouse =   $data['warehouse'];
            $company =     $data['company'];
            $description = htmlentities($data['description']);
            $up_date =     $data['up_date'];
            $status =      $data['status'];
            //$size = $data['size'];
            //$color = $data['color'];
            $term = $data['term'];
            //$material = $data['material'];
            ('' != $data['price'])     ? $price      = $data['price']     : $price      = '0';
            //('' != $data['last_price'])? $last_price = $data['last_price']: $last_price = '0';
            //('' != $data['dprice'])    ? $dprice     = $data['dprice']    : $dprice     = '0';
            //('' != $data['iprice'])    ? $iprice     = $data['iprice']    : $iprice     = '0';
          }else{
            $id =          '';
            $code =        '';
            $name =        '';
            $cat =         '';
            $image =       '';
            $quantity =    '';
            $thres =       '';
            $warehouse =   '';
            $company =     '';
            $description = '';
            $up_date =     '';
            $status =      '1';
            //$size = '';
            //$color = '';
            //$material = '';
            $price      = '0';
            //$last_price = '0';
            //$dprice     = '0';
            //$iprice     = '0';
          }
        ?>
        <table class="form" border=0>
          <tr>
            <td class='label'>Code</td>
            <td>
              <input type="hidden" name="id" size="50" value="<?php echo $id; ?>" readonly />
              <input type="text" name="code" size="5" value="<?php echo $code; ?>" />
            </td>
            <td rowspan=4>
                  <?php
                  if('' != $image){
                    $preview = HTTP_IMAGE.$image;
                  }
                  ?>
                  <input type="hidden" name="image" value="<?php echo $image; ?>" id="image" />
                  <img src="<?php echo $preview; ?>" alt="" id="preview" class="image" onclick="image_upload('image', 'preview');" width='125px' height='150px' />
            </td>
          </tr>
          <tr>
            <td class='label'>Status</td>
            <td>
              <select name="status">
                <option value="0" <?php if('0'==$status) echo 'selected'; ?>>Unuse</option>
                <option value="1" <?php if('1'==$status) echo 'selected'; ?>>IN USE</option>
                <option value="2" <?php if('2'==$status) echo 'selected'; ?>>INACTIVE</option>
              </select>   
            </td>
          </tr>
          <tr>
            <td class='label'>Cat</td>
            <td>
              <select name="cat">
                <option value="" <?php if(''==$cat) echo 'selected'; ?>>---</option>
                <option value="BOTTLE" <?php if('BOTTLE'==$cat) echo 'selected'; ?>>BOTTLE</option>
                <option value="JAR" <?php if('JAR'==$cat) echo 'selected'; ?>>JAR</option>
                <option value="CAP" <?php if('CAP'==$cat) echo 'selected'; ?>>CAP</option>
                <option value="LABEL" <?php if('LABEL'==$cat) echo 'selected'; ?>>LABEL</option>
                <option value="BOX" <?php if('BOX'==$cat) echo 'selected'; ?>>BOX</option>
                <option value="DIV" <?php if('DIV'==$cat) echo 'selected'; ?>>DIV</option>
                <option value="DISPLAY" <?php if('DISPLAY'==$cat) echo 'selected'; ?>>DISPLAY</option>
                <option value="PUMP" <?php if('PUMP'==$cat) echo 'selected'; ?>>PUMP</option>
                <option value="RACK" <?php if('RACK'==$cat) echo 'selected'; ?>>RACK</option>
                <option value="SPRAY" <?php if('SPRAY'==$cat) echo 'selected'; ?>>SPRAY</option>
                <option value="PAD" <?php if('PAD'==$cat) echo 'selected'; ?>>PAD</option>
                <option value="LACE" <?php if('LACE'==$cat) echo 'selected'; ?>>LACE</option>
                <option value="MISC" <?php if('MISC'==$cat) echo 'selected'; ?>>MISC</option>
              </select>
            </td>
          </tr>
          <tr>
            <td class='label'>quantity</td>
            <td><input type="text" name="quantity" value="<?php echo $quantity; ?>" size=5 /></td>
          </tr>
          <tr>
            <td class='label'>Name</td>
            <td colspan=2><input type="text" name="name" size="50" value="<?php echo $name; ?>" /></td>
          </tr>
          <tr>
            <td class='label'>Price</td>
            <td colspan=2>
              <input type="text" name="price" size="50" value="<?php echo $price; ?>" />
                
                <!--ul style='align:right;'>
                  <li style='list-style:none;'> Last Price : <input type="text" name="last_price" size="5" value="<?php echo $last_price; ?>" /> </li>
                  <li style='list-style:none;'> Domestic Price : <input type="text" name="dprice" size="5" value="<?php echo $dprice; ?>" /> </li>
                  <li style='list-style:none;'> International Price : <input type="text" name="iprice" size="5" value="<?php echo $iprice; ?>" /> </li>
                </ul-->
            </td>
          </tr>
          <tr>
            <td class='label'>thres</td>
            <td colspan=2><input type="text" name="thres" size="50" value="<?php echo $thres; ?>" /></td>
          </tr>
          <!--tr>
            <td class='label'>size</td>
            <td colspan=2><input type="text" name="size" size="50" value="<?php echo $size; ?>" /></td>
          </tr>
          <tr>
            <td class='label'>color</td>
            <td colspan=2><input type="text" name="color" size="50" value="<?php echo $color; ?>" /></td>
          </tr>
          <tr>
            <td class='label'>material</td>
            <td colspan=2><input type="text" name="material" size="50" value="<?php echo $material; ?>" /></td>
          </tr-->
          <tr>
            <td class='label'>company</td>
            <td colspan=2><input type="text" name="company" size="50" value="<?php echo $company; ?>" /></td>
          </tr>
          <tr>
            <td class='label'>warehouse</td>
            <td colspan=2><input type="text" name="warehouse" size="50" value="<?php echo $warehouse; ?>" /></td>
          </tr>
          <tr>
            <td class='label'>Term</td>
            <td colspan=2>
              <?php if(!isset($term)) $term = 'w'; ?>
              <select name='term'>
                <option value='w' <?php if($term=='w') echo 'selected'; ?>>every week</option>
                <option value='m' <?php if($term=='m') echo 'selected'; ?>>every month</option>
                <option value='y' <?php if($term=='y') echo 'selected'; ?>>every day</option>
              </select>
            </td>
          </tr>
          <tr>
            <td class='label'>description</td>
            <td colspan=2><textarea name="description" style="width:284px;height:133px;"><?php echo $description; ?></textarea></td>
          </tr>
          <tr><td colspan=3> History comment </td></tr>
          <tr>
            <td class='label'>history</td>
            <td colspan=2><textarea name="comment" style="width:284px;height:20px;"></textarea></td>
          </tr>
        </table>
      </div>
		</form>
  </div>
</div>

<script type="text/javascript">
$('.save').bind('click',function(e){
  var url = $('form#updateForm').attr('action');
  var data= $('form#updateForm').serialize();
  $.post(url,data);
  
  setTimeout("window.location.reload();",1000);
});
</script>
