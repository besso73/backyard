<style>
.adore-table td {
    border: 0.5px dotted gray;
}

.adore-table input[type="text"]{
    width: 40px;
    height:15px;
    text-align: right;
}
</style>

<?php
require_once DIR_DATA . 'products/adore.cat';

$adore_css = 'style="display:block;"';
if (isset($_GET['adore'])) {
    $adore_css = 'style="display:block;"';
}
?>

<div id='order-adores' <?php echo $adore_css; ?>>
    <button id='btn-adores' class='btn custom-btn'>
        Adore
    </button>
</div>

<div id='adores-table' style='display:none;'>

    <table style="width:100%;" class='adores-table'>
        <tr>
            <?php
            foreach ($aAdore as $idx => $aType ) {
            ?>
            <th>order</th>
            <?php
            }
            ?>
        </tr>
        <?php
        $count = count($aAdore[3]);
        for ( $i = 0; $i < $count ; $i++ ) {
        //foreach ( $aAdore[3] as $i => $aTmp ) {
        ?>
        <tr>
            <?php
            foreach ( $aAdore as $aType ) {

                if ( !isset($aType[$i]) ) {
                    $type = '';
                } else {
                    $type = $aType[$i];
                }

                $input = $adorePrefix . $type;
                $pid = $name = $rt_price = $ws_price = $price = '';
                $css_healthy = 'style="background-color:gray;"';
                if (isset($adores[$input])) {
                    //echo '<pre>'; print_r($adores[$input]); echo '</pre>';
                    $pid = $adores[$input]['product_id'];
                    $price = $adores[$input]['rt_price'];
                    $name = $adores[$input]['name'];
                    $css_healthy = '';
                }
                
                $value = NULL;
                if ( isset($sales[$input]) ) {
                    $value = $sales[$input]['order_quantity'];
                    if ( substr($value,-2) == '00' ) $value = (int)$value;
                }
                ?>

                <td>
                    <?php 
                    if ( $type != '' ) {
                    ?>
                    <input type='text' value="<?php echo $value; ?>" class='adores-input input-small'
                        data-id=<?php echo $pid; ?> 
                        data-price=<?php echo $price; ?>
                        data-key="<?php echo $input; ?>" 
                        data-name="<?php echo $name; ?>" 
                        <?php echo $css_healthy; ?> placeholder="<?php echo $type; ?>" />
                    <?php
                    }
                    ?>
                </td>

            <?php
            }   
            ?>
        </tr>
        </tr>
        <?php
        }   // end foreach $aAdore[3]
        ?>

    
    </table>
</div>
