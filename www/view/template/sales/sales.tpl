<?php

// Share same code with invoice one
$isInvoiceMode = (isset($_REQUEST['invoice'])) ? true : false;
if ( $invoice_no ) $isInvoiceMode = true;
?>
<pre style='color:red;display:none;' id='notice'>
</pre>
<div id='product-list-panel'>
	<table id='product-sales-list' class="tablesorter">
		<thead>
		<tr>
			<th>Item.No</th>
			<th>Description</th>
			<th>Unit.Price</th>
			<th>Inventory</th>
			<th>Qty.</th>
			<?php //if($isInvoiceMode) { ?>
			<th>Shipped</th>
			<?php //} ?>
			<th>Discount</th>
			<th>Total</th>
			<th></th>
		</tr>
		</thead>

		<?php
		if (isset($sales)) {
		    //echo '<pre>'; print_r($sales); echo '</pre>'; exit;
			echo '<tbody>';
			$idx = 1;
			foreach($sales as $key => $sale){
				$description = htmlspecialchars_decode($sale['description']);
		?>
		<tr>
			<td>
				<input type='text' name='m[]' value='<?php echo $sale['model']; ?>' class='input-mini'/>
				<input type='hidden' name='id[]' value='<?php echo $sale['product_id']; ?>' />
			</td>
			<td>
				<input type='text' id='desc[]' value="<?php echo $description; ?>" class='input-xlarge' readonly/>
			</td>
			<td>
				<input type='text' name='p[]' class='input-mini right' value='<?php echo $sale['price1']; ?>'/>
			</td>
			<td>
				<input type='text' name='i[]' class='input-xmini right' readonly value='<?php echo $sale['inventory']; ?>' />
			</td>
			<td>
			    <?php
			    $o = ( substr($sale['order_quantity'],-2) == '00' ) ? round($sale['order_quantity']) : $sale['order_quantity'] ;
			    ?>
				<input type='text' name='o[]' class='input-xmini right' value='<?php echo $o; ?>'/>
			</td>
			<?php //if($isInvoiceMode) { ?>
			<td>
				<?php
			    $s = ( substr($sale['shipped'],-2) == '00' ) ? round($sale['shipped']) : $sale['shipped'] ;
			    ?>
				<input type='text' name='s[]' value='<?php echo $s; ?>' class='input-xmini right'/>
			</td>
			<?php //} ?>
			<td>
				<input type='text' name='d[]' class='input-xmini right' value='<?php echo $sale['discount']; ?>' />
			</td>
			<td>
				<input type='text' name='t[]' class='input-mini right' readonly value='<?php echo $sale['total_price']; ?>' />
			</td>
			<td class='td-btn'>
				<button class='btn del-sale-row'>Del</button>
			</td>
		</tr>
		<?
		        $idx++;
			}	// end loop
			echo '</tbody>';
		}	// isset($sales)
		?>

	</table>
</div>

<div id='product-atc-container'>
	<div id='product-control-container'>
		<!--button id="flush-product-cache" style='visibility:hidden'></button-->
		<!--button class="btn" id="product-insert">New Product</button-->
		<input id='product-atc' type='text' class='input-large' \>
	</div>
	
	<div id='total-order-container'>
		<input type='text' name='total_order_price' value='<?php echo $total_order_price; ?>' class='input-mini right' placeholder='Total' style='background-color:orange;' readonly />
		<input type='text' name='store_discount_percent' value='<?php echo $store_discount_percent; ?>' class='input-xmini right' placeholder='D/C' />
		<input type='hidden' name='total_discount_price' value='<?php echo $total_discount_price; ?>'  />
		<input type='text' name='subtotal_order_price' value='<?php echo $subtotal_order_price; ?>' class='input-mini right' placeholder='SubTotal' readonly style='background-color:orange;' />
		<button id="submit-order" class='btn btn-primary <?php if ($isInvoiceMode) echo "invoice"; ?>'>
			<?php echo ($isInvoiceMode) ? 'Invoice' : 'Submit' ; ?>
		</button>

		<select name='status'>
		    <option value='1' <?php if ( $status == '1') echo 'selected'; ?>>Order</option>
		    <option value='0' <?php if ( $status == '0') echo 'selected'; ?>>Hold</option>
		</select>
	</div>
</div>

<?php

    require_once('view/template/sales/tee.tpl');
    require_once('view/template/sales/stocking.tpl');
    require_once('view/template/sales/hats.tpl');
    require_once('view/template/sales/adore.tpl');
    //require_once('view/template/sales/belt.tpl');
?>

<div id='product-detail-panel'>
	<table>
		<tr>
			<td>Item.No</td>
			<td>Description</td>
			<td>Unit.Price</td>
			<td>Inventory</td>
			<td>Qty.</td>
			<?php //if($isInvoiceMode) { ?>
			<td>Shipped</td>
			<?php //} ?>
			<td>Discount</td>
			<td>Total</td>
			<td></td>
		</tr>
		<tr id="product-apply-row">
			<td>
				<input type='text' name='m[]' class='input-mini' readonly/>
				<input type='hidden' name='id[]'/>
			</td>
			<td>
				<input type='text' id='desc[]' class='input-xlarge' readonly/>
			</td>
			<td>
				<input type='text' name='p[]' value='0' class='input-mini right'/>
			</td>
			<td>
				<input type='text' name='i[]' value='0' class='input-xmini right' readonly />
			</td>
			<td>
				<input type='text' name='o[]' value='0' class='input-xmini right'/>
			</td>
			<?php //if($isInvoiceMode) { ?>
			<td>
				<input type='text' name='s[]' value='0' class='input-xmini right'/>
			</td>
			<?php //} ?>
			<td>
				<input type='text' name='d[]' value='0' class='input-xmini right'/>
			</td>
			<td>
				<input type='text' name='t[]' value='0' class='input-mini right' readonly />
			</td>
			<td class='td-btn'>
				<button id="apply-product" class='btn' style='display:none;'>Add</button>
			</td>
		</tr>
	</table>
</div>
<button id="flush-product-cache" style='display:none;'></button>
