<?php
require_once DIR_DATA . 'products/stocking.cat';

//echo '<pre>'; print_r($stockings); echo '</pre>'; exit;

$stocking_css = 'style="display:block;"';
if (isset($_GET['stockings'])) {
    $stocking_css = 'style="display:block;"';
}
?>
<div id='order-stocking' <?php echo $stocking_css; ?>>
    <button id='btn-stocking' class='btn custom-btn'>
        Stockings
    </button>
</div>

<div id='stocking-table' style='display:none;'>
<?php
//echo '<pre>'; print_r($stockings); echo '</pre>';
?>

<style>
.stocking-table td {
    border: 0.5px dotted gray;
}

.stocking-table input[type="text"]{
    width: 40px;
    height:15px;
    text-align: right;
}
</style>

<table style="width:100%;" class='stocking-table'>
    <tr>
        <th>LEDA</th>
        <th colspan='4'>#525</th>
        <th>#426</th>
        <th>#427</th>
        <th>#428</th>
    </tr>
    <tr>
        <th>COLOR</th>
        <?php
        foreach ( $aStockingSize['LD'] as $sizeKey => $sizeVal ) {
        ?>
        <th><?php echo $sizeVal; ?></th>
        <?php
        }   // end for $aStockingSize
        ?>
    </tr>
    <?php
    foreach ( $aStockingColor['LD'] as $colorKey => $colorVal ) {
    ?>

        <tr>
            <td><?php echo $colorVal; ?></td>
            <?php
            foreach ( $aStockingSize['LD'] as $sizeKey => $sizeVal ) {
                $key = $stockingPrefix . 'LD' . $sizeKey . $colorKey;
                //echo '<pre>'; print_r($key); echo '</pre>'; 
                $pid = $name = $rt_price = $ws_price = $price = '';
                $css_healthy = 'style="background-color:gray;"';
                if (isset($stockings[$key])) {
                    //echo '<pre>'; print_r($stockings[$key]); echo '</pre>';
                    $pid = $stockings[$key]['product_id'];
                    $price = $stockings[$key]['rt_price'];
                    $name = $stockings[$key]['name'];
                    $css_healthy = '';
                }
                
                $value = NULL;
                if ( isset($sales[$key]) ) {
                    $value = $sales[$key]['order_quantity'];
                    if ( substr($value,-2) == '00' ) $value = (int)$value;
                }
            ?>
            <td>
                
                <input type='text' value="<?php echo $value; ?>" class='stocking-input'
                    data-id=<?php echo $pid; ?> 
                    data-price=<?php echo $price; ?>
                    data-name="<?php echo $name; ?>" 
                    data-key="<?php echo $key; ?>" 
                    <?php echo $css_healthy; ?> 
                    placeholder="<?php echo $sizeVal; ?>">
            </td>
            <?php
            }   // end for $aStockingSize
            ?>
        </tr>

        
    <?php
    }   // end foreach LD
    ?>

    <!-- Normal -->
    <tr>
        <th>COLOR</th>
        <?php
        foreach ( $aStockingSize['NM'] as $sizeKey => $sizeVal ) {
        ?>
        <th><?php echo $sizeVal; ?></th>
        <?php
        }   // end for $aStockingSize
        ?>
    </tr>

    <?php
    foreach ( $aStockingColor['NM'] as $colorKey => $colorVal ) {
    ?>
        <tr>
            <td><?php echo $colorVal; ?></td>
            <?php
            foreach ( $aStockingSize['NM'] as $sizeKey => $sizeVal ) {
                $key = $stockingPrefix . $sizeKey . $colorKey;
                //echo '<pre>'; print_r($key); echo '</pre>'; 
                $pid = $name = $rt_price = $ws_price = $price = '';
                $css_healthy = 'style="background-color:gray;"';
                if (isset($stockings[$key])) {
                    //echo '<pre>'; print_r($stockings[$key]); echo '</pre>';
                    $pid = $stockings[$key]['product_id'];
                    $price = $stockings[$key]['rt_price'];
                    $name = $stockings[$key]['name'];
                    $css_healthy = '';
                }
                
                $value = NULL;
                if ( isset($sales[$key]) ) {
                    $value = $sales[$key]['order_quantity'];
                    if ( substr($value,-2) == '00' ) $value = (int)$value;
                }
                
            ?>
            
            <td>
                <input type='text' name="" value="<?php echo $value; ?>" class='stocking-input' 
                    data-id=<?php echo $pid; ?> 
                    data-price=<?php echo $price; ?> 
                    data-name="<?php echo $name; ?>" 
                    data-key="<?php echo $key; ?>" 
                    <?php echo $css_healthy; ?> 
                    placeholder="<?php echo $sizeVal; ?>">
            </td>

        <?php
        }   // end for $aStockingSize
        ?>
        </tr>
    <?php
    }   // end foreach LD
    ?>
</table>
</div>