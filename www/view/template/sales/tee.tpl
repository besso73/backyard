<style>
.tee-table td {
    border: 0.5px dotted gray;
}

.tee-table input[type="text"]{
    width: 40px;
    height:15px;
    text-align: right;
}
</style>

<?php
require_once DIR_DATA . 'products/tees.cat';

$tee_css = 'style="display:block;"';
if (isset($_GET['tee'])) {
    $tee_css = 'style="display:block;"';
}
?>

<div id='order-tee' <?php echo $tee_css; ?>>
    <button id='btn-tee' class='btn custom-btn'>
        Tee Shirts
    </button>
</div>

<div id='tee-table' style='display:none;'>

    <table style="width:100%;" class='tee-table'>
        <tr>
            <th>STYLE</th>
            <th>COLOR</th>
            <?php
            foreach ( $aTeeSize as $size ) {
            ?>
            <th><?php echo $size; ?></th>
            <?php
            }   // end for $aTeeSize
            ?>
        </tr>
        <?php
        foreach ( $aTeeType as $typeKey => $typeVal ) {
        ?>
            <?php
            foreach ( $aTeeColor[$typeKey] as $colorKey => $colorVal ) {
            ?>
            
            <tr>
                <td><?php echo $typeVal; ?></td>
                <td><?php echo $colorVal; ?></td>
                <?php
                foreach ( $aTeeSize as $size ) {
                    $input = $teePrefix . $typeKey . $colorKey . $size;
                    
                    $pid = $name = $rt_price = $ws_price = $price = '';
                    $css_healthy = 'style="background-color:gray;"';
                    if (isset($tees[$input])) {
                        //echo '<pre>'; print_r($tees[$input]); echo '</pre>';
                        $pid = $tees[$input]['product_id'];
                        $price = $tees[$input]['rt_price'];
                        $name = $tees[$input]['name'];
                        $css_healthy = '';
                    }
                    if (is_numeric($size)){
                        $sizeLabel = $size . 'XL';
                    } else{
                        $sizeLabel=$size;
                    } 
                    /*
                    if ($typeKey == 'KS') {
                        $x = 1;
                    } else {
                        $x = 0;
                    }
                    
                    */
                    
                    $value = NULL;
                    if ( isset($sales[$input]) ) {
                        $value = $sales[$input]['order_quantity'];
                        if ( substr($value,-2) == '00' ) $value = (int)$value;
                    }
                ?>
                <td>
                    
                    <input type='text' value="<?php echo $value; ?>" class='tee-input'
                        data-id=<?php echo $pid; ?> 
                        data-price=<?php echo $price; ?>
                        data-key="<?php echo $input; ?>" 
                        data-name="<?php echo $name; ?>" 
                        <?php echo $css_healthy; ?> placeholder="<?php echo $sizeLabel; ?>" />
                </td>
                <?php
                }   // end for $aTeeSize
                ?>
            </tr>
            <?php
            }   // end foreach $aTeeColor
            ?>
            
        <?php
        }   // end foreach $aTeeType
        ?>
    
    </table>
</div>