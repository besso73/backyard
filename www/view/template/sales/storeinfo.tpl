<?php
$descLen = explode(chr(13),$description);
if( count($descLen) > 1 ){
    $descHeight = count( $descLen ) * 25;
    $descHeight = 'height:'.$descHeight.'px';
}
?>
<table id='storeinfo-table' cellpadding="0" cellspacing="0">
    <tr>
        <td align='left'>
            <input type='hidden' name='salesrep' value='<?php echo $salesrep; ?>' />
            <input type='hidden' name='store_id' value='<?php echo $store_id; ?>' />
            <input type='hidden' name='txid' value='<?php echo $txid; ?>' />
            <input type='hidden' name='mode' value='<?php echo $mode; ?>' />
            <input type='hidden' name='async' value='false' />
            <input type='hidden' name='ajax' value='0' />
            <input type="text" class='input-large' name='store_name' value="<?php echo $store_name; ?>" placeholder='Name' autocomplete="off" />
            <input type="text" name='accountno' value='<?php echo $accountno; ?>' class='input-small' placeholder="Account no" autocomplete="off" />
            <input type="text" name='storetype' value='<?php echo $storetype; ?>' class='input-mini hide' placeholder="Type" readonly />
            <?php
            if ('insert' == $mode) {
                $accountControlText = 'New';
                $account_class = 'account-new';
            } else {
                $accountControlText = 'Edit';
                $account_class = 'account-edit';
            }
            ?>
            <button id="account-insert" class="btn <?php echo $account_class; ?>"
             data-id="<?php echo $store_id; ?>"><?php echo $accountControlText; ?></button>
        </td>
    </tr>
    <tr>
        <td>
            <input type="text" class="input-medium" name='phone1' value='<?php echo $phone1; ?>' placeholder='Phone' />
            <input type="text" class="input-medium" name='fax' value='<?php echo $fax; ?>'  placeholder='Fax'  />
            <?php
            if ('update' == $mode) {
            ?>
            <button id="account-history" class="btn">History</button>
            <?php
            }
            ?>
        </td>
    </tr>
    <tr>
        <td class='context'>
            <input type="text" class="input-small" name='city' value='<?php echo $city; ?>' style='width:48%' placeholder='City'/>
            <input type="text" class="input-small" name='state' value='<?php echo $state; ?>' style='width:10%' placeholder='ST'/>
            <input type="text" class="input-small" name='zipcode' value='<?php echo $zipcode; ?>' style='width:24%' placeholder='Zipcode' />
        </td>
    </tr>
    <tr>
        <td>
            <input type="text" name='address1' value='<?php echo $address1; ?>' class='input-xlarge' placeholder="Address" />
            <input type="text" class='date_pick input-small' name='order_date' value='<?php echo $order_date; ?>' />
        </td>
    </tr>
    <tr>
        <td>
			<textarea name='description' style='min-height:70px;width:98%; scroll-y:auto;' placeholder='Add Meno Here'>
			    <?php echo $description; ?>
			</textarea>
        </td>
    </tr>
</table>




<script>
$('document').ready(function(){
	
	/*
    $('input[name=dc1],input[name=dc2]').bind('click',function(e){
      $this = $(this);
      $this.select();
    });
    
    $.fn.discount = function($total,$dc){
      $discount = $total * ( (100-$dc) / 100 );
      $discount = $discount.toFixed(2);
      return $discount;
    }
    */
    
    /*
    store level discount
    event : before all submit X -> all input change event
    deprecated : should controll all each one.
    */
    /*
    $('input[name=dc1],input[name=dc2]').bind('change',function(e){
      $el_order_price = $('input[name=order_price]');
      if($el_order_price.val() >0){
        alert('You should set Store Discount before ordering');
        return false;
      }
    
      // Store Description 
      $added = '';
      $msg = '';
    
      $el_desc = $('textarea[name=description]');
    
      $dc1 = $('input[name=dc1]').val();
      $dc1_desc = $('input[name=dc1_desc]').val();
      if($dc1 > 0){
        $accountno = $('input[name=accountno]').val();
        
        if( $accountno == 'IL9805' || $accountno == 'IL9800' ){
          $msg1 = '[ Pickup Discount ] ' + $dc1 + ' % ' + $dc1_desc +'\n';
        }else{
          $msg1 = '[ Store Discount 1 ] ' + $dc1 + ' % ' + $dc1_desc +'\n';
        }
        $ptn1 = /(\[ Store Discount 1 \].+\n?)/;
        if( $el_desc.val().match($ptn1) )  $el_desc.val( $el_desc.val().replace($ptn1,'') );
        $added = $added + $msg1;
      }
    
      $dc2 = $('input[name=dc2]').val();
      $dc2_desc = $('input[name=dc2_desc]').val();
      if($dc2 > 0){
        $msg2 = '[ Store Discount 2 ] ' + $dc2 + ' % ' + $dc2_desc +'\n';
        $ptn2 = /(\[ Store Discount 2 \].+\n?)/;
        if( $el_desc.val().match($ptn2) )  $el_desc.val( $el_desc.val().replace($ptn2,'') );
        $added = $added + $msg2;
      }
    
      if($added != ''){
        if($el_desc.html() != ''){
          $el_desc.val( $added + $('textarea[name=description]').val() );
        }else{
          $el_desc.val( $added );
        }
      }
    });
    */
    
    /***
    $.fn.storeDiscount = function(){
      $dc1 = $('input[name=dc1]').val();
      $dc2 = $('input[name=dc2]').val();
      $el_order_price = $('input[name=order_price]');
      $el_balance = $('input[name=balance]');
      $order_price = $el_order_price.val();
      
      if($dc1 > 0){
        $discounted1 = $.fn.discount($('input[name=order_price]').val(),$dc1);
        //$dc1_diff  = parseFloat($('input[name=order_price]').val() - $discounted1);
        //$dc1_diff = $dc1_diff.toFixed(2);
        $el_order_price.val($discounted1);
        $el_balance.val($discounted1);
      }
      if($dc2 > 0){
        $discounted2 = $.fn.discount($('input[name=order_price]').val(),$dc2);
        //$dc2_diff  = parseFloat($('input[name=order_price]').val() - $discounted2);
        //$dc2_diff = $dc2_diff.toFixed(2);
        $el_order_price.val($discounted2);
        $el_balance.val($discounted2);
      }
    }
    ***/
    
    /*
    $('textarea').autoResize({
      // On resize:
      onResize : function() {
          $(this).css({
            opacity:0.8
          });
      },
      // After resize:
      animateCallback : function() {
        $(this).css({
          opacity:1
        });
      },
      // Quite slow animation:
      animateDuration : 300,
      // More extra space:
      extraSpace :40
    });
    */
    
    /*
    $('textarea[name=description]')
    .css('min-height','50px')
    .css('overflow-y','auto');
    */

});
</script>
