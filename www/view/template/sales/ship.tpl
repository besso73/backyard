<table id="shipinfo-table" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <input type='hidden' name='ship_id' value='' />
            <input type='hidden' name='ship_user' value='' />
            <input type='hidden' name='invoice_no' value='<?php echo $invoice_no; ?>' />
            <input type="text" name='shipped_yn' value='Shipped' class='input-mini' readonly />
            <select class='input-small' name='ship_method'>
                <option value='ups' <?php if(strtolower($ship_method) == 'ups') echo "selected"; ?>>ups</option>
                <option value='trk' <?php if(strtolower($ship_method) == 'trk') echo "selected"; ?>>truck</option>
                <option value='del' <?php if(strtolower($ship_method) == 'del') echo "selected"; ?>>delivery</option>
                <option value='pck' <?php if(strtolower($ship_method) == 'pck') echo "selected"; ?>>pickup</option>
                <!--option value='ins' <?php if(strtolower($ship_method) == 'ins') echo "selected"; ?>>invoice only</option-->
            </select>
            <?php if('' != $txid) { ?>
            <button class='btn btn-warning' id='show-invoice'><?php echo (0 != $invoice_no) ? $invoice_no : 'Preview'; ?></button>
            <?php } ?>
        </td>
    </tr>
    <tr>
        <td>
            <select class='input-small' name='payment'>
                <option value='n3' <?php if(strtolower($payment) == 'n3') echo "selected"; ?>>Net30</option>
                <option value='n6' <?php if(strtolower($payment) == 'n6') echo "selected"; ?>>Net60</option>
                <option value='cc' <?php if(strtolower($payment) == 'cc') echo "selected"; ?> style='color:red;'>Cod</option>
                <!--option value='cm' <?php if(strtolower($payment) == 'cm') echo "selected"; ?> style='color:red;'>cod-m.order</option-->
                <!--option value='cd' <?php if(strtolower($payment) == 'cd') echo "selected"; ?>>card</option>
                <option value='pp' <?php if(strtolower($payment) == 'pp') echo "selected"; ?>>prepaid</option>
                <option value='pd' <?php if(strtolower($payment) == 'pd') echo "selected"; ?>>paid</option-->
                <option value='bb' <?php if(strtolower($payment) == 'bb') echo "selected"; ?>>Bill2Bill</option>
            </select>
            <input type='number' name='ship_cod' value='<?php echo $ship_cod; ?>'  class='input-mini' placehodler='COD'/>
            <!--input type='number' name='ship_lift' value='<?php echo $ship_lift; ?>' class='input-mini' placehodler='LIFT'/-->
			<input type="text" class='date_pick input-small' name='ship_appointment' value='<?php if($ship_appointment) { echo $ship_appointment; }else{ echo date('Y-m-d'); } ?>' size='8' />
        </td>
    </tr>
    <tr>
        <td>
			<textarea name='shipto' style='width:250px;height:150px;' placeholder='Add ShipTo Here'><?php echo str_replace('\n',chr(13),$shipto); ?></textarea>
        </td>
    </tr>
</table>

<!--span id='ship_history' class='small_btn'>Shipping History</span-->
<script>
/***
$(document).ready(function(){
    var $el_ship = $('#ship'),
        $el_ship_method = $el_ship.find('select[name=ship_method]'),
        $el_ship_lift = $el_ship.find('input[name=ship_lift]'),
        $el_ship_cod  = $el_ship.find('input[name=ship_cod]'),
        $el_payment = $el_ship.find('select[name=payment]');
    $.fn.addAmountBalance = function($add){
      var $el_order_price = $('input[name="order_price"]'),
          $el_balance     = $('input[name="balance"]');
      $order_price = parseFloat($el_order_price.val()) + parseFloat($add);
      $order_price = $order_price.toFixed(2);
      $el_order_price.val( $order_price );
      $balance = parseFloat($el_order_price.val()) - parseFloat($('input[name=payed_sum]').val());
      $balance = $balance.toFixed(2);
      $el_balance.val( $balance );
      $('#floatmenu').find('input[name=float_order_price]').val($order_price);
      $.fn.showOthersInPayment($add);
    }
    $.fn.showOthersInPayment = function($add){
      $('p.added_amount').html( '(' + $add + ')' );
    }
    // change Lift value on change of method and update Amount/Balance
    $el_payment.bind('change',function(e){
      $this = $(this);
      $add = 0;
      $payment = $this.val();
      if( $el_ship_cod.val() > 0 ){
        alert('COD value already exist \'' + $el_ship_cod.val() + '\' , Edit COD forcely');
        $el_ship_cod.css('background-color','#00FFFF');
        $el_ship_cod.focus();
        //return false;
      }
      if('cc' == $payment || 'cm' == $payment){
        $add = 10;
        $el_ship_cod.val($add);
        $.fn.addAmountBalance($add);
      }
      if('n3' == $payment){
        $add = 0;
        $el_ship_cod.val('0');
        $.fn.addAmountBalance($add);
      }
    });
    
    $el_ship_method.bind('change',function(e){
      //alert( $el_ship_method.val() );
      $this = $(this);
      $add = 0;
      $sl = $('input[name=ship_lift]').val();
      
      if( $sl > 0 ){
        if( $this.val() == 'ups' || $this.val() == 'del' || $this.val() == 'pck' ){
          alert('list charge is removed');
          $.fn.addAmountBalance( $sl * -1 );
          $el_ship_lift.val(0);
        }
      }
      if( $sl == 0 ){
        if( $this.val() == 'trk'){
          $sl = 5;
          alert('list charge is added');
          $.fn.addAmountBalance( $sl  );
          $el_ship_lift.val($sl);
        }
      }
    });
    
    // todo. thinking up JS event. i mapped only focusout for event-driven
    // exactly there are no way to remember just previous value so decided not to use change event
    // todo. reported sometimes error !
    // todo. IE focusing error , not focus well in sales items but cod still . so blocked all .select()
    $el_ship_cod
    .bind('focusin',function(){
      $prev = $(this).val();
      $(this).select();
    })
    .bind('keydown',function(e){
      if('13' == e.which){
        $existVal = $prev;
        $val = $(this).val();
        $ptn = /\d/;
        if( !$ptn.test($val) ){
          $(this).val('0'); return;
        }
        $add = parseFloat($val) - parseFloat($existVal);
        $add = $add.toFixed(2);
        $.fn.addAmountBalance($add);
        //$(this).select();
        $prev = $val;
      }
    })
    .bind('focusout',function(e){
      if($(this).val() == ""){  $(this).val(0); }
      $existVal = $prev;
      $val = $(this).val();
      $ptn = /\d/;
      if( !$ptn.test($val) ){
          $(this).val('0'); return;
      }
      $add = parseFloat($val) - parseFloat($existVal);
      $add = $add.toFixed(2);
      $.fn.addAmountBalance($add);
      //$(this).select();
      $prev = $val;
    });
    
    $el_ship_lift
    .bind('focusin',function(){
      $prev = $(this).val();
      $(this).select();
    })
    .bind('keydown',function(e){
      if('13' == e.which){
        $existVal = $prev;
        $val = $(this).val();
        $ptn = /\d/;
        if( !$ptn.test($val) ){
          $(this).val('0'); return;
        }
        $add = parseFloat($val) - parseFloat($existVal);
        $add = $add.toFixed(2);
        $.fn.addAmountBalance($add);
        //$(this).select();
        $prev = $val;
      }
    })
    .bind('focusout',function(e){
      if($(this).val() == ""){  $(this).val(0); }
      $existVal = $prev;
      $val = $(this).val();
      $ptn = /\d/;
      if( !$ptn.test($val) ){
        $(this).val('0'); return;
      }
      $add = parseFloat($val) - parseFloat($existVal);
      $add = $add.toFixed(2);
      $.fn.addAmountBalance($add);
      //$(this).select();
      $prev = $val;
    });
});
***/
</script>
