<style>
.hat-table td {
    border: 0.5px dotted gray;
}

.hat-table input[type="text"]{
    width: 40px;
    height:15px;
    text-align: right;
}
</style>

<?php
require_once DIR_DATA . 'products/hats.cat';

$hat_css = 'style="display:block;"';
if (isset($_GET['hat'])) {
    $hat_css = 'style="display:block;"';
}
?>

<div id='order-hats' <?php echo $hat_css; ?>>
    <button id='btn-hats' class='btn custom-btn'>
        Hats
    </button>
</div>

<div id='hats-table' style='display:none;'>

    <table style="width:100%;" class='hat-table'>
        <tr>
            <th>COLOR</th>
            <?php
            foreach ( $aHatType as $type ) {
            ?>
            <th><?php echo $type; ?></th>
            <?php
            }   
            ?>
        </tr>

        <?php
        
        foreach ( $aHatColor as $colorKey => $colorVal ) {
        ?>
        
        <tr>
            <td><?php echo $colorVal; ?></td>
            <?php
                foreach ( $aHatType as $typeKey => $typeVal ) {
                    $input = $hatPrefix . $typeKey . $colorKey;
                    
                    $pid = $name = $rt_price = $ws_price = $price = '';
                    $css_healthy = 'style="background-color:gray;"';
                    if (isset($hats[$input])) {
                        //echo '<pre>'; print_r($hats[$input]); echo '</pre>';
                        $pid = $hats[$input]['product_id'];
                        $price = $hats[$input]['rt_price'];
                        $name = $hats[$input]['name'];
                        $css_healthy = '';
                    }
                    
                    $value = NULL;
                    if ( isset($sales[$input]) ) {
                        $value = $sales[$input]['order_quantity'];
                        if ( substr($value,-2) == '00' ) $value = (int)$value;
                    }
                ?>
                <td>
                    
                    <input type='text' value="<?php echo $value; ?>" class='hats-input'
                        data-id=<?php echo $pid; ?> 
                        data-price=<?php echo $price; ?>
                        data-key="<?php echo $input; ?>" 
                        data-name="<?php echo $name; ?>" 
                        <?php echo $css_healthy; ?> placeholder="<?php echo $colorKey; ?>" />
                </td>
                <?php
                }
                ?>
            
        </tr>
        <?php
        }   // end foreach $aHatColor
        ?>

    
    </table>
</div>
