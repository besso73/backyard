<style>
.belt-table td {
    border: 0.5px dotted gray;
}

.belt-table input[type="text"]{
    width: 40px;
    height:15px;
    text-align: right;
}
</style>

<?php
require_once DIR_DATA . 'products/belts.cat';

$belt_css = 'style="display:block;"';
if (isset($_GET['belt'])) {
    $belt_css = 'style="display:block;"';
}
?>

<div id='order-belts' <?php echo $belt_css; ?>>
    <button id='btn-belts' class='btn custom-btn'>
        Belts
    </button>
</div>

<div id='belts-table' style='display:none;'>

    <table style="width:100%;" class='belt-table'>
        <tr>
            <th>COLOR</th>
            <?php
            foreach ( $aBeltSize as $sizeKey => $aSubmenu ) {
                $colSize = count($aSubmenu);
                echo "<th colspan='$colSize'>$sizeKey</th>";
            }   
            ?>

        </tr>
        <tr>
            <th></th>
            <?php
            foreach ( $aBeltSize as $sizeKey => $aSubmenu ) {
                foreach ( $aSubmenu as $m ) {
                    echo "<th>$m</th>"; 
                }
            }   
            ?>

        </tr>


        <?php
        foreach ( $aBeltColor as $colorKey => $colorVal ) {
        ?>
        
        <tr>
            <td><?php echo $colorVal; ?></td>
            <?php
                foreach ( $aBeltSize as $sizeKey => $aSubmenu ) {
                    foreach ( $aSubmenu as $subKey => $subVal ) {

                        $input = $beltPrefix . $sizeKey . $subKey . $colorKey;
                        
                        $pid = $name = $rt_price = $ws_price = $price = '';
                        $css_healthy = 'style="background-color:gray;"';
                        if (isset($belts[$input])) {
                            //echo '<pre>'; print_r($belts[$input]); echo '</pre>';
                            $pid = $belts[$input]['product_id'];
                            $price = $belts[$input]['rt_price'];
                            $name = $belts[$input]['name'];
                            $css_healthy = '';
                        }
                        
                        $value = NULL;
                        if ( isset($sales[$input]) ) {
                            $value = $sales[$input]['order_quantity'];
                            if ( substr($value,-2) == '00' ) $value = (int)$value;
                        }
                ?>
                <td>
                    
                    <input type='text' value="<?php echo $value; ?>" class='belt-input'
                        data-id=<?php echo $pid; ?> 
                        data-price=<?php echo $price; ?>
                        data-key="<?php echo $input; ?>" 
                        data-name="<?php echo $name; ?>" 
                        <?php echo $css_healthy; ?> placeholder="<?php echo $sizeLabel; ?>" />
                </td>
                <?php
                    }
                }
                ?>
            
        </tr>
        <?php
        }   // end foreach $aBeltColor
        ?>

    
    </table>
</div>
