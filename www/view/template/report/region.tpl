<?php echo $header; ?>
<?php if($error_warning){ ?><div class="warning"><?php echo $error_warning; ?></div><?php } ?>
<?php if ($success){ ?><div class="success"><?php echo $success; ?></div><?php } ?>

    <style type="text/css">
      #map_canvas {width: 100%; height: 600px; margin: 0; padding: 0;}
      .infowindow * {font-size: 90%; margin: 0}
    </style>
    <style type="text/css">
     .labels {  /* style for google map markerwithlabel */
       color: black;
       background-color: white;
       font-family: "Lucida Grande", "Arial", sans-serif;
       font-size: 11px;
       /* font-weight: bold; */
       text-align: center;
       width: 70px;     
       border: 2px solid black;
       white-space: nowrap;
     }
    </style>
    <!--script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script-->
    <script type="text/javascript" src="view/template/report/geoxml3.js"></script>
    <script type="text/javascript" src="view/template/report/label.js"></script>
    <script type="text/javascript">
      function initialize(){
        var myLatlng = new google.maps.LatLng(39.397, -100.644);
        var myOptions = {
          zoom: 7,
          center: myLatlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
        var geoXml = new geoXML3.parser({map: map});
        geoXml.parse('view/template/report/KML/<?php echo $kmlFilename ?>');
      };
    </script>
<div class="box">
  
  <div class="heading">
    <h1>From <?php echo $filter_from ?> To <?php echo $filter_to ?></h1>
  </div>
  <div class="content">
    <table style='width:600px;'>
      <tr class="filter np">
        <td class="center" colspan='2' style='width:120px'>
          <div>
            <input type="text" class='date_pick' name="filter_from" value="<?php echo $filter_from; ?>" style='width:70px;' /> -
            <input type="text" class='date_pick' name="filter_to" value="<?php echo $filter_to; ?>" style='width:70px;' />
          </div>
          <div>
            <a href='<?php echo $lnk_pmonth ?>'>
            <?php echo $pmonth_label ?>
            </a>&nbsp;
            <a href='<?php echo $lnk_tmonth ?>'>
            <?php echo $tmonth_label ?>
            </a>&nbsp;
            <a href='<?php echo $lnk_nmonth ?>'>
            <?php echo $nmonth_label ?>
            </a>
          </div>
        </td>
        <td colspan='8'>
          <a onclick='filter();' class="button btn_filter">
            <span>Search</span>
          </a>
        </td>
      </tr>
    </table>
    <div id="map_canvas"></div>
    <script type="text/javascript">
      var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
      document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
      try{
        var pageTracker = _gat._getTracker("UA-1556555-8");
        pageTracker._trackPageview();
      }catch(err){}
    </script>
  </div>
</div>
<script>
window.onload = initialize();

$(document).ready(function(){
  $.fn.filter = function(){
  	url = '/report/region&token=<?php echo $token; ?>';
  	var filter_from = $('input[name=\'filter_from\']').attr('value');
  	if(filter_from)  url += '&filter_from=' + encodeURIComponent(filter_from);
  	var filter_to = $('input[name=\'filter_to\']').attr('value');
  	if(filter_to)  url += '&filter_to=' + encodeURIComponent(filter_to);
  	location = url;
  }

  // date picker binding
  $('input.date_pick').bind('focusin',function(event){
    var $tgt = $(event.target);
    if($tgt.is('input.date_pick')){
      //$(".date-pick").datePicker({startDate:'01/01/1996'});
      $(".date_pick").datepicker({
        clickInput:true,
        createButton:false,
        startDate:'2000-01-01'
      });
    }
  });
  $('.btn_filter').bind('click',function(e){
    $.fn.filter();
  });
  
});
</script>
<?php echo $footer; ?>






