<style>
.title td{
  font-size:13px;
  border:1px solid black;
  padding:5px;
  font-weight:bold;
  text-align:center;
}
.content td{
  font-size:13px;
  border:1px dotted black;
  padding:5px;
}
.total td{
  font-size:13px;
  border:1px dotted black;
  padding:5px;
  background-color:#e6e6e6;
}
td.right{
  text-align:right;
}
@media print{
  @page{
    size: landscape;
    margin-left:30cm;
  }
}
</style>

<?php
$aCatBase = $this->config->getCatalogBase();
?>
<h3><?php echo $from ?> ~ <?php echo $to ?></h3>
<table cellspacing='0' cellpadding='0'>
  <tr class='title'>
    <td>/</td>
    <td>Rep</td>
    <?php
    foreach($aCatBase as $base => $cat){
    ?>
    <td>
      <?php echo $base; ?>
    </td>
    <?php
    }
    ?>
    <td>Total</td>
  </tr>
  <?php
  $i = 1;
  $aCatSum = array();
  foreach($aProduct as $key => $row){
    $sum = 0;
  ?>
  <tr class='content'>
    <td><?php echo $i ?></td>
    <td><?php echo $key ?></td>
    <?php
    foreach($aCatBase as $base => $cat){
    ?>
    <td class='right'>
      <?php
      if(isset($row[$base])){
        echo number_format($row[$base],2);
        $sum += $row[$base];
        if( !isset($aCatSum[$base]) ) $aCatSum[$base] = 0;
        $aCatSum[$base] += $row[$base];
      }
      ?>
    </td>
    <?php
    }
    ?>
    <td class='right'><?php echo number_format($sum,2) ?></td>
  </tr>
  <?php
    $i++;
  }
  ?>

  <?php
  // for percent we need to get sum
  $catSum = array_sum($aCatSum);
  ?>
  <tr class='total'>
    <td>GT</td>
    <td></td>
    <?php
    $sum = 0;
    $aChart = array();
    foreach($aCatBase as $base => $cat){
    ?>
    <td class='right'>
      <?php
      if(isset($aCatSum[$base])){
        echo number_format($aCatSum[$base],2);
        $sum += $aCatSum[$base];
        echo '<br/>';
        echo '<font color="blue">';
        echo round( ( $aCatSum[$base] / $catSum ) * 100 , 2 ).'%';
        echo '</font>';
        $aChart[$base] = round( ( $aCatSum[$base] / $catSum ) * 100 , 2 );
      }
      ?>
    </td>
    <?php
    }
    ?>
    <td class='right'><?php echo number_format($sum,2) ?></td>
  </tr>


</table>

<?php

function implode_key($glue = "", $pieces = array()){
  $keys = array_keys($pieces);
  return implode($glue, $keys); 
}

//$this->log->aPrint( $aChart );
//$chl = implode_key('|',$aChart);
//$chd = $chg = implode(',',$aChart);

arsort($aChart);

$html = '';
foreach($aChart as $key => $val){
  $html .= '["' . $key . '",' . $val . '],';
}
$html = substr($html,0,-1);

?>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
// Load the Visualization API and the piechart package.
google.load('visualization', '1.0', {'packages':['corechart']});
// Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawChart);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawChart(){
  // Create the data table.
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Rep');
  data.addColumn('number', '%');
  data.addRows([<?php echo $html ?>]);
  // Set chart options
  var options = {'title':'<?php echo $from ?> ~ <?php echo $to ?>','width':800,'height':600};
  // Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
  chart.draw(data, options);
}
</script>
<div id="chart_div"></div>
