<style>
@media print {
  body, td, th, input, select, textarea, option, optgroup {
    font-size: 14px;
  }
  #header,#menu,#footer{
    display:none;
  }
  #content .left,.right{
    display:none;
  }
  #content .heading div.buttons{
    display:none;
  }
  .np{
    display:none;
  }
} 
</style>
<?php
  $heading_title = 'Montly';
  $export = '';
  $lnk_action = '';
?>
<?php echo $header; ?>
<?php if($error_warning){ ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success){ ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>

<!-- atc -->
<script type='text/javascript' src='view/template/report/atc/jquery/jquery.metadata.js'></script>
<script type='text/javascript' src='view/template/report/atc/src/jquery.auto-complete.js'></script>
<link rel='stylesheet' type='text/css' href='view/template/report/atc/src/jquery.auto-complete.css' />

<style>
.box {
  z-index:10;
}
.content .name_in_list{
  color:purple;
  cursor:pointer;
}
#detail{
  position : absolute;
  top: 100px;
  left: 100px;
  visibility:hidden;
  border: 1px dotted green;
  z-index:2;
}
#content{
  width:900px;
}
.box .content{
  border:none;
}

body,td{
  color: #000000;
  font-family: Arial,Helvetica,sans-serif;
  font-size: 35px;
}

td{
  font-size: 40px;
}

.atc{
  height:60px;
  width:300px;
  font-size:50px;
}
</style>

<div class="box">
  <div class="content">
    <table class="table">
      <tbody>
        <tr class="filter np">
          <td class="left" colspan='10' style='width:120px'>
            <input type=text name='filter_accountno' style='' class='atc' />
          </td>
        </tr>
        <tr style='height:20px'><td colspan='10'></td></tr>
          <?php 
          if( count($stat) > 0 ){ 
          ?>
          <?php
          $total = $order_sum = 0;
          
          foreach ($stat as $txid => $row){
            //$this->log->aPrint( $row );
          ?>
          <tr style='background-color:<?php echo $bg_td ?>'>
            <td class='center' colspan=8 style='text-align:left;'>
              <a class='prod_detail'><?php echo $txid ?></a>
              <table id='<?php echo $txid ?>' style='margin-left:40px;display:block;'>
                <?php
                foreach($row as $k => $v){
                  
                  $order_price = $v['order_price'];
                  $order_date = $v['order_date'];
                ?>
                <tr>
                  <td><?php echo $v['model'] ?></td>
                  <td><?php echo $v['name'] ?></td>
                  <td><?php echo $v['sum'] ?></td>
                  <td><?php echo $v['order_quantity'] ?></td>
                </tr>
                <?php
                }
                ?>
              </table>
            </td>
          </tr>
          <?php 
            $total += $order_price;
          } // end foreach 
          ?>
          <tr style='background-color:#e2e2e2;'>
            <td class='center' colspan='8'><?php echo $total; ?></td>
          </tr>
          <?php }else{ ?>
          <tr>
            <td class="center" colspan="8">No Result</td>
          </tr>
          <?php } ?>
          <tr style='height:40px'><td colspan='8'></td></tr>
    
        
        
        
      </tbody>
    </table>
        
  </div>
</div>  
        
<script type="text/javascript">
$(document).ready(function(){
  /*    
  $('input[name=filter_accountno]').bind('keydown',function(e){
    $tgt = $(e.target);
    if( e.keyCode == '13' ){  
      url = '/report/account&accountno='+$(this).val();	
      location.href = url;
    }   
  });   
  */    
        
        
  $('input.atc')
  .focus()
  .bind('keydown',function(e){
    $tgt = $(e.target);
    if( e.keyCode == '13' ){  
      $accountno = $(this).val();
      url = '/report/account&accountno=' + $accountno;
			//$('.heading>h1').html(url);
			location.href = url;
    }
  });

  $('.prod_detail').bind('click',function(e){
    $tgt = $(e.target);
    $group = $tgt.text();
    $pnt = $tgt.parents('td');
    $display = $pnt.find('table').css('display');
    if($display == 'none'){
      $pnt.find('table').css('display','block');
    }
    if($display == 'block'){
      $pnt.find('table').css('display','none');
    }
  });
});     
</script>
<?php echo $footer; ?>
