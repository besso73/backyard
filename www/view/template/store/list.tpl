<?php echo $header; ?>
<?php if($error_warning){ ?><div class="warning"><?php echo $error_warning; ?></div><?php } ?>
<?php if($success){ ?><div class="success"><?php echo $success; ?></div><?php } ?>

<style>
.box{ z-index:10; }
.content .name_in_list{ color:purple; cursor:pointer; }
#detail{  position:absolute;  top:100px;  left:100px; visibility:hidden;  border:1px dotted green;  z-index:2;  }
td.accountno_in_list{
    padding-left: 20px;
}
td.name_in_list{
    width: 200px;
}
</style>

<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry"></script>

<div class="box">
    <div class="heading">
        <h1>Account</h1>
        <div class="buttons">
            <?php if(count($total) > 0) { ?>
            <a onclick="location = '<?php echo $export; ?>'" class="btn"><?php echo $total; ?> Export</a>
            <?php } ?>
            <!--a class="btn" id="account-insert">Insert</a-->
            <a onclick="$('form').submit();" class="btn">Delete</a>
        </div>
    </div>
    <?php
    $filter_name = $filter_storetype = $filter_city = $filter_state = '';
    if(!$filter_status) $filter_status = '';
    ?>
    <div class="content">
        <form action="<?php echo $lnk_delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="table">
            <thead>
            <tr>
                <td width="1" style="text-align:center;">
                	<input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" 
                	style='margin-top:0;' /></td>
                <td class="left">ID</td>
                <td class="left">
                    <?php if($sort == 'name'){ ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>">Name</a>
                    <?php }else{ ?>
                    <a href="<?php echo $sort_name; ?>">Name</a>
                    <?php } ?>
                </td>
                <td class="left">
                    <?php if($sort == 'storetype'){ ?>
                    <a href="<?php echo $sort_storetype; ?>" class="<?php echo strtolower($order); ?>">Type</a>
                    <?php }else{ ?>
                    <a href="<?php echo $sort_storetype; ?>">W/R</a>
                    <?php } ?>
                </td>
                <td class="left">City</td>
                <td class="left">State</td>
                <td class="left">Phone</td>
                <td class="center">
                    <?php if($sort == 'salesrep'){ ?>
                    <a href="<?php echo $sort_salesrep; ?>" class="<?php echo strtolower($order); ?>">Rep</a>
                    <?php }else{ ?>
                    <a href="<?php echo $sort_salesrep; ?>">Rep</a>
                    <?php } ?>
                </td>
                <td class="left">
                    <?php if($sort == 'status'){ ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                    <?php }else{ ?>
                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                    <?php } ?>
                </td>
                <td class="right"><?php echo $column_action; ?></td>
            </tr>
            </thead>
            <tbody>
            <tr class="filter">
                <td colspan='2'>
                	<input type="text" class="input-small" name="filter_accountno" value="<?php echo $filter_accountno; ?>" /></td>
                <td><input type="text" class="input-small" name="filter_name" value="<?php echo $filter_name; ?>"  /></td>
                <td>
                  <select name='filter_storetype' class="input-small">
                    <?php if(strtolower($filter_storetype) == 'w'){ ?>
                    <option value="">--</option>
                    <option value="w" selected="selected">W</option>
                    <option value="r">R</option>
                    <?php } elseif(strtolower($filter_storetype) == 'r'){ ?>
                    <option value="">--</option>
                    <option value="w">W</option>
                    <option value="r" selected="selected">R</option>
                    <?php }else{ ?>
                    <option value="" selected="selected">--</option>
                    <option value="w">W</option>
                    <option value="r">R</option>
                    <?php }?>
                  </select>
                </td>
                <td><input type="text" class="input-small" name="filter_city" value="<?php echo $filter_city; ?>"  /></td>
                <td><input type="text" class="input-small" name="filter_state" value="<?php echo $filter_state; ?>"  /></td>
                <td align="left"><input type="text" class="input-small" name="filter_phone1" value="<?php echo $filter_phone1; ?>" style="text-align: left;"  /></td>
                <td align="left">
                  <?php
                  if($this->user->getGroupID()){
                  ?>
                    <input type="text" name="filter_salesrep" value="<?php echo $filter_salesrep; ?>" style="text-align:center;width:30px;" />
                  <?php
                  }else{
                    echo $this->user->getUserName();
                  }
                  ?>
                </td>
                <td>
                  <?php $aStoreCode = $this->config->getStoreStatus();  ?>
                  <select class="input-small" name="filter_status">
                    <option value="" <?php if(''==$filter_status) echo 'selected'; ?>>All</option>
                    <option value="0" <?php if('0'==$filter_status) echo 'selected'; ?>><?php echo $aStoreCode['0']; ?></option>
                    <option value="1" <?php if('1'==$filter_status) echo 'selected'; ?>><?php echo $aStoreCode['1']; ?></option>
                    <option value="2" <?php if('2'==$filter_status) echo 'selected'; ?>><?php echo $aStoreCode['2']; ?></option>
                    <!--option value="3" <?php if('3'==$filter_status) echo 'selected'; ?>><?php echo $aStoreCode['3']; ?></option-->
                    <option value="9" <?php if('9'==$filter_status) echo 'selected'; ?>><?php echo $aStoreCode['9']; ?></option>
                  </select>
                </td>
                <td align="right"><a class="btn btn_filter"><?php echo $button_filter; ?></a></td>
            </tr>
            <?php if($store){ ?>
            <?php
            //$this->log->aPrint( $store );
            foreach ($store as $row){
                $bg_td = '';
                if( $row['status'] == '0' ) $bg_td = 'gray';
                if( $row['status'] == '9' ) $bg_td = '#e2e2e2';
            ?>
            <tr style='background-color:<?php echo $bg_td ?>'>
                <td style="text-align: center;"><?php if($row['selected']){ ?>
                    <input type="checkbox" class='id_in_list' name="selected[]" value="<?php echo $row['id']; ?>" checked="checked" style='margin-top:0;' />
                    <?php }else{ ?>
                    <input type="checkbox" class='id_in_list' name="selected[]" value="<?php echo $row['id']; ?>" style='margin-top:0;' />
                    <?php } ?>
                    <input type='hidden' name='view' value='proxy' />
                    <input type='hidden' class='address1_in_list' name='address1' value='<?php echo $row['address1']; ?>' />
                    <input type='hidden' class='zipcode_in_list' name='zipcode' value='<?php echo $row['zipcode']; ?>' />
                    <input type='hidden' class='fax_in_list' name='fax' value='<?php echo $row['fax']; ?>' />
                </td>
                <td class='left accountno_in_list'>
                  <?php echo $row['accountno']; ?>
                </td>
                
                <td class='name_in_list'><?php echo $row['name']; ?></td>
                <td class='center storetype_in_list'><?php echo $row['storetype']; ?></td>
                <td class='center city_in_list'><?php echo $row['city']; ?></td>
                <td class='center state_in_list'><?php echo $row['state']; ?></td>
                <td class='center phone1_in_list'><?php echo $row['phone1']; ?></td>
                <td class='center salesrep_in_list'><?php echo $row['salesrep']; ?></td>
                <td class="left" class='status_in_list'>
                	<?php //echo $row['status']; ?>
                	<?php echo $aStoreCode[$row['status']]; ?>
                </td>
                <td class="center"><a class='btn update-account2' data-id="<?php echo $row['id']; ?>">Edit</a></td>
            </tr>
            <?php } // end foreach ?>
            <?php }else{ ?><tr><td class="center" colspan="11">No Result</td></tr><?php } ?>
            </tbody>
        </table>
        </form>
        <div class="pagination"><?php echo $pagination; ?></div>
    </div>
</div>
<!-- common detail div -->
<div id='detail' class='ui-widget-content'></div>
<script type="text/javascript">
$(document).ready(function(){
    $.fn.filter = function(){

    	var url = '/store/list&token=<?php echo $token; ?>';

		var filter_name = $('input[name=\'filter_name\']').attr('value');

    	if(filter_name) {
    	    url += '&filter_name=' + window.btoa(filter_name);
    	}
    	var filter_accountno = $('input[name=\'filter_accountno\']').attr('value');
    	if(filter_accountno)  url += '&filter_accountno=' + encodeURIComponent(filter_accountno);
        var filter_storetype = $('select[name=\'filter_storetype\']').attr('value');
        if(filter_storetype != '')  url += '&filter_storetype=' + encodeURIComponent(filter_storetype);
    	var filter_city = $('input[name=\'filter_city\']').attr('value');
    	if(filter_city) url += '&filter_city=' + encodeURIComponent(filter_city);
    	var filter_state = $('input[name=\'filter_state\']').attr('value');
    	if(filter_state)  url += '&filter_state=' + encodeURIComponent(filter_state);
    	var filter_phone1 = $('input[name=\'filter_phone1\']').attr('value');
    	if(filter_phone1) url += '&filter_phone1=' + encodeURIComponent(filter_phone1);
    	var filter_salesrep = $('input[name=\'filter_salesrep\']').attr('value');
    	if(filter_salesrep) url += '&filter_salesrep=' + encodeURIComponent(filter_salesrep);
    	var filter_balance = $('input[name=\'filter_balance\']').attr('value');
    	if(filter_balance) url += '&filter_balance=' + encodeURIComponent(filter_balance);
    	var filter_status = $('select[name=\'filter_status\']').attr('value');
    	if(filter_status != '*')  url += '&filter_status=' + encodeURIComponent(filter_status);
    	location = url;
    }
    $('.btn_filter').bind('click',function(e){  $.fn.filter(); });
    $('#form input').keydown(function(e){ if(e.keyCode == 13) $.fn.filter(); });
    $('.list').click(function(event){
        var $tgt = $(event.target);
        if($tgt.is('a.edit>span')){
            var $pnt = $tgt.parents('tr'),
                $ele_chkbox = $pnt.find('.id_in_list'),
                $store_id = $ele_chkbox.val();
            $.ajax({
                type:'get', dataType:'html',
                url:'/store/list/callUpdatePannel',
                data:'token=<?php echo $token; ?>&store_id=' + $store_id,
                success:function(html){
                    $('#detail').css('visibility','visible');
                    $('#detail').html(html);
                    //$('#detail').draggable();
                }
            });
        }
    }); // end of click event
	
    $.fn.printLabel = function($idlist){
        $param = '&idlist=' + $idlist,
        $url='/store/list/printLabel&token=<?php echo $token; ?>' + $param;
        window.open($url);
    }
    $(window).bind('scroll',function(){
        var winP = $(window).scrollTop();
        var mapCss = {  "position":"absolute","top":winP +'px'  }
        $("#detail").css(mapCss);
    });
});
</script>

<?php echo $footer; ?>
