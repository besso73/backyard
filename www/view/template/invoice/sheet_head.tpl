	<?php if ($page > 1) echo '<br/>'; ?>
	<table id='top'>
        <tr>
            <td id='company-info'>
                <ul>
                    <li>
                    	<img id='logo-image' src='<?php echo LOGO_IMAGE; ?>' />
                    </li>
                    <li><?php echo COMPANY_ADDRESS1; ?></li>
                    <li><?php echo COMPANY_ADDRESS2; ?></li>
                    <!--li>TEL : <?php echo COMPANY_TEL; ?></li-->
                </ul>
            </td>
            <td id='general-info' align='right'>
            	
                <h3>
                	<?php if ($page == 1) { ?>
                    <button id='pdf-invoice' class='btn' data-txid='<?php echo $txid; ?>'>Print</button>
                    <?php } ?>
                    <?php echo (0 != $invoice_no) ? 'INVOICE' : 'SALES PREVIEW'; ?>
                </h3>
               
                <table border=1 cellspacing=0 cellpading=1>
                    <tr>
                    	<td>Phone #</td>
                    	<td>Fax #</td>
    		            <td>DATE</td>
    		            <td>INVOICE #</td>
                    </tr>
                    <tr>
                        <td><?php echo COMPANY_TEL; ?></td>
                        <td><?php echo COMPANY_FAX; ?></td>
                        <td><?php echo substr($order_date,0,10); ?></td>
                        <td><?php echo $invoice_no; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
