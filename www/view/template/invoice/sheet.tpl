<style>
/* http://stackoverflow.com/questions/1763639/how-to-deal-with-page-breaks-when-printing-a-large-html-table */
@media print {
    table { page-break-after:auto }
    div   { page-break-inside:avoid; } /* This is the key */
    tr    { page-break-inside:avoid; page-break-after:auto }
    td    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
    #pdf-invoice {
    	display: none;
    }
}
</style>

<?php
/* Stand alone page Just for Invoice
*/
$originalPrice = 0;
$unitDiscounts = 0;
$server_name = getenv('SERVER_NAME');
$request_uri = getenv('REQUEST_URI');
$url = 'http://'.$server_name.$request_uri;

function add_date($givendate,$day=0,$mth=0,$yr=0){
    $givendate = $givendate. ' 00:00:00';
    $cd = strtotime($givendate);
    $newdate = date('Y-m-d', mktime(date('h',$cd),
                    date('i',$cd), date('s',$cd), date('m',$cd)+$mth,
                    date('d',$cd)+$day, date('Y',$cd)+$yr));
    return $newdate;
}
?>

<link rel="stylesheet" type="text/css" href="/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/css/invoice.css" />
<script type="text/javascript" src="/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/invoice.js"></script>



<?php
$pageMap = array('20','25');
$rowCount = count($sales);
$pageCount = 1;
$page = 1;
$checkSum = $pageMap[0];
for($i=1; $i<$rowCount; $i++) {
	
	if ( $rowCount/$checkSum > 1 ) {
		$pageCount++;
		$checkSum += (int)$pageMap[1];
	}
}
?>
<?php
for($page=1; $page<=$pageCount; $page++) {
	if ($page == 1) {
	    $startOffset = 0;
	    $endOffset = $pageMap[0];
	} else if ($page == 2 ) {
	    $startOffset += $pageMap[0];
	    $endOffset = $startOffset + $pageMap[1];
	} else {
	    $startOffset += $pageMap[1];
	    $endOffset = $startOffset + $pageMap[1];
	}
	if ( $page == $pageCount ) {
		$endOffset = $rowCount;
	}
?>

<div class='sheet' data-page='<?php echo $page; ?>'>

    <?php require DIR_TEMPLATE . '/invoice/sheet_head.tpl'; ?>

    <?php
    if ( $page == 1 ) {
    ?>

    <?php
    $shipto = nl2br($shipto);
    $address =<<<HTML
        <ul style="height:92px;margin-bottom:0px">
            <li>$store_name</li>
            <li>$address1</li>
            <li>$city , $state, $zipcode</li>
            <li>TEL : $phone1</li>
        </ul>
HTML;
    if('' == $billto) $billto = $address;
    if('' == $shipto) $shipto = $address;
    ?>
    <table id='billinfo'>
        <tr>
            <td width='400px'>
                <table cellspacing=0 cellpading=1>
                    <tr>
                        <td style='padding-left:20px;font-size:13px;'>Bill To</td>
                    </tr>
                    <tr>
                        <td style='padding-left:20px;'>
                            <?php echo $billto; ?>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table cellspacing=0 cellpading=1>
                    <tr>
                        <td style='padding-left:20px;font-size:13px;'>Ship To</td>
                    </tr>
                    <tr>
                        <td style='padding-left:20px;'>
                            <?php echo $shipto; ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

	<?php
    $aPaymethod = array (
        'n3' => 'Net30',
        'n6' => 'Net60',
        'cc' => 'Cod',
        'pp' => 'prepaid',
        'pd' => 'paid',
        'bb' => 'Bill2Bill'
    );
    ?>
    <table id='metainfo' border='1' cellspacing=0 cellpading=1>
        <tr> 
            <td>P.O.#</td>
            <td>TERMS</td>

            <td>REP</td>
            <td>SHIP DATE</td>
            <td>SHIP VIA</td>
            <td>Count</td>
        </tr>
        <tr>
            <td><?php echo $txid; ?></td>
            <td><?php echo $aPaymethod[$payment]; ?></td>
            <td><?php echo $salesrep; ?></td>

            <td><?php echo $shipped_date; ?></td>
            <td><?php echo strtoupper($ship_method); ?></td>
            <td><?php echo $rowCount; ?></td>
        </tr>
    </table>

	<?php
	}	// page = 1
	?>

	<table id='detail' border='1'>
        <thead>
        <tr>
            <th style="width:80px">Item Code</th>
            <th style="width:420px">Description</th>
            <th>Order</th>
            <th>Ship</th>
            <th>Unit Price</th>
            <!--th>Org Price</th-->
            <th>Unit D/C</th>
            <th>Amount</th>
        </tr>
        </thead>
		<tbody>
		<?php
		$aSale = array();
		foreach ($sales as $sale ) {
		    $aSale[] = $sale;
		}
		for($i=$startOffset; $i<$endOffset; $i++){
        //foreach ($sales as $sale ) {
		    
			$sale = $aSale[$i];
			//echo '<pre>'; print_r($sales); echo '</pre>'; exit;
			$line_css = '';
			if ($i%5 == 4) $line_css = 'border-bottom:1px dotted gray;';
			//echo '<pre>'; print_r($sale['order_quantity']); echo '</pre>';
			//echo '<pre>'; print_r(substr($sale['order_quantity'],-2)); echo '</pre>';
			$ordered = ( substr($sale['order_quantity'],-2) == '00' ) ? round($sale['order_quantity']) : $sale['order_quantity'] ;
			$shipped = ( substr($sale['shipped'],-2) == '00' ) ? round($sale['shipped']) : $sale['shipped'] ;
			$originalPrice += round( $sale['shipped'] * $sale['price1'] , 2 );
		?>
		    <tr class='item'>
		    	<td style="text-align:center;<?php echo $line_css; ?>" class='left'>

		    	<?php echo $sale['model']; ?></td>
		    	<td style="<?php echo $line_css; ?>" class='left'><?php echo stripslashes(htmlspecialchars_decode($sale['description'])); ?></td>
		    	<td style="<?php echo $line_css; ?>"> <?php echo $ordered; ?></td>
		    	<td style="<?php echo $line_css; ?>"> <?php echo $shipped; ?></td>
		    	<td style="text-align:right;<?php echo $line_css; ?>"> <?php echo $sale['price1']; ?></td>
		    	<!--td style="text-align:right;<?php echo $line_css; ?>"><?php echo number_format($shipped * $sale['price1'],2); ?></td-->
		    	<td style="text-align:right;<?php echo $line_css; ?>">
		    	    <?php
		    	    $unitDiscount = 0;
		    	    if ( $sale['discount'] > 0 ) {
		    	        echo $sale['discount'] . '%';
		    	        
		    	        $orgDiscount = $shipped * $sale['price1'];
		    	        $unitDiscount = round( $orgDiscount - $sale['total_price'] , 2);
		    	        $unitDiscounts += $unitDiscount;
		    	    }
		    	    ?>
		    	</td>
		    	<td style="<?php echo $line_css; ?>">
		    	    <?php echo $sale['total_price']; ?>
		    	</td>
		    </tr>
		<?php
		
		   
		}
		?>
		
		<?php
		if ( $page == $pageCount ) {
			require DIR_TEMPLATE . '/invoice/sheet_foot.tpl';
		}
		?>
		<tbody>
	</table>
	<?php if( $pageCount > 1 ) { ?>
	<div style='font-size:11px;text-align:center;width:800px;'>(<?php echo 'Page : '. $page; ?>)</div>
<?php } ?>
</div>




<?php
}	// end of pageCount
?>

<script>
$('#pdf-invoice').bind('click',function(e) {
	window.print();
});
</script>
