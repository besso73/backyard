<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" xml:lang="<?php echo $lang; ?>">
<head>
<title><?php echo $title; ?></title><base href="<?php echo $base; ?>" />
<?php foreach ($links as $link){ ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css" />
<link rel="stylesheet" type="text/css" href="view/javascript/jquery/ui/themes/ui-lightness/ui.all.css" />
<?php foreach ($styles as $style){ ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="view/javascript/jquery/jquery.ui.core.js"></script>
<script type="text/javascript" src="view/javascript/jquery/jquery.ui.widget.js"></script>
<script type="text/javascript" src="view/javascript/jquery/jquery.ui.mouse.js"></script>
<script type="text/javascript" src="view/javascript/jquery/jquery.ui.draggable.js"></script>
<script type="text/javascript" src="view/javascript/jquery/jquery.ui.droppable.js"></script>
<script type="text/javascript" src="view/javascript/jquery/superfish/js/superfish.js"></script>

<script type="text/javascript" src="view/javascript/jquery/tab.js"></script>
<script type="text/javascript" src="view/javascript/jquery/autoresize.jquery.js"></script>
<script type="text/javascript" src="view/javascript/datePicker/date.js"></script>

<script src="view/javascript/datePicker/jquery.datePicker.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="view/javascript/datePicker/datePicker.css">

<script src="view/javascript/jquery/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="view/stylesheet/jquery.fancybox-1.3.4.css" />

<!-- jquery UI for atc ... -->
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" id="theme">
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>


<style>
.datepicker{ position:absolute;  z-index:99; }
</style>
<!---->
<?php foreach ($scripts as $script){ ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
  // Confirm Delete
  $('#form').submit(function(){
    if($(this).attr('action').indexOf('delete',1) != -1){
      if(!confirm ('<?php echo $text_confirm; ?>')){  return false; }
    }
  });
  // Confirm Uninstall
  $('a').click(function(){
    if($(this).attr('href') != null && $(this).attr('href').indexOf('uninstall',1) != -1){
      if(!confirm ('<?php echo $text_confirm; ?>')){  return false; }
    }
  });
});
</script>
<script type="text/javascript">
/* Warning: Enabling the Script panel causes a Firefox slow-down due to a platform bug. This will be fixed with the next major Firefox and Firebug versions. */
$(document).ready(function(){
  $(".scrollbox").each(function(i){
  	$(this).attr('id', 'scrollbox_' + i);
		sbox = '#' + $(this).attr('id');
  	$(this).after('<span><a onclick="$(\'' + sbox + ' :checkbox\').attr(\'checked\', \'checked\');"><u><?php echo $text_select_all; ?></u></a> / <a onclick="$(\'' + sbox + ' :checkbox\').attr(\'checked\', \'\');"><u><?php echo $text_unselect_all; ?></u></a></span>');
	});
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  /*** $(".list tr:even").css("background-color", "#F4F4F8"); ***/
});
</script>
</head>
<body>
<div id="container">
	<div id="content">
