<?php echo $header; ?>

<div class="container">
    <form class="form-signin" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <h3 class="form-signin-heading">
        <img id='logo-image' src='<?php echo LOGO_IMAGE; ?>' />
        </h3>
        <input type="text" placeholder="ID" 
        id='username' name="username" value="<?php echo $username;?>" class="input-block-level" style='margin-bottom: 20px;'>
        <input type="password" placeholder="Password" 
        id='password' name="password" value="<?php echo $password; ?>" 
        class="input-block-level" style='margin-bottom: 20px;'>
        <!--label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label-->
        <button type="submit" class="btn btn-large btn-primary">Sign In</button>
    </form>
</div>
<script type="text/javascript">
    $('document').ready(
      function(){ $('#username').focus(); }
    );
	$('#password').keydown(function(e){
		if(e.keyCode == 13){  $('#form').submit();  }
	});
</script>
