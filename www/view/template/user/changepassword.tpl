<form action="/user/user/updatepassword" method="post" enctype="multipart/form-data" id="form-change-password">
    <div class="content">
        <label for='change-password'>New Password</label>
        <input type="password" name="password" id='change-password' value="" size=10 />
        <a id='ajaxSubmit' class="btn">Change</span></a>
    </div>
</form>

<script type="text/javascript">
$.fn.ajaxSubmit = function(form){
    $.post(form.attr('action'), form.serialize(), function(data){
        $.fancybox.close();
    });
}

$('#form-change-password input').keydown(function(e){
    if(e.keyCode == 13){
        form = $('#form-change-password');
        $.fn.ajaxSubmit(form);
    }
});

$('#ajaxSubmit').bind('click',function(e){
    form = $('#form-change-password');
    $.fn.ajaxSubmit(form);
});
</script>
