<?php
final class Config {
	private $data = array();

   	public function get($key) {
    	return (isset($this->data[$key]) ? $this->data[$key] : NULL);
    }

    public function set($key, $value) {
    	$this->data[$key] = $value;
    }

    public function has($key) {
    	return isset($this->data[$key]);
    }

   	public function load($filename) {
    	$file = DIR_CONFIG . $filename . '.php';
    	if (file_exists($file)) { 
    		$cfg = array();
    		require($file);
    		$this->data = array_merge($this->data, $cfg);
    	} else {
    		exit('Error: Could not load config ' . $filename . '!');
    	}
   	}

    public function getCategory(){
    }

    public function getCatalog(){
        $catalog = array();
        $categoryFile = DIR_APPLICATION . "data/catalog";
        require($categoryFile);
        return $catalog;
    }

    public function getCatalogBase(){
        $catalogBase = array();
        $categoryFile = DIR_APPLICATION . "data/catalog_base";
        //echo $categoryFile;
        require($categoryFile);
        return $catalog_base;
    }

    public function getCatalogMobile(){
        $catalog = array();
        $categoryFile = DIR_APPLICATION . "data/catalog_mobile";
        require($categoryFile);
        return $catalog;
    }

    public function getCatalogInventory(){
        $catalog = array();
        $categoryFile = DIR_APPLICATION . "data/catalog_inventory";
        require($categoryFile);
        return $catalog;
    }

    public function getStoreStatus(){
    	$aStoreCode = array(
    		'0' => 'DEAD',
    		'1' => 'GOOD',
    		'2' => 'BAD',
    		'3' => 'WORST',
    		'9' => 'POTENTIAL'
    	);
    	return $aStoreCode;
    }

    // todo , To init Arg1 fail. weird
    public function redirectGroup($group){
        if($group == '') $group = '11';
        $aRedirectGroup = array(
          '1' => 'sales/list',
          '11' => 'sales/list'
        );
        //echo( $group );
        //var_dump( $aRedirectGroup[$group] );
        return $aRedirectGroup[$group];
    }

    public function mainProduct(){
    }
}
?>
