$(function() {

/***
    var pp
    $.fn.getPP = function(){
        $.ajax({
            type:'get',
            url:'/common/cache/ajaxFlushCache',
            dataType:'json',
            data:'reset=true&key=products',
            success:function(res){
                if (res.code == '200') {
                    localStorage.setItem('pp',res.message);
                 }
            }
        });
    }

    var init = function(){
        var p = localStorage.getItem('pp');
        if ( p === null ) {
            $.fn.getPP();
        }
    }

    init();
***/

    /* TODO focusein could be better, b
     */
    $('input').live('click',function(e){
        $(this).select();
    });
    
    // global variables
    var order_form = $('#order-form'),
        input_mode = order_form.find('input[name=mode]'),
        input_accountno = order_form.find('input[name=accountno]'), 
        input_txid = order_form.find('input[name=txid]');

    if ('' == input_txid.val()) {
        input_mode.val('insert');
    } else {
        input_mode.val('update');
    }
    
    if ( '' == input_accountno.val() ) {
        $('input[name="store_name"]').focus();
    } else {
        $('#product-atc').select().focus();
    }

    $.fn.keyMagic = function(e) {
        var that = $(e.target);
        var oParent = that.closest('tr');
        
        var offset = oParent.find('input').index(that);
        
        // up
        if ( 'keydown' == e.type && '38' == e.which) {
            oPrev = oParent.prev();
            //alert(oPrev.length);
            //alert(oParent.prev().get(0).nodeName);
            if ( oPrev.length == 1 ) {
                oPrev.find('input').eq(offset).select().focus();
            }
        }
        // down
        if ( 'keydown' == e.type && '40' == e.which) {
            oNext = oParent.next();
            if ( oNext.length == 1 ) {
                oNext.find('input').eq(offset).select().focus();
            }
        }
        // left
        if ( 'keydown' == e.type && '37' == e.which) {
            if ( offset > 0 ) {
                oParent.find('input').eq(offset-1).select().focus();
            }
        }
        // left
        if ( 'keydown' == e.type && '39' == e.which) {
            if ( offset < oParent.find('input').length  ) {
                oParent.find('input').eq(offset+1).select().focus();
            }
        }
        
    }
    
    /////////////////////////////////////////////////////////////////////////////
    // order
    /////////////////////////////////////////////////////////////////////////////
    $.fn.setProductDetailPanel = function(item) {
        var oName = $('#product-detail-panel input[id="desc[]"]');
        $('#product-detail-panel input[name="id[]"]').val(item.product_id);
        $('#product-detail-panel input[name="m[]"]').val(item.model);
        
        oName.val(item.name);
        if ( item.name.match(/.*DZ$/) || item.name.match(/.*DZ\)$/)  ) {
            oName.css('background-color','#FFFF99');
        }
        
        $('#product-detail-panel input[name="i[]"]').val(item.quantity);
        if( $('#storetype').val() == 'W' ) {
            $('#product-detail-panel input[name="p[]"]').val(item.ws_price);
        } else {
            $('#product-detail-panel input[name="p[]"]').val(item.rt_price);
        }
        $('#product-detail-panel input[name="o[]"]').select().focus();
    }

    //var p = localStorage.getItem('p');
    $( "#product-atc" )
    .autocomplete({
        max : 50,
        source: function (request, response) {
/*
            var p = localStorage.getItem('pp');
            if ( p == null ) {
*/
                $.ajax({
                    url:"/sales/atc/products",
                    dataType:"json",
                    data: { query: request.term },
                    success: function(data) {
                        response(data);
                    }
                });
/*
            } else {
                var text, result = [];
                op = jQuery.parseJSON(p);
                $.each(op, function(idx,o){
                    console.log(o['name']); 
                    text = o['name'].toLowerCase();
                    if ( text.match(request.term) || text.match(request.term) ) {
                        result.push(o)
                    }
                });
                var data = JSON.stringify(result);
                response(data);
            }
*/
        },
        select: function(event,ui) {
            $( "#product-atc" ).val( ui.item.name + " (" + ui.item.model + ")");
            $.fn.setProductDetailPanel(ui.item);
            return false;
            },
            minLength: 2
        }).data( "autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<a>" + item.name + " (" + item.model + ")" )
        .appendTo( ul );
    };
    
    $.fn.autoSave = function(e) {
        var rows = $('#product-sales-list tbody tr'),
            length = rows.length,
            limit = 20,
            notice_count = 100;
        //if ( length >= notice_count && length % 10 == 0 ) {
        if ( length >= notice_count ) {
            $.fn.loader( length, 2000 );
        }
        return;
        if ( length >= limit && length % limit == 0) {
            var input_salesrep = order_form.find('input[name=salesrep]'),
                input_order_date = order_form.find('input[name=order_date]'),
                oAjax = order_form.find('input[name=ajax]'),
                oTxid = order_form.find('input[name=txid]'),
                oMode = order_form.find('input[name=mode]');

            if(false == $.fn.validateNull(input_accountno)) return;
            if(false == $.fn.validateNull(input_order_date)) return;
            if(false == $.fn.validateNull(input_salesrep)) return;

            if (!$('#product-sales-list').has('tbody')) {
                alert('No order');
                return false;
            }

            oAjax.val('1');
            var action = order_form.attr('action');
            $.ajax({
                type: 'POST',
                url: action, 
                data: order_form.serialize(),
                dataType: 'json',
                success: function(res){
                    if (res.code == '200') {
                        //$('#flush-product-cache').click();
                        //$.fancybox.close();
                        //location.reload();
                        oTxid.val(res.txid);
                        oMode.val('update');
                    }
                }
            });
        }
    }

    // Control product-detail-panel - total, order price, balance etc
    $.fn.controlDetailPanel = function(that) {
        //alert('call controlDetailPanel');
        row = that.closest('tr');

        var price = row.find('input[name="p[]"]').val();
        var oOrder = row.find('input[name="o[]"]'),
            order = oOrder.val();

        var inventory = row.find('input[name="i[]"]').val();
        var discount = row.find('input[name="d[]"]').val();
        var store_discount = $('input[name="store_discount"]').val();
        var total = 0;
        
        //if (that.is('input[name="s[]"]')) {
        var oShipped = row.find('input[name="s[]"]');
        if (oShipped.length != 0) {
            var shipped = oShipped.val();
            total = parseFloat( price * shipped );
        } else {
            total = parseFloat( price * order );
        }

        //total  = total.toFixed(2);
        total  = $.fn.roundToTwo(total);
        //console.log(total);

        if(store_discount > 0){
            total = parseFloat( total * ((100 - store_discount) / 100) );
            //total  = total.toFixed(2);
            total  = $.fn.roundToTwo(total);
        }

        if(discount > 0){
            total = parseFloat( total * ((100 - discount) / 100) );
        
        }
        //console.log(total);
        //console.log(total)
        //total = Math.round( total, 2 );
        total  = $.fn.roundToTwo(total);
        
        row.find('input[name="t[]"]').val(total);

        if( parseInt(inventory) < parseInt(order) ){
            row.find('input[name="i[]"]')
            .css('background-color','red');
        }
    }
    
    $.fn.roundToTwo = function(num) {    
        return +(Math.round(num + "e+2")  + "e-2");
    }
    
    $.fn.resetProductDetail = function(){
        $('#product-detail-panel input[name="id[]"]').val('');
        $('#product-detail-panel input[name="m[]"]').val('');
        $('#product-detail-panel input[id="desc[]"]').val('').css('background-color','white');
        $('#product-detail-panel input[name="i[]"]').val('0').css('background-color','#FFF');
        $('#product-detail-panel input[name="p[]"]').val('');
        $('#product-detail-panel input[name="d[]"]').val('0');
        $('#product-detail-panel input[name="t[]"]').val('0');
        $('#product-detail-panel input[name="o[]"]').val('0');
        $('#product-detail-panel input[name="s[]"]').val('0');
        //$('#product-atc').val('').select();
        $('#product-atc').val('');
        
        
    }
    
    /* not to used
    $.fn.adjustAtcPosition = function() {

        // get position in brower of atc input box
        var eTop = $('#product-atc').offset().top;
        var t = $(window).scrollTop();
        console.log(t);
        t = eTop - t ;
        
        t = t + 200;
        $('#product-atc').scrollTop(300);
    };
    */
    
    // generate total price
    $('#product-detail-panel').on('focusin','input',function(e){
        _that = $(e.target);
        $_count = _that.find('input[name="o[]"]').val();
        $_discount = _that.find('input[name="d[]"]').val();
        $_total = _that.find('input[name="t[]"]').val();
    })
    .on('keydown','input',function(e){
        that = $(this);
    	if ( '13' == e.which) {
        	e.preventDefault();
        	
            pattern = /\d/;
            if (!pattern.test(that.val())) {
                that.val('0');
            }
            
            var oShipped = that.parents('tr').find('input[name="s[]"]');
            if( that.is('input[name="o[]"]') && oShipped.is(':visible') ) {
                oShipped.val(that.val())
            } 

            $.fn.controlDetailPanel(that);
            
            if ( that.is('input[name="o[]"]') ) {
                //alert('click apply-product in detail keydown');
                //$('#apply-product').click();
                
                var order = $('#product-detail-panel input[name="o[]"]').val(),
                    oName = $('#product-detail-panel input[id="desc[]"]'),
                    oId = $('#product-detail-panel input[name="id[]"]'),
                    isDup = false;
        
                if (order == '0' || order == '') {
                    alert('Order should be great than zero!');
                    return false;
                }
                
                isDup = $.fn.checkDuplicate(oId.val());
                // just update the count of order
                if ( isDup != false ) {

                    var row = $('#product-sales-list').find('input[value="' + isDup + '"]');
                    var _oOrder = row.closest('tr').find('input[name="o[]"]');
                    _order = parseFloat(_oOrder.val()) + parseFloat(order);
                    _oOrder.val(_order);

                    var _oShipped = row.closest('tr').find('input[name="s[]"]');
                    _shipped = parseFloat(_oShipped.val()) + parseFloat(order);
                    _oShipped.val(_shipped);

                    trigger = jQuery.Event("keypress");
                    trigger.which = 13;
                    _oOrder.focusin().focusout();
                    
                    $.fn.controlDetailPanel(_oOrder);
                    
                    
                } else {
                
                    var product_row = $('#product-apply-row').clone().attr('id','');
                    //debugger;
                    product_row.appendTo('#product-sales-list');
        
                    product_row.find('.td-btn')
                    .html('')
                    .append('<button class="btn del-sale-row" >Del</button>');

                    
                    
                    //$.fn.adjustAtcPosition();
                }
            }
            
            $.fn.calculateTotal();
            $.fn.resetProductDetail();
            $('#product-atc').select();
            $.fn.autoSave();
            $('#product-detail-panel').unbind('focusout');
            
        }
    });
    /***
    .on('focusout','input',function(e){
        that = $(e.target);
        if( that.is('input[name="p[]"]') || that.is('input[name="o[]"]') || that.is('input[name="s[]"]') || that.is('input[name="d[]"]') || that.is('input[name="t[]"]') ) {
            e.preventDefault();
            pattern = /\d/;
            if (!pattern.test(that.val())) {
                that.val('0');
            }
            
            var oShipped = that.parents('tr').find('input[name="s[]"]');
            if( that.is('input[name="o[]"]') && oShipped.is(':visible') ) {
                oShipped.val(that.val())
            }
            
            $.fn.controlDetailPanel(that);

            if ( that.is('input[name="o[]"]') ) {

                var order = $('#product-detail-panel input[name="o[]"]').val(),
                    oName = $('#product-detail-panel input[id="desc[]"]');

                if (order == '0' || order == '') {
                    //alert('Order should be great than zero!!');
                    return false;
                }

                var product_row = $('#product-apply-row').clone().attr('id','');
                product_row.appendTo('#product-sales-list');

                product_row.find('.td-btn')
                .html('')
                .append('<button class="btn del-sale-row" >Del</button>');

                $.fn.calculateTotal();
                $.fn.resetProductDetail();

                $('#product-atc').select();
            }

        }
        
    });
    ***/
    
    $.fn.calculateTotal = function() {
        var total_order_price = 0,
            total_discount_price = 0,
            total = 0
            discount = 0,
            subtotal_order_price = 0,
            rows = '';
        rows = $('#product-sales-list tbody tr');
        rows.each(function(idx,row){
            total = $(row).find('input[name="t[]"]').val();
            total_order_price += parseFloat(total);

            discount = $(row).find('input[name="d[]"]').val()
            if (discount > 0) {
                total_discount_price += parseFloat(discount);
            }
        })
        total_order_price  = total_order_price.toFixed(2);
        //total_discount_price  = total_discount_price.toFixed(2);
        total_order_price = Math.round( total_order_price * 100 ) / 100;
        store_discount = $('input[name="store_discount_percent"]').val()
        
        //if(store_discount > 0){
            subtotal_order_price = parseFloat( total_order_price * ((100 - store_discount) / 100) );
            subtotal_order_price  = subtotal_order_price.toFixed(2);
            //var store_discount_price = _total_order_price - total_order_price;
        /*
        } else {
            subtotal_order_price = total_order_price;
        }
        */
        subtotal_order_price = Math.round( subtotal_order_price * 100 ) / 100;
        
        //total_discount_price += parseFloat(total_discount_price + store_discount_price);

        $('input[name="total_order_price"]').val(total_order_price);
        $('input[name="subtotal_order_price"]').val(subtotal_order_price);
        //$('input[name="total_discount_price"]').val(total_discount_price);
        
        /* TODO : block for a while
        $('#product-sales-list')
        .trigger('update')
        .tablesorter({element_type:'input'});
        */
        
        
    }

    $.fn.checkDuplicate = function(pid){
        var pid_in_list,
            isDup = false;
        $('#product-sales-list tr').each(function(idx,row) {
            pid_in_list = $(row).find('input[name="id[]"]').val();
            if ( pid == pid_in_list ) {
                isDup = pid_in_list;
                return isDup;
            }
        });
        return isDup;
    }

    $('#apply-product').bind('click', function(e){

        e.preventDefault();
        //e.stopPropagation();
        
        //parent = $(e.target).closest('tr#product-apply-row');
        // 0 order check
        var order = $('#product-detail-panel input[name="o[]"]').val(),
            oName = $('#product-detail-panel input[id="desc[]"]'),
            oId = $('#product-detail-panel input[name="id[]"]'),
            isDup = false;

        if (order == '0' || order == '') {
            alert('Order should be great than zero');
            return false;
        }
        
        /*
        // check if already exist the product
        var product_id = $('#product-detail-panel input[name="id[]"]').val();
        var has_pid = false;
        $('#product-sales-list').each(function(idx,row) {
            var pid_in_list = $(row).find('input[name="id[]"]').val();
            if ( product_id == pid_in_list ) {
                has_pid = true;
                return false;
            }
        });
        
        if (has_pid) {
            var desc = $('#product-detail-panel input[id="desc[]"]').val();
            alert( 'You already added "' + desc + '" \nPlease Edit in list' );
            $.fn.resetProductDetail();
            return false;
        }
        */
        
        /*
        isDup = $.fn.checkDuplicate(oId.val());
        console.log( isDup );
        debugger;
        return;
        */
        
        var product_row = $('#product-apply-row').clone().attr('id','');
        product_row.appendTo('#product-sales-list');

        product_row.find('.td-btn')
        .html('')
        .append('<button class="btn del-sale-row" >Del</button>');

        
        $.fn.calculateTotal();
        $.fn.resetProductDetail();
        
    }); // end of apply-product

    $('#product-sales-list button.del-sale-row').live('click',function(e) {
        var row = $(e.target).parents('tr');
        row.remove();
        $.fn.calculateTotal();
    });

    $('#product-sales-list input')
    .live('focusin', function(e) {
        var that = $(e.target),
            row = that.closest('tr'),
            oOrder = row.find('input[name="o[]"]');
        _order = oOrder.val();
    })
    .live('keydown', function(e){
        var oParent, oPrev, oNext,
            oName, oOrder, oShipped, order,
            that = $(e.target);
        
        if ( that.is('input[name="d[]"]') || that.is('input[name="s[]"]') ) {
            // up
            if ( '38' == e.which) {
                oParent = that.closest('tr');
                oName = that.attr("name");
                oPrev = oParent.prev();
                //alert(oPrev.length);
                //alert(oParent.prev().get(0).nodeName);
                if ( oPrev.length == 1 ) {
                    //if ( 'TR' == oParent.prev().get(0).nodeName ) {
                        oPrev.find('input[name="' + oName + '"]').select().focus();
                    //}
                }
            }

            // down
            if ( '40' == e.which) {
                oParent = that.closest('tr');
                oName = that.attr("name");
                oNext = oParent.next();
                if ( oNext.length == 1 ) {
                    //if ( 'TR' == oNext.get(0).nodeName ) {
                        oNext.find('input[name="' + oName + '"]').select().focus();
                    //}
                }

            }
        }
        
        if ( '13' == e.which ) {
            row = that.closest('tr');
            oOrder = row.find('input[name="o[]"]');
            oShipped = row.find('input[name="s[]"]');
            order = oOrder.val();
            
            if ( that.is('input[name="o[]"]') ) {
                oShipped.val(order);
            }
            
            if( that.is('input[name="p[]"]') || that.is('input[name="o[]"]') || that.is('input[name="s[]"]') || that.is('input[name="d[]"]') ) {
                
                if (order == '0' || order == '') {
                    oOrder.val(_order);
                    oShipped.val(_order);
                    return false;
                }
                $.fn.controlDetailPanel(that);
                $.fn.resetProductDetail();
                $.fn.calculateTotal();
            }
        }
    })
    .live('focusout', function(e){
        var oParent, oPrev, oNext,
            oName, oOrder, oShipped, order,
            that = $(e.target);
        
        if( that.is('input[name="p[]"]') || that.is('input[name="o[]"]') || that.is('input[name="s[]"]') || that.is('input[name="d[]"]') ) {
            
            row = that.closest('tr');
            oOrder = row.find('input[name="o[]"]');
            oShipped = row.find('input[name="s[]"]');
            order = oOrder.val();
            
            if ( that.is('input[name="o[]"]') ) {
                oShipped.val(order);
            }
                
            if (order == '0' || order == '') {
                oOrder.val(_order);
                oShipped.val(_order);
                return false;
            }
            $.fn.controlDetailPanel(that);
            $.fn.resetProductDetail();
            $.fn.calculateTotal();
            
        }
    });
    
    /* store discount percent */
    $('input[name="store_discount_percent"]')
    .bind('click', function(e) {
        var that = $(this);
        that.select().focus();
    })
    .bind('keydown', function(e) {
        var that = $(this);
        if ('13' == e.which || '9' == e.which) {
            $.fn.calculateTotal();
            e.preventDefault();
        }
    })
    .bind('focusout', function(e) {
        var that = $(this);
        $.fn.calculateTotal();
        e.preventDefault();
    });

    /* check value is null of input box */ 
    $.fn.validateNull = function(obj){
        if(obj.attr('value') == ''){
          alert('fill value : ' + obj.attr('name'));
          obj.css('background-color','red');
          return false;
        }
    }

    
    $.fn.validateNormalOrder = function(){
        var rows = $('#product-sales-list tbody tr'),
            oO, oS;
        $.each(rows, function(idx, row) {
            oM = $(row).find('input[name="m[]"]');
            oO = $(row).find('input[name="o[]"]');
            oS = $(row).find('input[name="s[]"]');
            if (oO.val() == 0) {
                alert('No order quantity : ' + oM.val());
                return false;
            }
            if (oO.val() != oS.val()){
                oS.val(oO.val());
                oO.focusin().focusout();
            }
        });
    }
    
    /* control submit */
    $('#submit-order').bind('click',function(e){
        e.preventDefault();
        var that = $(e.target),
            input_salesrep = order_form.find('input[name=salesrep]'),
            input_order_date = order_form.find('input[name=order_date]'),
            oTxid = order_form.find('input[name=txid]'),
            oStatus = order_form.find('input[name=status]'),
            oAjax = order_form.find('input[name=ajax]');

        that.attr('disabled','disabled');

        if(false == $.fn.validateNull(input_accountno)) return;
        if(false == $.fn.validateNull(input_order_date)) return;
        if(false == $.fn.validateNull(input_salesrep)) return;

        if (!$('#product-sales-list').has('tbody')) {
            alert('No order');
            return false;
        }
        /* Discount restriction
         * TODO use PHP Constants
         */
        //if(input_total_discount_percent.val() > 20){  alert('Discount shouldn't over 20%); return; }
        if ( $(this).hasClass('invoice') ) {
            var action = order_form.attr('action');
            action += '&invoice=true';
            order_form.attr('action',action);
        }
        if ( oTxid.val() == '' ) {
            $.fn.validateNormalOrder();
        }
        
        //oStatus.val('1');
        oAjax.val('0');
        
        var oApplyOrder = $('#product-apply-row').find('input[name="o[]"]');
        if ( oApplyOrder.val() > 0 ) {
            $('#apply-product').click();
        }
        
        var oOrder = $('#product-sales-list').find('input[name="o[]"]');
        if ( oOrder.length == 0 ) {
            alert('No order items, check again');
            return false;
        }

        // add calculateTotal again to check again
        $.fn.calculateTotal();
        //alert('before order submit');
        order_form.submit();
        
    });
    
    // James Test Code
    /*
    $('#product-detail-panel input[name="id[]"]').val('1234');
    $('#product-detail-panel input[name="m[]"]').val('3S2404');
    $('#product-detail-panel input[id="desc[]"]').val('30S Remover Oil 4oz');
    $('#product-detail-panel input[name="i[]"]').val('57');
    $('#product-detail-panel input[name="p[]"]').val('100');
    $('#product-detail-panel input[name="t[]"]').val('500');
    $('#product-detail-panel input[name="o[]"]').val('5').select().focus();
    */
    
    $('input[name="m[]"]').live('dblclick',function(e){
        e.preventDefault();
        var that = $(e.target),
            product_id = that.parents('tr').find('input[name="id[]"]').val();

        if (product_id == '') {
            alert('No product_id');
            return false;
        }
        var data = {
            mode: 'update',
            product_id: product_id
        }
        $.ajax({
            type:'get',
            url:'/product/price/callUpdatePannel',
            dataType:'html',
            data: data,
            success:function(html){
                $.fancybox(html);
            }
        });
    });
    
    $('#tee-table').bind('keydown','input',function(e) {
        $.fn.keyMagic(e);
    });
    
    $('#btn-tee').bind('click', function(e) {
        e.preventDefault();
        $('#tee-table').toggle();
        if ($('#tee-table').is(':visible')) {
            //console.log('visible');
            $.fn.deleteTee();
        } else {
            $.fn.controlTee();
            //$('#tee-table').find('input:first').focus();
        }
    });
    
    $.fn.deleteTee = function() {
        var inputs = $('#tee-table').find('.tee-input'),
            qty = 0,
            price = 0,
            row = '';
        $.each( inputs, function( idx, input ) {
            var that = $(this);
            qty = that.val();
            model = that.data('key'),
            product_id = that.data('id');
            if ( '' != qty && $.isNumeric(qty) ) {
                //alert(qty + '-' + model);
                price = that.data('price');
                if ( '' != price && price > 0 ) {
                    //row = $('#product-sales-list').find('input[value="+ model +"]');
                    var row = $('#product-sales-list').find('input[value="' + product_id + '"]');
                    var tr = row.parents('tr');
                    tr.remove();
                }
            }
        });
    }
    
    $.fn.controlTee = function() {
        //$('#tee-table').show();
        var inputs = $('#tee-table').find('.tee-input'),
            qty = 0,
            price = 0;
        $.each( inputs, function( idx, input ) {
            var that = $(this);
            qty = that.val();
            //qty = 1;
            model = that.data('key');
            if ( '' != qty && $.isNumeric(qty) ) {
                //alert(qty + '-' + model);
                price = that.data('price');
                
                if ( '' != price && price > 0 ) {
                    $.fn.applyTeesToPanel(that);
                }
            }
        });
    }
    
    $.fn.applyTeesToPanel = function(that){

        var order = that.val();
        var product_id = that.data('id'),
            model = that.data('key'),
            price = that.data('price'),
            name = that.data('name');
        
        //order = 1;
        total = parseFloat(price) * parseFloat(order);
        total = Math.round( total * 100 ) / 100;
        
        $('#product-apply-row input[name="m[]"]').val(model);
        $('#product-apply-row input[name="id[]"]').val(product_id);
        $('#product-apply-row input[id="desc[]"]').val(name);
        $('#product-apply-row input[name="p[]"]').val(price);
        $('#product-apply-row input[name="o[]"]').val(order);
        $('#product-apply-row input[name="s[]"]').val(order);
        $('#product-apply-row input[name="t[]"]').val(total);
        
        // Below screw DOM id = #apply-product
        //var row_html = '<td><input type="text" readonly="" class="input-mini right" name="m[]" value="' + model + '"><input type="hidden" name="id[]" value="' + product_id + '"></td><td><input type="text" class="input-xlarge" id="desc[]" value="' + name + '" readonly></td><td><input type="text" class="input-mini right" value="' + price + '" name="p[]"></td><td><input type="text" readonly="" class="input-xmini" value="0" name="i[]"></td><td><input type="text" class="input-xmini" value="'+ order +'" name="o[]"></td><td><input type="text" class="input-xmini" value="0" name="d[]"></td><td><input type="text" readonly="" class="input-mini right" value="' + total + '" name="t[]"></td><td class="td-btn"><button class="btn" id="">Add</button>';
        //$('#product-apply-row').html('').append(row_html);
        
        var product_row = $('#product-apply-row').clone().attr('id','');
        product_row.appendTo('#product-sales-list');

        product_row.find('.td-btn')
        .html('')
        .append('<button class="btn del-sale-row" >Del</button>');

        $.fn.resetProductDetail();
        $.fn.calculateTotal();
        $.fn.autoSave();
    } 

    $('#stocking-table').bind('keydown','input',function(e) {
        $.fn.keyMagic(e);
    });
    
    $('#btn-stocking').bind('click', function(e) {
        e.preventDefault();
        $('#stocking-table').toggle();
        if ($('#stocking-table').is(':visible')) {
            //console.log('visible');
            $.fn.deleteStocking();
        } else {
            $.fn.controlStocking();
            //$('#stocking-table').find('input:first').focus();
        }
    });
    
    $.fn.deleteStocking = function() {
        var inputs = $('#stocking-table').find('.stocking-input'),
            qty = 0,
            price = 0,
            row = '';
        $.each( inputs, function( idx, input ) {
            var that = $(this);
            qty = that.val();
            model = that.data('key'),
            product_id = that.data('id');

            if ( '' != qty && $.isNumeric(qty) ) {
                //alert(qty + '-' + model);
                price = that.data('price');
                //alert(price);
                if ( '' != price && price > 0 ) {
                    //row = $('#product-sales-list').find('input[value="+ model +"]');
                    var row = $('#product-sales-list').find('input[value="' + product_id + '"]');
                    //debugger;
                    var tr = row.parents('tr');
                    tr.remove();
                }
            }
        });
    }
    
    $.fn.controlStocking = function() {
        //$('#stocking-table').show();
        var inputs = $('#stocking-table').find('.stocking-input'),
            qty = 0,
            price = 0;
        $.each( inputs, function( idx, input ) {
            var that = $(this);
            qty = that.val();
            model = that.data('key');
            //qty = 1;
            if ( '' != qty && $.isNumeric(qty) ) {
                //alert(qty + '-' + model);
                price = that.data('price');
                //alert(price);
                if ( '' != price && price > 0 ) {
                    $.fn.applyStockingsToPanel(that);
                }
            }
        });
    }
    
    $.fn.applyStockingsToPanel = function(that){
        
        var order = that.val();
        var product_id = that.data('id'),
            model = that.data('key'),
            price = that.data('price'),
            name = that.data('name');
        //order = 1;
        total = parseFloat(price) * parseFloat(order);
        total = Math.round( total * 100 ) / 100;
        
        $('#product-apply-row input[name="m[]"]').val(model);
        $('#product-apply-row input[name="id[]"]').val(product_id);
        $('#product-apply-row input[id="desc[]"]').val(name);
        $('#product-apply-row input[name="p[]"]').val(price);
        $('#product-apply-row input[name="o[]"]').val(order);
        $('#product-apply-row input[name="s[]"]').val(order);
        $('#product-apply-row input[name="t[]"]').val(total);
        
        // Below screw DOM id = #apply-product
        //var row_html = '<td><input type="text" readonly="" class="input-mini right" name="m[]" value="' + model + '"><input type="hidden" name="id[]" value="' + product_id + '"></td><td><input type="text" class="input-xlarge" id="desc[]" value="' + name + '" readonly></td><td><input type="text" class="input-mini right" value="' + price + '" name="p[]"></td><td><input type="text" readonly="" class="input-xmini" value="0" name="i[]"></td><td><input type="text" class="input-xmini" value="'+ order +'" name="o[]"></td><td><input type="text" class="input-xmini" value="0" name="d[]"></td><td><input type="text" readonly="" class="input-mini right" value="' + total + '" name="t[]"></td><td class="td-btn"><button class="btn" id="">Add</button>';
        //$('#product-apply-row').html('').append(row_html);
        
        //$('#apply-product').click();
        
        var product_row = $('#product-apply-row').clone().attr('id','');
        product_row.appendTo('#product-sales-list');

        product_row.find('.td-btn')
        .html('')
        .append('<button class="btn del-sale-row" >Del</button>');

        $.fn.resetProductDetail();
        $.fn.calculateTotal();
        $.fn.autoSave();
    }
    
    ///////////////////////////////////////////////////////////
    // Hats
   
    $('#hats-table').bind('keydown','input',function(e) {
        $.fn.keyMagic(e);
    }); 

    $('#btn-hats').bind('click', function(e) {
        e.preventDefault();
        $('#hats-table').toggle();
        if ($('#hats-table').is(':visible')) {
            $.fn.deleteHats();
        } else {
            $.fn.controlHats();
        }
    });
    
    $.fn.deleteHats = function() {
        var inputs = $('#hats-table').find('.hats-input'),
            qty = 0,
            price = 0,
            row = '';
        $.each( inputs, function( idx, input ) {
            var that = $(this);
            qty = that.val();
            model = that.data('key'),
            product_id = that.data('id');

            if ( '' != qty && $.isNumeric(qty) ) {
                price = that.data('price');
                if ( '' != price && price > 0 ) {
                    var row = $('#product-sales-list').find('input[value="' + product_id + '"]');
                    var tr = row.parents('tr');
                    tr.remove();
                }
            }
        });
    }
    
    $.fn.controlHats = function() {
        var inputs = $('#hats-table').find('.hats-input'),
            qty = 0,
            price = 0;
        $.each( inputs, function( idx, input ) {
            var that = $(this);
            qty = that.val();
            model = that.data('key');
            //qty = 1;
            if ( '' != qty && $.isNumeric(qty) ) {
                //alert(qty + '-' + model);
                price = that.data('price');
                //alert(price);
                if ( '' != price && price > 0 ) {
                    $.fn.applyHatsToPanel(that);
                }
            }
        });
    }
    
    $.fn.applyHatsToPanel = function(that){
        
        var order = that.val();
        var product_id = that.data('id'),
            model = that.data('key'),
            price = that.data('price'),
            name = that.data('name');
        //order = 1;
        total = parseFloat(price) * parseFloat(order);
        total = Math.round( total * 100 ) / 100;
        
        $('#product-apply-row input[name="m[]"]').val(model);
        $('#product-apply-row input[name="id[]"]').val(product_id);
        $('#product-apply-row input[id="desc[]"]').val(name);
        $('#product-apply-row input[name="p[]"]').val(price);
        $('#product-apply-row input[name="o[]"]').val(order);
        $('#product-apply-row input[name="s[]"]').val(order);
        $('#product-apply-row input[name="t[]"]').val(total);
        
        var product_row = $('#product-apply-row').clone().attr('id','');
        product_row.appendTo('#product-sales-list');

        product_row.find('.td-btn')
        .html('')
        .append('<button class="btn del-sale-row" >Del</button>');

        $.fn.resetProductDetail();
        $.fn.calculateTotal();
        $.fn.autoSave();
    }

    $('#adores-table').bind('keydown','input',function(e) {
        $.fn.keyMagic(e);
    });

    $('#btn-adores').bind('click', function(e) {
            e.preventDefault();
            $('#adores-table').toggle();
            if ($('#adores-table').is(':visible')) {
            $.fn.deleteAdores();
            } else {
            $.fn.controlAdores();
            }
            });

    $.fn.deleteAdores = function() {
        var inputs = $('#adores-table').find('.adores-input'),
            qty = 0,
            price = 0,
            row = '';
        $.each( inputs, function( idx, input ) {
                var that = $(this);
                qty = that.val();
                model = that.data('key'),
                product_id = that.data('id');

                if ( '' != qty && $.isNumeric(qty) ) {
                price = that.data('price');
                if ( '' != price && price > 0 ) {
                var row = $('#product-sales-list').find('input[value="' + product_id + '"]');
                var tr = row.parents('tr');
                tr.remove();
                }
                }
                });
    }

    $.fn.controlAdores = function() {
        var inputs = $('#adores-table').find('.adores-input'),
            qty = 0,
            price = 0;
        $.each( inputs, function( idx, input ) {
                var that = $(this);
                qty = that.val();
                model = that.data('key');
                if ( '' != qty && $.isNumeric(qty) ) {
                price = that.data('price');
                if ( '' != price && price > 0 ) {
                $.fn.applyAdoresToPanel(that);
                }
                }
                });
    }

    $.fn.applyAdoresToPanel = function(that){

        var order = that.val();
        var product_id = that.data('id'),
            model = that.data('key'),
            price = that.data('price'),
            name = that.data('name');
        total = parseFloat(price) * parseFloat(order);
        total = Math.round( total * 100 ) / 100;

        $('#product-apply-row input[name="m[]"]').val(model);
        $('#product-apply-row input[name="id[]"]').val(product_id);
        $('#product-apply-row input[id="desc[]"]').val(name);
        $('#product-apply-row input[name="p[]"]').val(price);
        $('#product-apply-row input[name="o[]"]').val(order);
        $('#product-apply-row input[name="s[]"]').val(order);
        $('#product-apply-row input[name="t[]"]').val(total);

        var product_row = $('#product-apply-row').clone().attr('id','');
        product_row.appendTo('#product-sales-list');

        product_row.find('.td-btn')
            .html('')
            .append('<button class="btn del-sale-row" >Del</button>');

        $.fn.resetProductDetail();
        $.fn.calculateTotal();
        $.fn.autoSave();
    }

    $('#btn-belts').bind('click', function(e) {
            e.preventDefault();
            $('#belts-table').toggle();
            if ($('#belts-table').is(':visible')) {
            $.fn.deleteBelts();
            } else {
            $.fn.controlBelts();
            }
            });

    $.fn.deleteBelts = function() {
        var inputs = $('#belts-table').find('.belts-input'),
            qty = 0,
            price = 0,
            row = '';
        $.each( inputs, function( idx, input ) {
                var that = $(this);
                qty = that.val();
                model = that.data('key'),
                product_id = that.data('id');

                if ( '' != qty && $.isNumeric(qty) ) {
                price = that.data('price');
                if ( '' != price && price > 0 ) {
                var row = $('#product-sales-list').find('input[value="' + product_id + '"]');
                var tr = row.parents('tr');
                tr.remove();
                }
                }
        });
    }

    $.fn.controlBelts = function() {
        var inputs = $('#belts-table').find('.belts-input'),
            qty = 0,
            price = 0;
        $.each( inputs, function( idx, input ) {
                var that = $(this);
                qty = that.val();
                model = that.data('key');
                if ( '' != qty && $.isNumeric(qty) ) {
                price = that.data('price');
                if ( '' != price && price > 0 ) {
                $.fn.applyBeltsToPanel(that);
                }
                }
        });
    }

    $.fn.applyBeltsToPanel = function(that){

        var order = that.val();
        var product_id = that.data('id'),
            model = that.data('key'),
            price = that.data('price'),
            name = that.data('name');
        total = parseFloat(price) * parseFloat(order);
        total = Math.round( total * 100 ) / 100;

        $('#product-apply-row input[name="m[]"]').val(model);
        $('#product-apply-row input[name="id[]"]').val(product_id);
        $('#product-apply-row input[id="desc[]"]').val(name);
        $('#product-apply-row input[name="p[]"]').val(price);
        $('#product-apply-row input[name="o[]"]').val(order);
        $('#product-apply-row input[name="s[]"]').val(order);
        $('#product-apply-row input[name="t[]"]').val(total);

        var product_row = $('#product-apply-row').clone().attr('id','');
        product_row.appendTo('#product-sales-list');

        product_row.find('.td-btn')
            .html('')
            .append('<button class="btn del-sale-row" >Del</button>');

        $.fn.resetProductDetail();
        $.fn.calculateTotal();
        $.fn.autoSave();
    }


});

// onload="alert('hey');setTimeout(hideNotice, 2000)"
setTimeout(hideNotice, 5000);
function hideNotice(){
    $('#notice').hide();
}
