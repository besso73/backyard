$(function() {


    /////////////////////////////////////////////////////////////////////////////
    // common
    /////////////////////////////////////////////////////////////////////////////
    //localStorage.clear();

    /* Display 'Select All/Unselect All' */
    $(".scrollbox").each(function(i){
  	    $(this).attr('id', 'scrollbox_' + i);
	    sbox = '#' + $(this).attr('id') + ' input';
  	    $(this).after('<span><a onclick="$(\'' + sbox + '\').prop(\'checked\', true);"><u>Select All</u></a> / <a onclick="$(\'' + sbox + '\').prop(\'checked\', false);"><u>Unselect All</u></a></span>');
	});

    // date picker binding
    $('input.date_pick')
    .bind('focusin',function(e){  $tgt = $(e.target); $_date = $tgt.val();  })
    .datepicker({
        clickInput:true,createButton:false,startDate:'2000-01-01',dateFormat:'yy-mm-dd'
    })
    .bind('change',function(e){
        var $tgt = $(e.target);
        if($tgt.is('input[name=ship_appointment]')){
            if( $tgt.val() != $_date ){
                $el_desc = $('textarea[name=description]');
                $desc = $el_desc.val();
                if( $desc.indexOf("Shipping Date") == -1 ) {
                    if($desc != ''){
                        $desc = $desc + '\n' + 'Shipping Date : ' + $tgt.val();
                    }else{
                        $desc = 'Shipping Date : ' + $(this).val();
                    }
                    $el_desc.val($desc);
                }
            }
        }
    });

    // todo. move to lib. common lib to calculate date difference
    $.fn.calculateDiffDays = function(day1,day2){
        d1 = $.fn.parseDate(day1);
        d2 = $.fn.parseDate(day2);
        dd = (d2-d1)/(1000*60*60*24); 
        return parseInt(dd);
    };

    $.fn.parseDate = function(date){
        var Ymd = date.split('-');
        return new Date(Ymd[0],Ymd[1],Ymd[2]);
    };

	/*
    var $clickNode = $('#order table tr');
    $('#order').bind('click',function(event){
        if($tgt.is('input')){
          $tgt.select();
        }
    });
    */

    /////////////////////////////////////////////////////////////////////////////
    // account
    /////////////////////////////////////////////////////////////////////////////
    // after event keydown completed , we need to release the binding

    $('#storeinfo input[name="accountno"]').bind('keydown',function(e){
        if(e.keyCode == 13){
        	e.preventDefault(); $.fn.storeSubmit(e);
        }
    });
    $('#storeinfo input[name="store_name"]').bind('keydown',function(e){
        if(e.keyCode == 13){
        	e.preventDefault(); $.fn.storeSubmit(e);
        }
    });
	
    $.fn.storeSubmit = function(e){
        param = '';
        accountno = $('input[name=accountno]').val();
        if(accountno)	param += 'filter_accountno=' + encodeURIComponent(accountno);
        /*
        salesrep = $('input[name=salesrep]').val();
        if(salesrep)  param += '&filter_salesrep=' + encodeURIComponent(salesrep);
        */
        store_name = $('input[name=store_name]').val();
        if(store_name)  param += '&filter_name=' + encodeURIComponent(store_name);
        $.ajax({
            type:'get',
            url:'/store/lookup/callback',
            dataType:'html',
            data:param,
            success:function(html){
                $.fancybox(html);
            } 
        });
    }
    
    $('#account-insert').bind('click',function(e){
        e.preventDefault();
        var that = $(e.target),
            store_id = $('#storeinfo').find('input[name=store_id]').val(),
        	data = '',
            mode = '';

        if (that.is('input'))	return false;

        if (that.hasClass('account-new')) {
            mode = 'new';
        } else {
            mode = 'edit';
        }

        data = {
            'store_id' : store_id,
            'mode' : mode
        }
        $.ajax({
            type: 'get',
            dataType: 'html',
            url: 'store/list/callInsertPannel',
            data: data,
            success:function(html){
                $.fancybox(html);
            }
        });
    });

	$('.update-account').bind('click',function(e){
        e.preventDefault();
        var that = $(e.target),
            store_id = that.data('id'),
        	data = '';
        data = {
            'store_id' : store_id,
            'mode' : 'update'
        }
        $.ajax({
            type: 'get',
            dataType: 'html',
            url: 'store/list/callInsertPannel',
            data: data,
            success:function(html){
                $.fancybox(html);
            }
        });
    });

	$('.update-account2').bind('click',function(e){
        e.preventDefault();
        var that = $(e.target),
            store_id = that.data('id'),
        	data = '';
        data = {
            'store_id' : store_id,
            'mode' : 'update',
            'from' : 'account'
        }
        $.ajax({
            type: 'get',
            dataType: 'html',
            url: 'store/list/callInsertPannel',
            data: data,
            success:function(html){
                $.fancybox(html);
            }
        });
    });

    $('#account-check').live('focusout', function(e){
        var that = $(e.target),
            accountno = that.val();
        $.ajax({
            type:'post',
            url:'/store/list/getUniqAccountno',
            data:'accountno=' + accountno,
            dataType: 'json',
            success:function(res){
                if (res.code == '200') {
                    that.val(res.message);
                } else {
                    alert('fail to get new account number');
                    return false;
                }
            }
        });
    })

    $('#submit-update-store').live('click',function(e) {

        e.preventDefault();
        var that = $('#updateForm'),
        	accountno = that.find('input[name=accountno]').val();
        	
        if (accountno == '' ){
        	alert ('Input Account Number');
        	return false;
        }

        if (that.find('input[name=name]').val() == '' ){
        	alert ('Input Account Name');
        	return false;
        }

        if (that.find('input[name=accountno]').val() == '' ){
        	alert ('Input Account Number');
        	return false;
        }

        that.submit();
        
        /*
        var action = that.attr('action'),
        	data = that.serialize();

        $.ajax({
            type: 'POST',
            url: action, 
            data: data,
            dataType: 'json',
            success: function(res){
                if (res.code == '200') {
                    $.fancybox.close();
                }
            }
        });
        */
    });


    $('#account-history').bind('click',function(e){
    	
        e.preventDefault();
        var accountno = $('#storeinfo').find('input[name=accountno]').val();
        //url = '/report/account&accountno=' + accountno;
        var url = '/sales/list&filter_txid=' + accountno;
        window.open(url);
    });

    /////////////////////////////////////////////////////////////////////////////
    // product
    /////////////////////////////////////////////////////////////////////////////

    $('#product-insert').bind('click',function(e){
    	e.preventDefault();
        $.ajax({
            type:'get',
            url:'/product/price/callUpdatePannel',
            dataType:'html',
            data:'mode=insert',
            success:function(html){
                $.fancybox(html);
            }
        });
    });

	/*
    $('#product-update').bind('click',function(e){
    	e.preventDefault();
        $.ajax({
            type:'get',
            url:'/product/price/callUpdatePannel',
            dataType:'html',
            data:'mode=update',
            success:function(html){
                $.fancybox(html);
            }
        });
    });
    */

    $('#flush-product-cache').bind('click',function(e) {
    	e.preventDefault();
        /* TODO
         * set general loading method
         * james, 2014-03
         */
        $.ajax({
            type:'get',
            url:'/common/cache/ajaxFlushCache',
            dataType:'json',
            data:'reset=true&key=products',
            success:function(res){
                if (res.code == '200') {
                    localStorage.setItem('p',res.message);
                    //alert('flush product cache done');
                }
            }
        });
    });

    
    $('#save-product').live('click',function(e){
        e.preventDefault();
        var that = $('#updateForm'),
            model = that.find('input[name=model]').val();

        if (model == '' ){
            alert ('Input item code');
            return false;
        }

        if (that.find('input[name=name]').val() == '' ){
            alert ('Input Product Name');
            return false;
        }

        if (that.find('input[name=rt_price]').val() == '' ){
            alert ('Input Retail price');
            return false;
        }
        
        var action = $('#updateForm').attr('action');
        $.ajax({
            type: 'POST',
            url: action, 
            data: $('#updateForm').serialize(),
            dataType: 'json',
            success: function(res){
                if (res.code == '200') {
                    $('#flush-product-cache').click();
                    $.fancybox.close();
                }
            }
        });
    });

	$('#product-model').live('focusout', function(e){
        var that = $(this),
            model = that.val();
        $.ajax({
            type:'post',
            url:'product/price/getProductByModel',
            data:'model=' + model,
            dataType: 'json',
            success:function(res){
                if (res.code == '200') {
                    alert(model + ' already exist. \nPlease choose other name');
                    that.val('').focus();
                }
            }
        });
    })
    
    /////////////////////////////////////////////////////////////////////////////
    // order
    /////////////////////////////////////////////////////////////////////////////

    $('#show-invoice').bind('click',function(e) {
    	e.preventDefault();
    	var txid = $('input[name="txid"]').val();
    	var url = '/invoice/sheet&txid=' + txid;
    	var option = 'width=800,scrollbars=1';
    	window.open(url, 'invoice', option);
    });
    
    $.fn.loader = function( msg, timeout ) {
        var html = '<div id="loader">'+ msg +'</div>',
            p = $('#product-atc-container').position();
        $("body").append(html);
        $("#loader").css('padding-top', p.top + 200 );

        setTimeout ( function() {
                $("#loader").remove();
            }, timeout
        );
    }
});
