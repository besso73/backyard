<?php
class ModelReportSale extends Model{
  private $isApprove = false;
  public function stat($request){
    // today
    if(!is_null($request['filter_from']) && !is_null($request['filter_to'])){
      $thismonth = mktime(0, 0, 0, date(substr($request['filter_from'],5,2)), date(substr($request['filter_from'],8,2)), date(substr($request['filter_from'],0,4)));
      $from = date("Y-m-01",$thismonth);
      $to   = date("Y-m-t",$thismonth);
      $month = date("Ym",$thismonth);
		}
    $working = $this->util->getWorkingDays($from,$to);
    //$this->log->aPrint( $working );
    $sql = "
        select x.order_user,substr(x.order_date,1,7) as order_date,
               rs.target,round( rs.target / $working ) as day_target,
               sum(x.subtotal) as order_price,
               round( (sum(x.subtotal) / rs.target * 100 ) , 2 ) as percent,
               round( (sum(x.subtotal) / ( rs.target / $working ) * 100 ) , 2 ) as day_percent,
               count(x.txid) as cnt,
               sum(IF(st.storetype = 'R',1,0)) as rcnt,
               sum(IF(st.storetype = 'W',1,0)) as wcnt,
               substr(x.order_date,1,10) as today
          from transaction x, rep_stat rs, storelocator st
         where rs.month = '$month'
           and x.order_user = rs.rep and x.store_id = st.id
           and x.status in ('1','2','3')";
    if( $this->isApprove == true ){
      $sql .= " and x.approve_status = 'approve'";
    }
    if(!is_null($request['filter_from']) && !is_null($request['filter_to'])){
      if( $request['filter_from'] == $request['filter_to'] ){
        $from = $to = $request['filter_from'];
      }else{
        $from = $to = date("Y-m-d");
      }
		}
    $sql .= " AND substr(x.order_date,1,10) between '" . $from . "' and '" . $to . "'";
    //todo. remove exclude account from default view
    //if( $request['hidden'] == false ){      $sql .= $exclude;    }
    $sql .= " group by x.order_user,substr(x.order_date,1,7)";

    if( $request['sort'] == '' ){
      $sql.= " order by (sum(x.subtotal) / rs.target )";
    }else{
      $sql.= " order by " . $request['sort'];
    }
    $sql.= " " . $request['order'];
    //$this->log->aPrint( $sql ); exit;
    $query = $this->db->query($sql);
    $aToday = $query->rows;

    // outer join fail shit
    $aSales = array();
    foreach($query->rows as $row){  array_push($aSales,$row['order_user']); }
    $i = count($aToday);
    if($i>0){
      foreach($this->user->getSales() as $sales){
        // todo. it's adhoc one. need to recover next month
        //if('BJ' == $sales) continue;
        if( !in_array($sales,$aSales) && COMPANY_KEY != $sales ){
          $aToday[$i] = $aToday[$i-1];
          $aToday[$i]['order_user'] = $sales;
          $sql = "select target from rep_stat where month = '$month' and rep = '$sales'";
          //$this->log->aPrint( $sql );
          $query = $this->db->query($sql);
          if( isset( $query->row['target'] ) ){
            $target = $query->row['target'];
            //$this->log->aPrint( $target );
            $aToday[$i]['target'] = $target;
            $aToday[$i]['day_target'] = round($target/$working);
            $aToday[$i]['order_price'] = 0;
            $aToday[$i]['percent'] = 0;
            $aToday[$i]['day_percent'] = 0;
            $aToday[$i]['cnt'] = 0;
            $aToday[$i]['rcnt'] = 0;
            $aToday[$i]['wcnt'] = 0;
            $i++;
          }
        }
      }
    }
    $rtn['today'] = $aToday;

    /////////////// this month ////////////////////////
    $sql = "
        select x.order_user,substr(x.order_date,1,7) as order_date,rs.target,
               sum(x.subtotal) as order_price,
               round( (sum(x.subtotal) / rs.target * 100 ) , 2 ) as percent,
               count(x.txid) as cnt,sum(IF(st.storetype = 'R',1,0)) as rcnt,
               sum(IF(st.storetype = 'W',1,0)) as wcnt
          from transaction x, rep_stat rs, storelocator st, user u
         where concat(substr(x.order_date,1,4),substr(x.order_date,6,2)) = rs.month
           and u.username = x.order_user and u.status = 1
           and x.order_user = rs.rep and x.store_id = st.id and x.status in ('1','2','3')";
    if( $this->isApprove == true ){
      $sql .= " and x.approve_status = 'approve' and x.order_user != COMPANY_KEY";
    }
    if(!is_null($request['filter_from']) && !is_null($request['filter_to'])){
      $thismonth = mktime(0, 0, 0, date(substr($request['filter_from'],5,2)), date(substr($request['filter_from'],8,2)), date(substr($request['filter_from'],0,4)));
      $from = date("Y-m-01",$thismonth);  $to = date("Y-m-t",$thismonth);
		}
 		$sql .= " AND substr(x.order_date,1,10) between '" . $from . "' and '" . $to . "'";
    //if( $request['hidden'] == false ){      $sql .= $exclude;    }
    $sql .= " group by x.order_user,substr(x.order_date,1,7)";
    if( $request['sort'] == '' ){ $sql.= " order by (sum(x.subtotal) / rs.target )";
    }else{  $sql.= " order by " . $request['sort']; }
    $sql.= " " . $request['order'];
    //$this->log->aPrint( $sql );
    $query = $this->db->query($sql);
    $aMonth = $query->rows;

    // outer join fail shit
    $aSales = array();
    foreach($query->rows as $row){  array_push($aSales,$row['order_user']); }
    $i = count($aMonth);
    if($i>0){
      foreach($this->user->getSales() as $sales){
        // todo. it's adhoc one. need to recover next month
        //if('AK2' == $sales) continue;
        if( !in_array($sales,$aSales) && COMPANY_KEY != $sales ){
          $aMonth[$i] = $aMonth[$i-1];
          $aMonth[$i]['order_user'] = $sales;
          $sql = "select target from rep_stat where month = '$month' and rep = '$sales'";
          //$this->log->aPrint( $sql );
          $query = $this->db->query($sql);
          if( isset( $query->row['target'] ) ){
            $target = $query->row['target'];
            //$this->log->aPrint( $target );
            $aMonth[$i]['target'] = $target;
            $aMonth[$i]['day_target'] = round($target/$working);
            $aMonth[$i]['order_price'] = 0;
            $aMonth[$i]['percent'] = 0;
            $aMonth[$i]['day_percent'] = 0;
            $aMonth[$i]['cnt'] = 0;
            $aMonth[$i]['rcnt'] = 0;
            $aMonth[$i]['wcnt'] = 0;
            $i++;
          }
        }
      }
    }
    $rtn['this_month'] = $aMonth;

    /////////////// past month ////////////////////////
    $sql = "
        select x.order_user,substr(x.order_date,1,7) as order_date,rs.target,
               sum(x.subtotal) as order_price,
               round( (sum(x.subtotal) / rs.target * 100 ) , 2 ) as percent,
               count(x.txid) as cnt,sum(IF(st.storetype = 'R',1,0)) as rcnt,
               sum(IF(st.storetype = 'W',1,0)) as wcnt
          from transaction x, rep_stat rs, storelocator st
         where concat(substr(x.order_date,1,4),substr(x.order_date,6,2)) = rs.month
           and x.order_user = rs.rep and x.store_id = st.id
           and x.status in ('1','2','3')";
    if( $this->isApprove ){
      $sql .= " and x.approve_status = 'approve' and x.order_user != COMPANY_KEY";
    }
    if(!is_null($request['filter_from']) && !is_null($request['filter_to'])){
      $lastmonth = mktime(0, 0, 0, date(substr($request['filter_from'],5,2))-1, date(substr($request['filter_from'],8,2)), date(substr($request['filter_from'],0,4)));
      $from = date("Y-m-d",$lastmonth);
      $to   = date("Y-m-t",$lastmonth);
		}
		/*
    Could you please tell me what is that deposit and is it a good idea to clean the rod before I put it back? Thanks. PS Love your site.
    */
 		$sql .= " AND substr(x.order_date,1,10) between '" . $from . "' and '" . $to . "'";
    //if( $request['hidden'] == false ){      $sql .= $exclude;    }
    $sql .= " group by x.order_user,substr(x.order_date,1,7)";
    if( $request['sort'] == '' ){
      $sql.= " order by (sum(x.subtotal) / rs.target )";
    }else{
      $sql.= " order by " . $request['sort'];
    }
    $sql.= " " . $request['order'];
    //$this->log->aPrint( $sql );
    $query = $this->db->query($sql);
    $rtn['last_month'] = $query->rows;
    //$this->log->aPrint( $rtn );
    return $rtn;
  }



  ##############################################################################
  ###### Product
  ##############################################################################
  public function stat_product($request){
    //$this->log->aPrint( $request ); exit;
    // today
/*****
    if(!is_null($request['filter_from']) && !is_null($request['filter_to'])){
      $thismonth = mktime(0, 0, 0, date(substr($request['filter_from'],5,2)), date(substr($request['filter_from'],8,2)), date(substr($request['filter_from'],0,4)));
      $from = date("Y-m-01",$thismonth);
      $to   = date("Y-m-t",$thismonth);
      $month = date("Ym",$thismonth);
		}
    $working = $this->util->getWorkingDays($from,$to);
    //$this->log->aPrint( $working );
    $group = $rep = '';
    if($group == 'rep') $rep = "substr(substring_index(s.txid,'-',-2),1,2),";
    $sql = "
        select $rep s.model as model, pd.name, sum(s.order_quantity) as qty, sum(s.order_quantity * s.price1 ) as total
          from sales as s
          join ( product as p , product_description as pd ) on s.model = p.model and p.product_id = pd.product_id
          join ( transaction x ) on x.txid = s.txid
         where s.order_quantity > 0
        ";
    if(!is_null($request['filter_from']) && !is_null($request['filter_to'])){
      if( $request['filter_from'] == $request['filter_to'] ){
        $from = $to = $request['filter_from'];
      }else{
        $from = $to = date("Y-m-d");
      }
		}
    $sql .= " AND substr(x.order_date,1,10) between '" . $from . "' and '" . $to . "'";    
    $sql .= " group by $rep s.model order by $rep substr(s.model,1,2),sum(s.order_quantity * s.price1 ) desc";
    //$this->log->aPrint( $sql );
    $query = $this->db->query($sql);
    $aProduct = array();
    foreach($query->rows as $row){
      $aProduct[ $row['model'] ] = array($row['name'],$row['qty'],$row['total']);
    }
    //$this->log->aPrint( $aProduct);
    $aCat = $this->config->getCatalog();
    //$this->log->aPrint( $aCat );
    // sort later
    $aCatSum = array();
    foreach($aCat as $k => $group){
      foreach($group as $m){
        if( $m != '' ){
          if( isset($aProduct[$m]) && is_array($aProduct[$m]) ){
            $aCatSum[$k][$m] = $aProduct[$m]; 
          }
        }
      }
    }
    $rtn['today'] = $aCatSum;
*****/
    $rtn['today'] = array();
    
    
    //$this->log->aPrint( $request );

    // this month
    if(!is_null($request['filter_from']) && !is_null($request['filter_to'])){
      //$thismonth = mktime(0, 0, 0, date(substr($request['filter_from'],5,2)), date(substr($request['filter_from'],8,2)), date(substr($request['filter_from'],0,4)));
      $from = $request['filter_from'];
      $to   = $request['filter_to'];
      $month = substr($request['filter_from'],5,2);
		}
    /*
    */
		//$this->log->aPrint( $month );
    //$this->log->aPrint( $working );
    $group = $rep = '';
    if($group == 'rep') $rep = "substr(substring_index(txid,'-',-2),1,2),";
    $sql = "
        select $rep s.model as model, pd.name, sum(s.order_quantity) as qty, sum(s.order_quantity * s.price1 ) as total
          from sales as s
          join ( transaction x ) on x.txid = s.txid
          join ( product as p , product_description as pd ) on s.model = p.model and p.product_id = pd.product_id
         where s.order_quantity > 0
    ";
    $sql .= " AND substr(x.order_date,1,10) between '" . $from . "' and '" . $to . "'";    
    $sql .= " group by $rep s.model order by $rep substr(s.model,1,2),sum(s.order_quantity * s.price1 ) desc";
    //$this->log->aPrint( $sql );
    $query = $this->db->query($sql);
    $aProduct = array();
    //$this->log->aPrint( $query->rows ); exit;
    foreach($query->rows as $row){
      $aProduct[ $row['model'] ] = array($row['name'],$row['qty'],$row['total']);
    }
    //$this->log->aPrint( $aProduct);
    $aCat = $this->config->getCatalog();
    //$this->log->aPrint( $aCat );
    $aCatSum = array();
    foreach($aCat as $k => $group){
      foreach($group as $m){
        if( $m != '' ){
          if( isset($aProduct[$m]) && is_array($aProduct[$m]) ){
            $aCatSum[$k][$m] = $aProduct[$m]; 
          }
        }
      }
    }
    //exit;
    //$this->log->aPrint( $aCatSum ); exit;
    $rtn['this_month'] = $aCatSum;
    return $rtn;
  }

  ##############################################################################
  ###### Sales
  ##############################################################################
  public function stat_sales($request){
    //$this->log->aPrint( $request );
    // this month
    if(!is_null($request['filter_from']) && !is_null($request['filter_to'])){
      $from = $request['filter_from'];
      $to = $request['filter_to'];
      /*
      $thismonth = mktime(0, 0, 0, date(substr($request['filter_from'],5,2)), date(substr($request['filter_from'],8,2)), date(substr($request['filter_from'],0,4)));
      $from = date("Y-m-01",$thismonth);
      $to   = date("Y-m-t",$thismonth);
      $month = date("Ym",$thismonth);
      */
		}
    //$this->log->aPrint( $working );
    $group = $rep = '';
    if($group == 'rep') $rep = "substr(substring_index(txid,'-',-2),1,2),";
    $sql = "
        select tx.order_user, $rep s.model as model, pd.name, sum(s.order_quantity) as qty, sum(s.order_quantity * s.price1 ) as total
          from sales as s
          join ( product as p , product_description as pd ) on s.model = p.model and p.product_id = pd.product_id
          join ( transaction as tx ) on s.txid = tx.txid
         where s.order_quantity > 0
    ";
    $sql .= " AND substr(tx.order_date,1,10) between '" . $from . "' and '" . $to . "'";
    $sql .= " group by tx.order_user,$rep s.model ";
    $sql .= " order by tx.order_user,p.model,sum(s.order_quantity * s.price1 ) desc";
    //$this->log->aPrint( $sql ); exit;
    $query = $this->db->query($sql);
    $aProduct = array();
    $aSumProduct = array();
    //$this->log->aPrint( $query->rows );
    $tmp_order_user = '';
    $qty_sum = $total_sum = 0;
    array_push( $query->rows , array('order_user'=>'dummy','model'=>'dummy','name'=>'dummy','name'=>'dummy','qty'=>0,'total'=>0) );
    foreach($query->rows as $row){
      $order_user = $row['order_user'];
      $model      = $row['model'];
      $name       = $row['name'];
      $qty        = $row['qty'];
      $total      = $row['total'];
      if( $tmp_order_user != $order_user ){
        $aProduct[$order_user] = array();
        $aSumProduct[$order_user] = array();
        if( $qty_sum > 0 )  $aSumProduct[$tmp_order_user] = array($qty_sum,$total_sum);
        $qty_sum = $total_sum = 0;
        $tmp_order_user = $order_user;
      }
      array_push($aProduct[$order_user],array($model,$name,$qty,$total));
      $qty_sum += $qty;
      $total_sum += $total;
    }

    //$this->log->aPrint( $aProduct); exit;
    $aCat = $this->config->getCatalog();
    $aCatSum = $aRtn = $aSumProduct = array();

    foreach( $aProduct as $order_user => $line){
      $sum = 0;
      if( $order_user != 'dummy' ){
        $aRtn[$order_user] = array();
        $aSumProduct[$order_user] = array();
        //$this->log->aPrint( '=======================================' );
        //$this->log->aPrint( $order_user );
        //$this->log->aPrint( '=======================================' );
        foreach($aCat as $k => $group){
          $t3 = $t4 = 0;
          //$this->log->aPrint( '-----------------------------------------' );
          //$this->log->aPrint( $k );
          //$this->log->aPrint( '-----------------------------------------' );
          foreach($group as $m){
            $qty = $total = 0;
            if( $m != '' ){
              foreach($line as $row){
                if( $row[0] == $m ){
                  //$this->log->aPrint( $row );
                  //$this->log->aPrint( $m ); $this->log->aPrint( $row ); break;
                  //$model      = $row[0];
                  //$name       = $row[1];
                  $qty        += $row[2];
                  $total      += $row[3];
                  //$qty_percent = round($row[2] / $aSumProduct[$order_user][0] * 100 , 2);
                  //$total_percent = round($row[3] / $aSumProduct[$order_user][1] * 100 , 2);
                  //array_push( $aRtn[$order_user] , array( $model, $name, $qty, $total, $qty_percent, $total_percent ) );
                }
              }
              //$qty = $total = 0;
              if($total > 0){
                $t3 += $qty;
                $t4 += $total;
                //$this->log->aPrint( $k . '::' . $t3 . '::' . $t4 );
                //$aRtn[$order_user][$k]['qty']   += $qty;
                //$aRtn[$order_user][$k]['total'] += $total;
                $aRtn[$order_user][$k]['total'] = array();
                $aRtn[$order_user][$k]['qty']   = array();
                $aRtn[$order_user][$k]['total'] = $t4;
                $aRtn[$order_user][$k]['qty'] = $t3;
              }
            }
          }
          $sum += $t4;
          //$this->log->aPrint( $sum );
        }
        $aSumProduct[$order_user] = $sum;
      } // if not dummy
    }
    //$this->log->aPrint( $aRtn ); exit;
    //$this->log->aPrint( $aSumProduct ); exit;

    $aT1 = array();
    foreach($aRtn as $order_user => $row){
      array_multisort($row,SORT_DESC);
      $aT1[$order_user] = $row;
    }
    //$this->log->aPrint( $aT1 ); exit;
    $aReturn = array();
    foreach($aT1 as $order_user => $line){
      //$this->log->aPrint( $row );
      foreach($line as $catalog => $row){
        $total      = $row['total'];
        $qty        = $row['qty'];
        $total_percent = round($total / $aSumProduct[$order_user] * 100 , 2);
        $row['percent'] = $total_percent;
        //array_push( $aReturn[$order_user] , array( $model, $name, $qty, $total, $qty_percent, $total_percent ) );
        $line[$catalog] = $row;
      }
      $aReturn[$order_user] = $line;
    }
    //$this->log->aPrint( $aReturn ); exit;
    $rtn['this_month'] = $aReturn;
    return $rtn;
  }



  ##############################################################################
  ###### account
  ##############################################################################
  public function stat_account($request){
    // today
    if(!is_null($request['filter_from']) && !is_null($request['filter_to'])){
      $thismonth = mktime(0, 0, 0, date(substr($request['filter_from'],5,2)), date(substr($request['filter_from'],8,2)), date(substr($request['filter_from'],0,4)));
      $from = date("Y-m-01",strtotime("-1 years"));
      $to   = date("Y-m-t",$thismonth);
      $month = date("Ym",$thismonth);
		}
    $working = $this->util->getWorkingDays($from,$to);
    //$this->log->aPrint( $working );
    $group = $rep = '';
    if($group == 'rep') $rep = "substr(substring_index(s.txid,'-',-2),1,2),";
    $accountno = $request['accountno'];
    $sql = "
        select x.txid,x.subtotal,s.model,pd.name,x.order_date,
               sum(s.order_quantity*s.price1) as sum,s.order_quantity
          from transaction x, storelocator sl, sales s, product_description pd, product p
         where x.store_id = sl.id
           and p.product_id = pd.product_id and s.product_id = pd.product_id
           and s.txid = x.txid
           and lower(sl.accountno) = '$accountno'
           and s.order_quantity > 0
    ";
    /*
    if(!is_null($request['filter_from']) && !is_null($request['filter_to'])){
      if( $request['filter_from'] == $request['filter_to'] ){
        $from = $to = $request['filter_from'];
      }else{
        $from = $to = date("Y-m-d");
      }
		}
		*/
    $sql .= " AND substr(s.order_date,1,10) between '" . $from . "' and '" . $to . "'";    
    $sql .= " group by x.txid,s.model order by x.order_date, p.model desc";
    //$this->log->aPrint( $sql );
    $query = $this->db->query($sql);
    //$this->log->aPrint( $query->rows );
    $aAcctno = array(); $i = 0; $tmpTxid = $txid = '';
    foreach( $query->rows as $row){
      $txid = $row['txid'];
      if( !$txid )  $tmpTxid = $txid;
      if( $txid != $tmpTxid ){
        $tmpTxid = '';
      }
      $aAcctno[$txid][$i] = $row;
      $i++;
    }
    //$this->log->aPrint( $aAcctno ); exit;
    return $aAcctno;
  }



  ##############################################################################
  ###### validate
  ##############################################################################
  public function validate($request,$txid=''){
    $aResponse = array();
    //$this->log->aPrint( $request ); exit;
    $from = isset( $request['filter_from'] ) ? $request['filter_from'] : date("Y-m-d");
    $to = isset( $request['filter_to'] ) ? $request['filter_to'] : date("Y-m-d");

    if( $txid != '' ){
      $sql = "select txid, ( order_price - cod - lift ) as torder, discount from transaction where txid = '$txid'";
    }else{
      $sql = "select txid, ( order_price - cod - lift ) as torder, discount from transaction where substr(order_date,1,10) between '$from' and '$to'";
    }
    $query = $this->db->query($sql);
    $aTX = $query->rows;

    foreach( $aTX as $tx ){
      $tId = $tx['txid'];
      $tOrder = $tx['torder'];
      $tDiscount = $tx['discount'];
      $aDiscount = array();

      $sql = "select x.txid,";
      if( $tDiscount != '' ){
        $aDiscount = json_decode( $tDiscount );
        $i=1;
        $dc1 = $dc2 = 0;
        foreach( $aDiscount as $dc ){
          $aDC = preg_split('/\|/',$dc);
          $var = 'dc'.$i;
          $$var = $aDC[0];
          $i++;
        }
        $sql.= " sum( round( s.order_quantity *  s.price1 * ( (100 - s.discount) /100 )  * ( ( 100 - s.discount2) / 100)  * ( ( 100 - $dc1) / 100)  , 2 )  * ( ( 100 - $dc2) / 100) ) as sale_price";
      }else{
        $sql.= " sum( round( s.order_quantity *  s.price1 * ( (100 - s.discount) /100 )  * ( ( 100 - s.discount2) / 100)  , 2 ) ) as sale_price";
      }
      $sql.= " from transaction x, sales s where x.txid = s.txid
                and x.txid = '$tId' group by x.txid";
      $query = $this->db->query($sql);
      $aSale = $query->row;
      if( round($tOrder) != round($aSale['sale_price']) ){
        $aResponse['txid'] = $tx['txid'];
        $aResponse['x_price'] = $tx['torder'];
        $aResponse['s_price'] = round($aSale['sale_price'],2);
      }
    }
    return $aResponse;
  }

	public function ordersales($request){
    $from  = $request['filter_from'];
    $to    = $request['filter_to'];
    $group = html_entity_decode($request['group']);
    $aCat = $this->config->getCatalog();
    $comma = implode("','",$aCat[$group]);
    $comma = "'" . $comma . "'";
    $sql = "
      select x.order_user as rep, sum( s.order_quantity ) as qty
        from transaction x, sales s
       where x.txid = s.txid
         and substr(x.order_date,1,10) between '$from' and '$to'
         and s.model in ( $comma )
       group by x.order_user
       order by sum( s.order_quantity ) desc
    ";
    //$this->log->aPrint( $sql ); exit;
    $query = $this->db->query($sql);
    return $query->rows;
	}
}
?>
