<?php
/* todo. need to make copy functionality */
class ModelSalesOrder extends Model{
    public function getSalesQuantity($model){
        $result = array();
        $sql = "select sum(s.order_quantity) as locked
                  from transaction t, sales s
                 where t.txid = s.txid
                   and t.shipped_yn != 'Y'
                   and s.model = '$model'";
        //$this->log->aPrint( $sql ); exit;
        $query = $this->db->query($sql);
        $locked = $query->row['locked'];
        if( $locked > 0 ){
            $sql = "select sum(s.order_quantity) as rep_total, t.order_user,'$locked' as locked
                      from transaction t, sales s
                     where t.txid = s.txid
                       and t.shipped_yn != 'Y'
                       and s.model = '$model'
                     group by t.order_user";
            //echo $sql;
            $query = $this->db->query($sql);
            $result = $query->rows;
        }
    	return $result;
    }
    
    /* lookup existing Txid and retrieve next offset from DB
     * return 1, if exists already */
    public function getTxid($txid){
        $sql = "select max( substr(txid,-1) + 1 ) as offset from transaction where txid like '%{$txid}%' order by txid";
        $query = $this->db->query($sql);
        isset($query->row['offset']) ? $offset = $query->row['offset'] : $offset = '1';
        $txid = $txid . '-' . $offset;
        return $txid;
    }
    
    public function updateApprove($txid,$status){
      $today = date('Y-m-d');
      $approve_user = $this->user->getUserName();
      if($status == 'approve'){
        $sql = "select status from transaction where txid = '$txid'";
        $query = $this->db->query($sql);
        if( $query->row['status'] == 1 ){
          $sql = "select model,(order_quantity + free + damage + promotion) as cnt from sales where txid = '$txid'";
    		  $query = $this->db->query($sql);
    		  foreach($query->rows as $row){
    		    $model = $row['model'];
    		    $cnt   = $row['cnt'];
    		    $user = $this->user->getUserName();
    
            $sql = "select quantity from product where model = '$model'";
            $query = $this->db->query($sql);
            $was =  $query->row['quantity'];
            $sql = "update product set quantity = quantity - " . $cnt . " where model = '" . $model . "'";
    	      //$this->log->aPrint( $sql );
    	      if($this->db->query($sql)){
              $msg = "[UPDATE] model $model saled : $was - $cnt by $user ( $txid )";
              $this->log->write($msg,'inventory.log');
              /*******
    	        $diff = $was - $cnt;
    	        if( $diff < 0 ){
    	          $mail = new Mail();
    	          $mail->protocol = $this->config->get('config_mail_protocol');
    	          $mail->hostname = $this->config->get('config_smtp_host');
    	          $mail->username = $this->config->get('config_smtp_username');
    	          $mail->password = $this->config->get('config_smtp_password');
    	          $mail->port = $this->config->get('config_smtp_port');
    	          $mail->timeout = $this->config->get('config_smtp_timeout');
                $subject = 'Inventory Alert!!! ' . date('m-d h:i');
                $body = $msg;
                $aReceiver = array(
                  'james@live.com',
                  '8472392086@VTEXT.COM'
                );
                foreach($aReceiver as $receiver){
                  $mail->setTo($receiver);
                  $mail->setFrom($this->config->get('config_email'));
                  $mail->setSender('james@live.com');
                  $mail->setSubject($subject);
                  $mail->setText($body);
                  $mail->send();
                }
    	        }
    	        *******/
    	      }
    	    }
    	    //return true;  // bad code
    	  }
    	  //return false;
      }
    
      $sql = "update transaction set approve_status = '$status', approved_user = '$approve_user', ";
      $sql.= " approved_date = '$today', order_date = IF( status != '2' , now(), order_date ) ";
      $sql.= " where txid = '$txid'";
      //$this->log->aPrint( $sql ); exit;
      if($this->db->query($sql)){
        return true;
      }
      /*
        todo. if approve -> pending -> approve could decrease twice the stock. rare case. ignore , james 201108
       */
    }
    
      public function insertTransaction($data){
    
          //$this->log->aPrint( $data ); exit;
    
          //$executor = $this->user->getUsername();
          $order_user = $this->user->getUsername();
          $order_date = $data['order_date'] . ' ' . date("H:i:s");
          $sql = "INSERT INTO transaction
                  	(txid,store_id,description,order_user,sold_ym,total,subtotal,
          			 order_date, shipped_yn, payment,ship_method,
          			 cod,ship_appointment,status,discount) 
          		VALUES (";
          $sql.= " '" . $data['txid'] . "','" . $data['store_id'] . "','" . $data['description'] . "','";
          $sql.= $order_user . "','" . $data['sold_ym'] . "','";
          $sql.= $data['total'] . "','" . $data['subtotal'] . "','" . $order_date . "','";
          $sql.= $data['shipped_yn'] . "','";
          $sql.= $data['payment'] . "','" . $data['ship_method'] . "','" . $data['ship_cod'] . "','";
          $sql.= $data['ship_appointment'] . "','". $data['status'] ."','" . $data['discount'] . "')";
    
          /*
          if( AUTO_INVOICE == true ){
              $invoiceNo = $this->generateInvoiceNo();
              $sql .= "$invoiceNo";
          }else{
              $sql .= "''";
          }
          */
          
          //echo '<pre>'; print_r($sql); echo '</pre>';	exit;
          if($this->db->query($sql)){
    		return true;
          }else{
              if( !$this->db->query($sql) ){
                  //$this->sendJames($aErr);
                  echo 'Error to insert Transaction';
                  exit;
                  return false;
              }
          }
          return true;
      }
    
      public function generateInvoiceNo(){
          $sql = "select max(invoice_no) as inv_no from transaction";
          $query = $this->db->query($sql);
          $maxInvoiceNo = $query->row['inv_no'];
          if( strlen($maxInvoiceNo) == 8 ){
            $invoiceNo = date("ym") . substr($maxInvoiceNo,4) + 1 ;
          }else{
            $invoiceNo = date("ym") . '0001' ;
          }
          return $invoiceNo;
      }
    
    // todo. input arg could be different in cases.
    // so it's better to check if already existed model or not
    public function insertSales($data){

          //$this->log->aPrint( $data ); exit;

          $aExist = array();
          $txid = $data[0][0];
          $order_date = $data[0][1];
          
          $sql = "delete from sales where txid = '$txid'";
          $this->db->query($sql);
          $query = $this->db->query($sql);
          
          foreach ($data as $row) {
          
              $productId   = $row[2];
              $model       = $row[3];
              //$desc        = $row[4];
              $price       = $row[4];
              $inventory   = $row[5];
              $orderQty    = $row[6];
              $shipped     = $row[7];
              $discount    = $row[8];
              $total       = $row[9];
              
              //$desc = $this->db->escape($desc);
          
          	$sql = "INSERT INTO sales 
          			(txid, model, product_id, order_quantity, shipped, price1, discount, total_price, order_date, inventory)
          			 VALUES ('$txid', '$model', '$productId', '$orderQty', '$shipped', '$price', '$discount',
          					'$total','$order_date', '$inventory' )";
          					
              //$this->log->aPrint( $sql );              exit;
              if( !$this->db->query($sql) ){
                  echo "error while insert sales : $sql";
                  exit;
                  //$this->sendJames($aErr);
              }
          }
          return true;
    }
      
      
      
    public function insertShip($data){
      $sql = "delete from ship where txid = '" . $data['txid'] . "'";
      if($this->db->query($sql)){
    	  // todo. set flag to control ok , james-201103
    	  //return true;
    	}else{
    		//return false;
    	}
      for($i=0;$i<count($data[1]);$i++){
        // todo. use ship_date as key, it's not strong one , james-201103 
        if($data['txid']){
          $sql = "INSERT INTO ship ";
          $sql.= " (txid,method,ship_date,lift,cod,ship_appointment,ship_comment,ship_user) values (";
          $sql.= " '" . $data['txid'] . "','" . $data[0][$i] . "','" . date('Y-m-d') . "','";
          $sql.= $data[2][$i] . "','" . $data[3][$i] . "','";
          $sql.= $data[4][$i] . "','" . addslashes($data[5][$i]) . "','";
          $sql.= $data[6][$i] . "')";
          //$this->log->aPrint( $sql ); exit;
    		  if($this->db->query($sql)){
    		    //return true;
    		  }else{
    		    //return false;
    		  }
    		}
    	} // end for
    	return true;
    }
    
    public function insertPay($data,$balance,$payed_sum,$order_price){
      $sql = "delete from pay where txid = '" . $data['txid'] . "'";
      if($this->db->query($sql)){   }else{    }
      /*
        $aPay_price,
        $aPay_method,
        $aPay_date,
        $aPay_num,
        $aPay_user,
        'txid' => $txid
      */
      /*
      $this->log->aPrint( $data );
      $this->log->aPrint( $balance );
      $this->log->aPrint( $order_price );
      $this->log->aPrint( $payed_sum );
      */
      # Double check the balance and payed
      $tmpPayed = 0;
      for($i=0;$i<count($data[0]);$i++){
        // todo. use pay_price as key, it's not strong one , james-201103 
        if($data[0][$i]){
          $sql = "INSERT INTO pay ";
          $sql.= " (txid,pay_price,pay_method,pay_date,pay_num,pay_user,store_id) values (";
          $sql.= " '" . $data['txid'] . "','" . $data[0][$i] . "','" . $data[1][$i] . "','";
          $sql.= $data[2][$i] . "','" . $data[3][$i] . "','";
          $sql.= $data['pay_user'] . "','" . $data['store_id'] . "')";
    		  $this->db->query($sql); // ? //return true; : //return false ;
    		}
    		$tmpPayed += $data[0][$i];
    	}
    	if(number_format($tmpPayed,2) != number_format($payed_sum,2)){
    	  echo 'Payed Sum Wrong !! call IT team'; exit;
    	}
    	$tmpBalance = number_format($order_price - $tmpPayed,2);
    	//$this->log->aPrint( $tmpBalance); exit;
    	if(number_format($balance,2) != $tmpBalance){
    	  echo 'Balance Wrong !! call IT team'; exit;
    	}
      // update balance and payed_sum to tx
      $sql = "update transaction set balance = $balance, payed_sum = $payed_sum where txid = '" . $data['txid'] . "'";
      $this->db->query($sql);
      return true;
    }
    
    public function selectTransaction($txid){
      $sql = "select * from transaction where txid ='$txid'";
    	$query = $this->db->query($sql);
    	$response = $query->row;
      return $response;
    }
    
    public function selectStore($id){
      $sql = "select * from storelocator where id ='$id'";
    	$query = $this->db->query($sql);
    	$response = $query->row;
      return $response;
    }
    
    public function selectStoreWithAccount($accountno){
      $sql = "select * from storelocator where accountno ='$accountno'";
    	$query = $this->db->query($sql);
    	$response = $query->row;
      return $response;
    }
    
    public function selectStoreARTotal($store_id){
      $sql = "select store_id,count(txid) as count,sum(total) as tot_order, sum(subtotal) as subtotal ,
                     sum(payed_sum) as tot_payed,(sum(total) - sum(payed_sum)) as balance
                from transaction where store_id = $store_id";
    	$query = $this->db->query($sql);
    	$data = $query->row;
      return $data;
    }
    
    public function quickbookHistory($store_id){
      $sql = "select a.* from storelocator s, ar_kim a where trim(s.accountno) = trim(a.c1) and s.id = $store_id";
    	$query = $this->db->query($sql);
    	$data = $query->row;
      return $data;
    }
    
    public function selectStoreHistory($store_id){
      //datediff(date_format(pa.pay_date,'%Y%m%d'),date_format(tx.order_date,'%Y%m%d')) as pay_diff
      // todo. bad design and no monetized date type lead this exception query , james 201108 
      $sql = "
             (select pa.txid as txid,tx.order_date, tx.total, tx.payed_sum, tx.balance, 
                    datediff(date_format(curdate(),'%Y%m%d'),date_format(tx.order_date,'%Y%m%d')) as order_diff,
                    pa.pay_date,
                    pa.pay_price,
                    pa.pay_method,
                    pa.pay_num,
                    datediff(date_format(concat(substr(pa.pay_date,1,4),substr(pa.pay_date,5,2),substr(pa.pay_date,7,2)),'%Y%m%d'),date_format(tx.order_date,'%Y%m%d')) as pay_diff
               from pay pa, transaction tx
              where pa.txid = tx.txid
                and tx.txid in (select txid from transaction where store_id = {$store_id})
                and tx.balance != 0 ) 
              union
             (select txid, order_date, total, payed_sum, balance,
                     datediff(date_format(curdate(),'%Y%m%d'),date_format(order_date,'%Y%m%d')) as order_diff, 
                     '','','','',''
                from transaction
               where store_id = {$store_id}
                 and format(balance,2) != 0.00 )
               order by txid,pay_price desc
             ";
      //$this->log->aPrint( $sql ); exit;
    	$query = $this->db->query($sql);
    	$data = $query->rows;
      //$this->log->aPrint( $data );
      $t1 = '';
      $aData = array();
      foreach($data as $k => $v){
        //echo $v['order_price'];
        $txid = $v['txid'];
        if($txid != $t1){
          $t1 = $txid;
          $i = 0;
          $aData[$txid]['order_date'] = $v['order_date'];
          $aData[$txid]['order_price'] = $v['total'];
          $aData[$txid]['payed_sum'] = $v['payed_sum'];
          $aData[$txid]['balance'] = $v['balance'];
          $aData[$txid]['order_diff'] = $v['order_diff'];
          $aData[$txid]['pay'][$i] = array(
                                      'pay_date' => $v['pay_date'],
                                      'pay_price' => $v['pay_price'],
                                      'pay_diff' => $v['pay_diff'],
                                      'pay_method' => $v['pay_method'],
                                      'pay_num' => $v['pay_num'],
                                     );
          $i++;
        }else{
          $i++;
          $aData[$txid]['pay'][$i]['pay_date'] = $v['pay_date'];
          $aData[$txid]['pay'][$i]['pay_price'] = $v['pay_price'];
          $aData[$txid]['pay'][$i]['pay_diff'] = $v['pay_diff'];
          $aData[$txid]['pay'][$i]['pay_method'] = $v['pay_method'];
          $aData[$txid]['pay'][$i]['pay_num'] = $v['pay_num'];
        }
      }
      //$this->log->aPrint( $aData );
      return $aData;
    }
    
    public function selectFreegoodSum($txid){
      //$this->log->aPrint( $txid );
      $sql = "select sum(free * price1) as freegood_sum from sales where txid = '$txid'";
      //$this->log->aPrint( $sql );
      $query = $this->db->query($sql);
      $res = $query->row['freegood_sum'];
      return $res;
    }
    
    public function getSales($txid){
          $sql = "select s.product_id, s.model, s.order_quantity, s.price1, s.discount, s.discount2,
                         s.free, s.damage, s.discount, s.total_price, p.quantity, s.weight_row, 
                         p.ups_weight ,p.image, pd.name as description, p.pc,
                         s.inventory, s.shipped
                    from sales s, product p, product_description pd
                   where s.txid ='$txid' 
                     and s.product_id = p.product_id
                     and p.product_id = pd.product_id
                     order by s.model" ;
        $query = $this->db->query($sql);
        $sales = $query->rows;
        $aSale = array();
        foreach($sales as $s) {
            $aSale[$s['model']] = $s;
        }
        return $aSale;
    }
    
    public function selectShip($txid){
      $aData = array();
      $sql = "select * from ship where txid ='$txid' order by ship_date";
      //echo $sql; exit;
    	$query = $this->db->query($sql);
      $res = $query->rows;
      $aData = $res;
      return $aData;
    }
    
    public function selectPay($txid){
      $aData = array();
      $sql = "select * from pay where txid ='$txid' order by pay_date";
    	$query = $this->db->query($sql);
      $res = $query->rows;
      $aData = $res;
      return $aData;
    }
    
    /*
      Update store information
      todo. i made Sales to edit Store-information in Order sheet.
      It could be blocked and let them go to Account Menu for any DDL
      If some decide to block this functinality later, Change several input as readonly or plain text , james 201105 
      todo. Need more htmlentities (aka esacpe in db library ) work
     */
    public function updateStore($data){
      //$this->log->aPrint( $data ); exit;
      $sql = "update storelocator set name = '" . $this->db->escape($data['name']) . "'," ;
      $sql.= " accountno = '" . $data['accountno'] . "'," ;
      $sql.= " salesrep = '" . $data['salesrep'] . "'," ;
      $sql.= " address1 = '" . $data['address1'] . "'," ;
      $sql.= " city = '" . $data['city'] . "'," ;
      $sql.= " state = '" . $data['state'] . "'," ;
      $sql.= " zipcode = '" . $data['zipcode'] . "'," ;
      $sql.= " storetype = '" . $data['storetype'] . "'," ;
      $sql.= " phone1 = '" . $data['phone1'] . "'," ;
      $sql.= " fax = '" . $data['fax'] . "'," ;
      $sql.= " discount = '" . $data['discount'] . "'" ;
      $sql.= " where id = '" . $data['id'] . "'";
      if($this->db->query($sql)){
    	  return true;
    	}else{
    	  return false;
    	}
    }
    
    public function updateTransaction($data){
    	date_default_timezone_set('America/Chicago');
        // Share same code with invoice one
        $isInvoiceMode = (isset($_REQUEST['invoice'])) ? true : false;

        $invoiceNo = isset($data['invoice_no']) ? $data['invoice_no'] : 0;
        $isShipped = false;
        if ($isInvoiceMode && 0 == $invoiceNo) {
                $invoiceNo = $this->generateInvoiceNo();
                $isShipped = true;
        }

        //$this->log->aPrint( $data ); exit;
        $executor = $this->user->getUsername();
        //$order_date = $data['order_date'] . ' ' . date("H:m:s");
        //$order_price = $data['order_price'] + $data['ship_cod'] + $data['ship_lift'];
        $sql = "update transaction set store_id = '" . $data['store_id'] . "'," ;
        $sql.= " description    = '" . $this->db->escape($data['description']) . "'," ;
        //$sql.= " order_user     = '" . $data['order_user'] . "'," ;
        $sql.= " sold_ym       = '" . $data['sold_ym'] . "'," ;
        $sql.= " total    = '" . $data['total'] . "'," ;
        $sql.= " subtotal    = '" . $data['subtotal'] . "'," ;
        //$sql.= " balance        = '" . $data['balance'] . "'," ;
        //$sql.= " weight_sum     = '" . $data['weight_sum'] . "'," ;
        //todo. allow to update order_date
        $sql.= " order_date     = concat('" . $data['order_date'] ."',substr(order_date,11,9)),";
        $sql.= " executor       = '" . $executor . "',";
        $sql.= " payment        = '" . $data['payment'] . "',";
        $sql.= " ship_method    = '" . $data['ship_method'] . "',";
        $sql.= " cod            = '" . $data['ship_cod'] . "',";
        
        if (0 !== $invoiceNo) {
            $sql.= " invoice_no = '" . $invoiceNo . "',";
        }
        if ($isShipped == true){
        	$sql.= " shipped_date = '" . date("Y-m-d h:i:s") . "',";
        }
        //$sql.= " lift           = '" . $data['ship_lift'] . "',";
        $sql.= " status         = '" . $data['status'] . "'," ;
        $sql.= " ship_appointment = '" . $data['ship_appointment'] . "',";
        $sql.= " shipto         = '" . $this->db->escape($data['shipto']) . "',";
        $sql.= " discount       = '" . $data['discount'] . "'";
        //$sql.= " pc_date        = '" . $data['pc_date'] . "',";
        //$sql.= " post_check     = '" . $data['post_check'] . "',";
        //$sql.= " cur_check      = '" . $data['cur_check'] . "',";
        //$sql.= " cur_cash       = '" . $data['cur_cash'] . "'";
        $sql.= " where txid     = '" . $data['txid'] . "'";
        //$this->log->aPrint( $sql ); exit;
        if($this->db->query($sql)){
          return true;
        }else{
          if( !$this->db->query($sql) ){
            $aErr = array();
            $aErr['key'] = $txid;
            $aErr['msg'] = $sql;
            $this->sendJames($aErr);
            echo '<br/>James solve soon, sorry for disturbing<br/>';
            return false;
          }
        }
    }
    
    public function sendJames($request){
      $subject = $request['key'];
      $html    = $request['msg'];
      $mail = new Mail();
    	$mail->protocol = $this->config->get('config_mail_protocol');
    	$mail->hostname = $this->config->get('config_smtp_host');
    	$mail->username = $this->config->get('config_smtp_username');
    	$mail->password = $this->config->get('config_smtp_password');
    	$mail->port = $this->config->get('config_smtp_port');
    	$mail->timeout = $this->config->get('config_smtp_timeout');
    
      //$this->log->aPrint( $subject );
      $aReceiver = array(
        'james@live.com',
      );
      foreach($aReceiver as $receiver){
    	  $mail->setTo($receiver);
    	  $mail->setFrom($this->config->get('config_email'));
    	  $mail->setSender('james@live.com');
    	  $mail->setSubject($subject);
    	  //$mail->setText(html_entity_decode($body, ENT_QUOTES, 'UTF-8'));
        $mail->setHtml($html);
    	  $mail->send();
      }
    }
    
    public function updateExecutor($req){
      $txid = $req['txid'];
      $executor = $req['executor'];
      $sql = "update transaction set executor = '$executor' ";
      $sql.= " where txid = '$txid'";
      //$this->log->aPrint( $sql ); exit;
      if($query = $this->db->query($sql)){
        return true;
      }
    }
}
?>
