<?php
class ModelSalesStat extends Model{
	public function getList($month){
    $sql = "select * from rep_stat where month = '$month'";
    //$this->log->aPrint( $sql ); exit;
    $query = $this->db->query($sql);
    return $query->rows;
	}

	public function getRep(){
    $sql = "select username,user_group_id,firstname from user";
    //$sql.= " where user_group_id = 11 order by username";
    //$this->log->aPrint( $sql );
    $query = $this->db->query($sql);
    return $query->rows;
	}

	public function getTarget(){
    $aReturn = array();
    $sql = "select * from rep_stat where substr(month,1,4) > '2011' and target > 0";
    $sql.= " order by month,target desc";
    //$this->log->aPrint( $sql );
    $query = $this->db->query($sql);
    foreach( $query->rows as $row){
      $aReturn[$row['month']][$row['rep']] = $row['target'];
    }
    return $aReturn;
	}

	public function getRepSum($rep,$month){
    $sql = "select order_user,sum(order_price) as total,count(order_price) as count from transaction ";
    $sql.= " where concat(substr(order_date,1,4),substr(order_date,6,2)) = '$month'";
    $sql.= "   and order_user = '$rep' group by order_user";
    $query = $this->db->query($sql);
    return $query->row;
	}

  public function updateTarget($data){
    $month = $data['month'];
    $rep = $data['rep'];
    $target = $data['target'];
    $today = date('Ymd');
    $sql = "select target from rep_stat";
    $sql.= " where rep = '$rep' and month = '$month'";
    $query = $this->db->query($sql);
    if( isset($query->row['target']) ){  // update
      $sql = "update rep_stat set target = '$target', up_date = '$today'";
      $sql.= " where rep = '$rep' and month = '$month'";
      if($this->db->query($sql)){
        return true;
      }else{
        return false;
      }
    }else{
      $sql = "insert into rep_stat set target = '$target', up_date = '$today',";
      $sql.= "       rep = '$rep' , month = '$month'";
      if($this->db->query($sql)){
        return true;
      }else{
        return false;
      }
    }
  }

  public function updatePackage($data){
    $month = $data['month'];
    $sql = "select count(*) as count from rep_stat where month = '$month'";
    $query = $this->db->query($sql);
    $count = $query->row['count'];
    if($count == 0) $data['ddl'] = 'insert';
    for($i=0; $i<count($data['rep']);$i++){
      if($data['target'][$i] > 0){
        if($data['ddl'] == 'insert'){
          $sql = "insert into rep_stat ( month,rep,target,up_date ) values ( ";
          $sql.= " '$month','". $data['rep'][$i] ."','". $data['target'][$i] ."','". date('Ymd') ."' )";
        }else{  // update
          $sql = "update rep_stat set target = " . $data['target'][$i];
          $sql.= " where id = " .$data['id'][$i];
        }
        $query = $this->db->query($sql);
      }
    }
    return true;
  }
}
?>
