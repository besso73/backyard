<?php

class ModelProductBase extends Model {
    
    public function getTees( $refresh = false ) {

        $key = CACHE_KEY_TEES;
        
        if ( $refresh == true ) {
            $this->cache->delete($key);
        }

        $sql = "SELECT p.product_id, pd.name as name, p.model, p.ws_price, p.rt_price,
        			   p.quantity, p.barcode
        		  FROM product p
        		  JOIN product_description pd ON p.product_id = pd.product_id
        		 WHERE p.status = 1
        		   AND p.model != ''
        		   AND pd.name != ''
        		   AND substr(p.model,1,2) = 'AA'
        		 ORDER BY p.model ASC";
        $query = $this->db->query($sql);
        $aTee = $query->rows;
        
        foreach ( $query->rows as $row ) {
            $tees[$row['model']] = $row;
        }
        
        $this->cache->set($key, $tees);
        
        return $tees;
    }
    
        
    public function getStockings( $refresh = false ) {

        $key = CACHE_KEY_STOCKINGS;
        
        if ( $refresh == true ) {
            $this->cache->delete($key);
        }

        $sql = "SELECT p.product_id, pd.name as name, p.model, p.ws_price, p.rt_price,
        			   p.quantity, p.barcode
        		  FROM product p
        		  JOIN product_description pd ON p.product_id = pd.product_id
        		 WHERE p.status = 1
        		   AND p.model != ''
        		   AND pd.name != ''
        		   AND substr(p.model,1,2) = 'PH'
        		 ORDER BY p.model ASC";
        $query = $this->db->query($sql);
        $aTee = $query->rows;
        
        foreach ( $query->rows as $row ) {
            $stockings[$row['model']] = $row;
        }
        
        $this->cache->set($key, $stockings);
        
        return $stockings;
    }

    public function getHats( $refresh = false ) {

        $key = CACHE_KEY_HATS;
        
        if ( $refresh == true ) {
            $this->cache->delete($key);
        }

        $sql = "SELECT p.product_id, pd.name as name, p.model, p.ws_price, p.rt_price,
        			   p.quantity, p.barcode
        		  FROM product p
        		  JOIN product_description pd ON p.product_id = pd.product_id
        		 WHERE p.status = 1
        		   AND p.model != ''
        		   AND pd.name != ''
        		   AND substr(p.model,1,2) = 'HT'
        		 ORDER BY p.model ASC";
        //echo $sql;
        $query = $this->db->query($sql);
        $aTee = $query->rows;
        
        foreach ( $query->rows as $row ) {
            $hats[$row['model']] = $row;
        }
        
        $this->cache->set($key, $hats);
        
        return $hats;
    }

    public function getAdores( $refresh = false ) {

        $key = CACHE_KEY_ADORE;

        if ( $refresh == true ) {
            $this->cache->delete($key);
        }

        $sql = "SELECT p.product_id, pd.name as name, p.model, p.ws_price, p.rt_price,
            p.quantity, p.barcode
                FROM product p
                JOIN product_description pd ON p.product_id = pd.product_id
                WHERE p.status = 1
                AND p.model != ''
                AND pd.name != ''
                AND substr(p.model,1,3) = 'adr'
                ORDER BY p.model ASC";
        $query = $this->db->query($sql);
        $aTee = $query->rows;

        foreach ( $query->rows as $row ) {
            $adores[$row['model']] = $row;
        }

        $this->cache->set($key, $adores);

        return $adores;
    } 

    public function getBelts( $refresh = false ) {

        $key = CACHE_KEY_ADORE;

        if ( $refresh == true ) {
            $this->cache->delete($key);
        }

        $sql = "SELECT p.product_id, pd.name as name, p.model, p.ws_price, p.rt_price,
            p.quantity, p.barcode
                FROM product p
                JOIN product_description pd ON p.product_id = pd.product_id
                WHERE p.status = 1
                AND p.model != ''
                AND pd.name != ''
                AND substr(p.model,1,2) = 'BT'
                ORDER BY p.model ASC";
        $query = $this->db->query($sql);
        $aTee = $query->rows;

        foreach ( $query->rows as $row ) {
            $belts[$row['model']] = $row;
        }

        $this->cache->set($key, $belts);

        return $belts;
    } 


    public function delete($id){
        $sql = "DELETE FROM product WHERE product_id = '" . (int)$id . "'";
        $this->db->query($sql);
        return true;
    }

}
?>
