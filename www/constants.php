<?php

/* cache key lists
 * james, 2014-03
 */
define('CACHE_KEY_PRODUCTS','products');
define('CACHE_KEY_TEES','tees');
define('CACHE_KEY_STOCKINGS','stockings');
define('CACHE_KEY_HATS','hats');
define('CACHE_KEY_ADORE','adores');
define('CACHE_KEY_BELT','belts');
define('ATC_MAX_RETURN',200);

define('LOGO_IMAGE','/image/icon/logo.jpg');
define('COMPANY_ADDRESS1','7789 North Caldwell Ave.');
define('COMPANY_ADDRESS2','Niles, IL, 60714');
define('COMPANY_TEL','847.663.0900');
define('COMPANY_FAX','847.663.0905');

