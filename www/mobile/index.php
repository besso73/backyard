<?php
require_once './include/constants.php';
require_once './libs/class.MVC.php';

$registry = new Registry();

$loader = new Loader($registry);
$registry->set('load', $loader);

$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
$registry->set('db', $db);

$request = new Request();
$registry->set('request', $request);

$response = new Response();
$response->addHeader('Content-Type: text/html; charset=utf-8');
$registry->set('response', $response); 

$cache = new Cache();
$registry->set('cache', $cache); 

$session = new Session();
$registry->set('session', $session); 

$user = new User($registry);
$registry->set('user', $user);

$controller = new Front($registry);

$route = isset($request->get['route']) ? $request->get['route'] : 'common/home';
$action = new Action($route);

// Dispatch
$controller->dispatch($action, new Action('error/not_found'));

// Output
$response->output();

?>