<!DOCTYPE html> 
<html>
<head>
	<title>Aroo Mobile</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="jquery.mobile/jquery.mobile-1.4.2.min.css" />
	<link rel="stylesheet" href="css/style.css" />
	<script src="jquery.mobile/jquery-1.7.1.min.js"></script>
	<script src="jquery.mobile/jquery.mobile-1.4.2.min.js"></script>
	<script src="js/main.js"></script>
</head>

<body>
	<div data-role="navbar" data-grid="b" data-iconpos="left">
	    <ul>
	        <li><a href="#" data-icon="home" class="ui-btn-active">Home</a></li>
	        <li><a href="#" data-icon="grid">Barcode</a></li>
	        <li><a href="#" data-icon="user">Signout</a></li>
	    </ul>
	</div>
	<div id='error' class='ui-bar'></div>
