<?php echo $header; ?>
<?php if($error != '') { ?><script> showStatus('<?php echo $error; ?>'); </script><?php } ?>
    

    <ul id="barcode-atc" data-role="listview" data-inset="true" data-filter="true" data-filter-placeholder="Find Products" data-filter-theme="a"></ul>

    <form id="barcode-form" action='index.php?route=product/barcode/updateBarcode' method="post" data-ajax="false">
        <div data-role="content"  data-theme="d">
            <li data-role="fieldcontain">
                <label for="model">Model:</label>
                <input type="text" name="model" placeholder="model" value="" readonly>
                <input type='hidden' name='product_id'/>
                <input type='hidden' name='mode' value='update' />
            </li>
            <li data-role="fieldcontain">
                <label for="barcode">Barcode:</label>
                <input type="text" name="barcode" placeholder="Barcode" value="">
            </li>
            <li data-role="fieldcontain">
                <label for="name">Name:</label>
                <input type="text" name="name" placeholder="name" value="">
            </li>
            <li data-role="fieldcontain">
                <label for="rt_price">Price:</label>
                <input type="text" name="rt_price" placeholder="Retail Price" value="">
            </li>
            <li data-role="fieldcontain">
                <label for="quantity">Quantity:</label>
                <input type="text" name="quantity" placeholder="Quantity" value="">
            </li>

        </div>
        <input type="submit" id="barcode-submit" value="Save">
    </form>

<?php echo $footer; ?>