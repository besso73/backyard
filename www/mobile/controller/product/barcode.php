<?php
class ControllerProductBarcode extends Controller{
    
    public function index(){
        
        $this->route = $this->request->get['route'];
    	$this->view();
    }
    
    private function view(){
        $data = array();

        $data['error'] = '';
    	$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');

		//$data['lnk_action'] = 'index.php?route=common/login';

		$template = $this->route . '.tpl';
		
    	$this->response->setOutput($this->load->view($template, $data));
    }

    public function getProduct() {
        $this->load->model('product/base');
        $model = $this->request->post['model'];
        $aModel = $this->model_product_base->getProductByModel($model);
        echo json_encode($aModel[0]);
    }

    public function products() {
        
        $query = isset($this->request->get['q']) ?  $this->request->get['q'] : '';
    
        $products = $this->cache->get('products');
    
        $idx = 0;
        $result = array();
        foreach($products as $product) {

            if($idx == 20) break;

            $query = strtolower($query);
            
            
            if (strstr($query,' ')) {
                $aWord = explode(' ',$query);
                $ptn = '(.*)';
                foreach($aWord as $word) {
                    $ptn .= '(' . $word . ')(.*)';
                }
            } else {
                $ptn = $query;
            }
            
            if( preg_match("/$ptn/", strtolower($product['name'])) ) {
                $result[] = $product;
                $idx++;
                continue;
            }

            if( preg_match("/$ptn/", strtolower($product['model'])) ) {
                $result[] = $product;
                $idx++;
                continue;
            }

        }
        echo json_encode($result);
    }

    public function updateBarcode(){
        $this->load->model('product/barcode');
    
        if($this->model_product_barcode->updateBarcode($this->request->post)){
            echo json_encode(array('code'=>200));
        }else{
            echo json_encode(array('code'=>500));
        }
    }
}
?>

