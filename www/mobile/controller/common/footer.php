<?php   
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->data = array();
		$this->template = '/common/footer.tpl';
		$this->response->setOutput($this->load->view($this->template, $this->data));
	} 	
}