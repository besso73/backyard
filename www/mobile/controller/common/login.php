<?php
class ControllerCommonLogin extends Controller { 

	public function index() {
		$data['error'] = $this->error;
		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');
		$data['lnk_action'] = 'index.php?route=common/login/login';
		$template = '/common/login.tpl';
	
		$this->response->setOutput($this->load->view($template, $data));
	}

	public function login() { 
	
		$username = isset($this->request->post['username']) ? $this->request->post['username'] : '';
		$password = isset($this->request->post['password']) ? $this->request->post['password'] : '';
		
		if ('' !== $username && '' !== $password) {
			if ($this->user->login($username, $password)) {
				$this->response->redirect('index.php?route=common/home');
			} else {
				$this->error = 'Wrong Login Information';
				$this->index();
				//$this->response->redirect('index.php?route=common/login');
			}
		} else {
			$this->error = 'Empty Login Information';
			$this->index();
			//$this->response->redirect('index.php?route=common/login');
		}
		
	}	

	public function logout() {
		if($this->user->isLogin()) {
			$this->user->logout();
			$this->response->redirect('index.php?route=common/home');
		}
	}
}
?>
