<?php  
class ControllerCommonHome extends Controller {
	public function index() {

		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');
	
		$isLogged = $this->user->isLogged();
		if ($isLogged > 0) {
			$this->response->redirect('index.php?route=product/barcode');
		} else {
			$this->response->redirect('index.php?route=common/login');
		}

		$template = '/common/home.tpl';
		$this->response->setOutput($this->load->view($template, $data));
	}
}
