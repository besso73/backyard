<?php
class ModelProductBase extends Model {

	public function getProductByModel($model) {
		$model = strtolower($model);
		$model = trim($model);
		if ($model) {
			$sql = "SELECT p.product_id, p.model, pd.name, p.rt_price, p.quantity, p.barcode
					  FROM product p
					  JOIN product_description pd on p.product_id = pd.product_id
					 WHERE LCASE(p.model) = '$model'";
			$query = $this->db->query($sql);
			return $query->rows;
		}else{
			return array();
		}
	}
}