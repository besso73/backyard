<?php
require_once DIR_APPLICATION . '/../include/class.DbConnManager.php';
require_once DIR_APPLICATION . '/../include/class.SqlBuilder.php';

final class Mssql {
	public function __construct() {
	}

	public function query($sql,$params) {
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		return DbConnManager::GetDb('mpower')->Exec($sql);
	}
}
