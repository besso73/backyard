<?php
error_reporting(E_ALL);

if (!ini_get('date.timezone')) {
	date_default_timezone_set('UTC');
}

/* Registry class to regist classes to call simply and easily */
final class Registry {
	private $data = array();

	public function get($key) {
		return (isset($this->data[$key]) ? $this->data[$key] : NULL);
	}

	public function set($key, $value) {
		$this->data[$key] = $value;
	}

	public function has($key) {
		return isset($this->data[$key]);
	}
}

abstract class Controller {
	protected $registry;
	protected $error;

	public function __construct($registry) {
		$this->registry = $registry;
	}

	public function __get($key) {
		return $this->registry->get($key);
	}

	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
}

final class Action {
	private $file;
	private $class;
	private $method;
	private $args = array();

	public function __construct($route, $args = array()) {
		$path = '';
		// Break apart the route
		$parts = explode('/', str_replace('../', '', (string)$route));
		
		foreach ($parts as $part) { 
			$path .= $part;
			$dir = DIR_APPLICATION . 'controller/' . $path;
			if (is_dir($dir)) {
				$path .= '/';
				array_shift($parts);
				continue;
			}
			
			if (is_file(DIR_APPLICATION . 'controller/' . str_replace(array('../', '..\\', '..'), '', $path) . '.php')) {
				$this->file = DIR_APPLICATION . 'controller/' . str_replace(array('../', '..\\', '..'), '', $path) . '.php';
				$this->class = 'Controller' . preg_replace('/[^a-zA-Z0-9]/', '', $path);
				array_shift($parts);
				break;
			}
		}
		
		if ($args) {
			$this->args = $args;
		}
			
		$method = array_shift($parts);
		if ($method) {
			$this->method = $method;
		} else {
			$this->method = 'index';
		}
	}

	public function execute($registry) {
		// Stop any magical methods being called  
		if (substr($this->method, 0, 2) == '__') {
			return false;
		}
		
		if (is_file($this->file)) {
			include_once($this->file);
			
			$class = $this->class;
			
			$controller = new $class($registry);

	
			if (is_callable(array($controller, $this->method))) {
				return call_user_func_array(array($controller, $this->method), $this->args);
			} else {	
				return false;
			}
		} else {	
			return false;
		}	
	}
}

abstract class Model {
	protected $registry;
	
	public function __construct($registry) {
		$this->registry = $registry;
	}

	public function __get($key) {
		return $this->registry->get($key);
	}

	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
}

final class Front {
	private $registry;
	private $pre_action = array();
	private $error;

	public function __construct($registry) {
		$this->registry = $registry;
	}

	public function addPreAction($pre_action) {
		$this->pre_action[] = $pre_action;
	}

 	public function dispatch($action, $error) {
		$this->error = $error;

		foreach ($this->pre_action as $pre_action) {
			$result = $this->execute($pre_action);
					
			if ($result) {
				$action = $result;
				
				break;
			}
		}
	
		while ($action) {
			$action = $this->execute($action);
		}
  	}
    
	private function execute($action) {
		$result = $action->execute($this->registry);
	
		if (is_object($result)) {
			$action = $result;
		} elseif ($result === false) {
			$action = $this->error;
		
			$this->error = '';	
		} else {
			$action = false;
		}
		
		return $action;
	}
}


final class Loader {
	private $registry;

	public function __construct($registry) {
		$this->registry = $registry;
	}
	
	public function controller($route) {
		// function arguments
		$args = func_get_args();
		
		// Remove the route
		array_shift($args);	

		$action = new Action($route, $args); 

		return $action->execute($this->registry);
	}
		
	public function model($model) {
		$key = 'model_' . str_replace('/', '_', $model);
		
			
		if (!$this->registry->has($key)) {
			$file = DIR_APPLICATION . 'model/' . $model . '.php';
			$class = 'Model' . preg_replace('/[^a-zA-Z0-9]/', '', $model);
	
			if (file_exists($file)) { 
				include_once($file);
				$this->registry->set($key, new $class($this->registry));
			} else {
				trigger_error('Error: Could not load model ' . $file . '!');
				exit();
			}
		}
	}

	/* Custom method to lookup web structure model */
	public function _model($model) {
		$key = 'model_' . str_replace('/', '_', $model);
		
			
		if (!$this->registry->has($key)) {
			$file = DIR_APPLICATION . '../model/' . $model . '.php';
			$class = 'Model' . preg_replace('/[^a-zA-Z0-9]/', '', $model);
	
			if (file_exists($file)) { 
				include_once($file);
				$this->registry->set($key, new $class($this->registry));
			} else {
				trigger_error('Error: Could not load model ' . $file . '!');
				exit();
			}
		}
	}
	
	public function view($template, $data = array()) {
		$file = DIR_VIEW . $template;
		if (file_exists($file)) {
			extract($data);
	
			ob_start();

			require($file);

			$output = ob_get_contents();
			ob_end_clean();

			return $output;
		} else {
			trigger_error('Error: Could not load template ' . $file . '!');
			exit();
		}
	}

	/* check later */
	/*
	public function library($library) {
		$file = DIR_SYSTEM . 'library/' . $library . '.php';

		if (file_exists($file)) {
			include_once($file);
		} else {
			trigger_error('Error: Could not load library ' . $file . '!');
			exit();
		}
	}
	public function helper($helper) {
		$file = DIR_SYSTEM . 'helper/' . $helper . '.php';

		if (file_exists($file)) {
			include_once($file);
		} else {
			trigger_error('Error: Could not load helper ' . $file . '!');
			exit();
		}
	}
	public function config($config) {
		$this->registry->get('config')->load($config);
	}

	public function language($language) {
		return $this->registry->get('language')->load($language);
	}
	*/
}

final class DB {
	private $driver;
	
	public function __construct($driver, $hostname, $username, $password, $database) {
		$file = DIR_LIBRARY . $driver . '.php';
		if (file_exists(DIR_LIBRARY . $driver . '.php')) {
			require_once(DIR_LIBRARY . $driver . '.php');
		} else {
			exit('Error: Could not load database file ' . $driver . '!');
		}
				
		$this->driver = new $driver($hostname, $username, $password, $database);
	}
		
  	public function query($sql) {
		return $this->driver->query($sql);
  	}
	
	public function escape($value) {
		return $this->driver->escape($value);
	}
	
  	public function countAffected() {
		return $this->driver->countAffected();
  	}

  	public function getLastId() {
		return $this->driver->getLastId();
  	}	
}

// Autoloader
function __autoload($class) {
	if (substr($class, 0, 10) == 'Controller' || substr($class, 0, 5) == 'Model') {
		$file = DIR_APPLICATION . str_replace('\\', '/', strtolower($class)) . '.php';
	} else {
		$file = DIR_LIBRARY . strtolower($class) . '.php';
	}
	if (file_exists($file)) {
		include($file);
	} else {
		echo('Error: Could not load class ' . $class . '.php!');
		exit();
	}
}



